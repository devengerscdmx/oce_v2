<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFacturaPensionRequest;
use App\Http\Requests\UpdateFacturaPensionRequest;
use App\Repositories\FacturaPensionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class FacturaPensionController extends AppBaseController
{
    /** @var  FacturaPensionRepository */
    private $facturaPensionRepository;

    public function __construct(FacturaPensionRepository $facturaPensionRepo)
    {
        $this->facturaPensionRepository = $facturaPensionRepo;
    }

    /**
     * Display a listing of the FacturaPension.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $facturaPensions = $this->facturaPensionRepository->all();

        return view('factura_pensions.index')
            ->with('facturaPensions', $facturaPensions);
    }

    /**
     * Show the form for creating a new FacturaPension.
     *
     * @return Response
     */
    public function create()
    {
        return view('factura_pensions.create');
    }

    /**
     * Store a newly created FacturaPension in storage.
     *
     * @param CreateFacturaPensionRequest $request
     *
     * @return Response
     */
    public function store(CreateFacturaPensionRequest $request)
    {
        $input = $request->all();

        $facturaPension = $this->facturaPensionRepository->create($input);

        Flash::success('Factura Pension saved successfully.');

        return redirect(route('facturaPensions.index'));
    }

    /**
     * Display the specified FacturaPension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $facturaPension = $this->facturaPensionRepository->find($id);

        if (empty($facturaPension)) {
            Flash::error('Factura Pension not found');

            return redirect(route('facturaPensions.index'));
        }

        return view('factura_pensions.show')->with('facturaPension', $facturaPension);
    }

    /**
     * Show the form for editing the specified FacturaPension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $facturaPension = $this->facturaPensionRepository->find($id);

        if (empty($facturaPension)) {
            Flash::error('Factura Pension not found');

            return redirect(route('facturaPensions.index'));
        }

        return view('factura_pensions.edit')->with('facturaPension', $facturaPension);
    }

    /**
     * Update the specified FacturaPension in storage.
     *
     * @param int $id
     * @param UpdateFacturaPensionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFacturaPensionRequest $request)
    {
        $facturaPension = $this->facturaPensionRepository->find($id);

        if (empty($facturaPension)) {
            Flash::error('Factura Pension not found');

            return redirect(route('facturaPensions.index'));
        }

        $facturaPension = $this->facturaPensionRepository->update($request->all(), $id);

        Flash::success('Factura Pension updated successfully.');

        return redirect(route('facturaPensions.index'));
    }

    /**
     * Remove the specified FacturaPension from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $facturaPension = $this->facturaPensionRepository->find($id);

        if (empty($facturaPension)) {
            Flash::error('Factura Pension not found');

            return redirect(route('facturaPensions.index'));
        }

        $this->facturaPensionRepository->delete($id);

        Flash::success('Factura Pension deleted successfully.');

        return redirect(route('facturaPensions.index'));
    }
}
