<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTipoTicketRequest;
use App\Http\Requests\UpdateTipoTicketRequest;
use App\Repositories\TipoTicketRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class TipoTicketController extends AppBaseController
{
    /** @var  TipoTicketRepository */
    private $tipoTicketRepository;

    public function __construct(TipoTicketRepository $tipoTicketRepo)
    {
        $this->tipoTicketRepository = $tipoTicketRepo;
    }

    /**
     * Display a listing of the TipoTicket.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tipoTickets = $this->tipoTicketRepository->all();

        return view('tipo_tickets.index')
            ->with('tipoTickets', $tipoTickets);
    }

    /**
     * Show the form for creating a new TipoTicket.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_tickets.create');
    }

    /**
     * Store a newly created TipoTicket in storage.
     *
     * @param CreateTipoTicketRequest $request
     *
     * @return Response
     */
    public function store(CreateTipoTicketRequest $request)
    {
        $input = $request->all();

        $tipoTicket = $this->tipoTicketRepository->create($input);

        Flash::success('Tipo Ticket saved successfully.');

        return redirect(route('tipoTickets.index'));
    }

    /**
     * Display the specified TipoTicket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoTicket = $this->tipoTicketRepository->find($id);

        if (empty($tipoTicket)) {
            Flash::error('Tipo Ticket not found');

            return redirect(route('tipoTickets.index'));
        }

        return view('tipo_tickets.show')->with('tipoTicket', $tipoTicket);
    }

    /**
     * Show the form for editing the specified TipoTicket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoTicket = $this->tipoTicketRepository->find($id);

        if (empty($tipoTicket)) {
            Flash::error('Tipo Ticket not found');

            return redirect(route('tipoTickets.index'));
        }

        return view('tipo_tickets.edit')->with('tipoTicket', $tipoTicket);
    }

    /**
     * Update the specified TipoTicket in storage.
     *
     * @param int $id
     * @param UpdateTipoTicketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipoTicketRequest $request)
    {
        $tipoTicket = $this->tipoTicketRepository->find($id);

        if (empty($tipoTicket)) {
            Flash::error('Tipo Ticket not found');

            return redirect(route('tipoTickets.index'));
        }

        $tipoTicket = $this->tipoTicketRepository->update($request->all(), $id);

        Flash::success('Tipo Ticket updated successfully.');

        return redirect(route('tipoTickets.index'));
    }

    /**
     * Remove the specified TipoTicket from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoTicket = $this->tipoTicketRepository->find($id);

        if (empty($tipoTicket)) {
            Flash::error('Tipo Ticket not found');

            return redirect(route('tipoTickets.index'));
        }

        $this->tipoTicketRepository->delete($id);

        Flash::success('Tipo Ticket deleted successfully.');

        return redirect(route('tipoTickets.index'));
    }
}
