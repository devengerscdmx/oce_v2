<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGerenteRequest;
use App\Http\Requests\UpdateGerenteRequest;
use App\Models\EmpresaCollect;
use App\Models\Ticket;
use App\Repositories\GerenteRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Estacionamiento;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Response;
use App\Models\Gerente;
use Illuminate\Support\Facades\Hash;

class GerenteController extends AppBaseController
{
    /** @var  GerenteRepository */
    private $gerenteRepository;

    public function __construct(GerenteRepository $gerenteRepo)
    {
        $this->gerenteRepository = $gerenteRepo;
    }

    /**
     * Display a listing of the Gerente.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $gerentes = $this->gerenteRepository->all();

        return view('gerentes.index')
            ->with('gerentes', $gerentes);
    }

    /**
     * Show the form for creating a new Gerente.
     *
     * @return Response
     */
    public function create()
    {
        $estacionamientos = Estacionamiento::select('no_est','nombre')
            ->where('no_est','!=',1)
            ->get();
        return view('gerentes.create')
            ->with('estacionamientos',$estacionamientos);
    }

    /**
     * Store a newly created Gerente in storage.
     *
     * @param CreateGerenteRequest $request
     *
     * @return Response
     */
    public function store()
    {

        $input = request()->validate([
            'Estacionamiento' => ['required'],
            'Nombre' => ['required', 'string'],
            'Email' => ['required', 'email'],
            'Contraseña' => ['required', 'min:8']
        ]);



        $gerente = new Gerente();
        $gerente->fill([
            'id_est' => $input['Estacionamiento'],
            'name' => $input['Nombre'],
            'email' => $input['Email'],
            'password' => Hash::make($input['Contraseña'])
        ]);
        $gerente->save();
        //dd($gerente);

        Flash::success('Gerente agregado satisfactoriamente');

        return redirect(route('gerentes.index'));
    }

    /**
     * Display the specified Gerente.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $gerente = $this->gerenteRepository->find($id);

        if (empty($gerente)) {
            Flash::error('Gerente no encontrado');

            return redirect(route('gerentes.index'));
        }

        return view('gerentes.show')->with('gerente', $gerente);
    }

    /**
     * Show the form for editing the specified Gerente.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $gerente = $this->gerenteRepository->find($id);
        $estacionamientos = Estacionamiento::select('no_est','nombre')
            ->where('no_est','!=',1)
            ->get();

        if (empty($gerente)) {
            Flash::error('Gerente no encontrado');

            return redirect(route('gerentes.index'));
        }

        return view('gerentes.edit')
            ->with('gerente', $gerente)
            ->with('estacionamientos', $estacionamientos);
    }

    /**
     * Update the specified Gerente in storage.
     *
     * @param int $id
     * @param UpdateGerenteRequest $request
     *
     * @return Response
     */
    public function update($id)
    {
        $gerente = $this->gerenteRepository->find($id);

        if (empty($gerente)) {
            Flash::error('Gerente no encontrado');

            return redirect(route('gerentes.index'));
        }

        //$input = request()->all();
        //dd($input);

        $input = \request()->validate([
            'Estacionamiento' => ['required'],
            'Nombre' => ['string', 'required'],
            'Email' => ['required', 'email'],
            'Contraseña' => ['required', 'min:8'],
        ]);

        if ($input['Contraseña'] == null) {
            $user2 = Gerente::find($id);
            $user2->name = $input['Nombre'];
            $user2->email = $input['Email'];
            $user2->id_est = $input['Estacionamiento'];
            $user2->save();
        } else {
            $input['Contraseña'] = Hash::make($input['Contraseña']);
            $user = Gerente::find($id);
            $user->name = $input['Nombre'];
            $user->email = $input['Email'];
            $user->id_est = $input['Estacionamiento'];
            $user->password = $input['Contraseña'];
            $user->save();
        }

        Flash::success('Gerente actualizado satisfactoriamente.');

        return redirect(route('gerentes.index'));
    }

    /**
     * Remove the specified Gerente from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $gerente = $this->gerenteRepository->find($id);

        if (empty($gerente)) {
            Flash::error('Gerente no encontrado');

            return redirect(route('gerentes.index'));
        }

        $this->gerenteRepository->delete($id);

        Flash::success('Gerente eliminado satisfactoriamente.');

        return redirect(route('gerentes.index'));
    }
}
