<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEmpresaCollectRequest;
use App\Http\Requests\UpdateEmpresaCollectRequest;
use App\Repositories\EmpresaCollectRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class EmpresaCollectController extends AppBaseController
{
    /** @var  EmpresaCollectRepository */
    private $empresaCollectRepository;

    public function __construct(EmpresaCollectRepository $empresaCollectRepo)
    {
        $this->empresaCollectRepository = $empresaCollectRepo;
    }

    /**
     * Display a listing of the EmpresaCollect.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $empresaCollects = $this->empresaCollectRepository->all();

        return view('empresa_collects.index')
            ->with('empresaCollects', $empresaCollects);
    }

    /**
     * Show the form for creating a new EmpresaCollect.
     *
     * @return Response
     */
    public function create()
    {
        return view('empresa_collects.create');
    }

    /**
     * Store a newly created EmpresaCollect in storage.
     *
     * @param CreateEmpresaCollectRequest $request
     *
     * @return Response
     */
    public function store(CreateEmpresaCollectRequest $request)
    {
        $input = $request->all();

        $empresaCollect = $this->empresaCollectRepository->create($input);

        Flash::success('Empresa Collect saved successfully.');

        return redirect(route('empresaCollects.index'));
    }

    /**
     * Display the specified EmpresaCollect.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $empresaCollect = $this->empresaCollectRepository->find($id);

        if (empty($empresaCollect)) {
            Flash::error('Empresa Collect not found');

            return redirect(route('empresaCollects.index'));
        }

        return view('empresa_collects.show')->with('empresaCollect', $empresaCollect);
    }

    /**
     * Show the form for editing the specified EmpresaCollect.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $empresaCollect = $this->empresaCollectRepository->find($id);

        if (empty($empresaCollect)) {
            Flash::error('Empresa Collect not found');

            return redirect(route('empresaCollects.index'));
        }

        return view('empresa_collects.edit')->with('empresaCollect', $empresaCollect);
    }

    /**
     * Update the specified EmpresaCollect in storage.
     *
     * @param int $id
     * @param UpdateEmpresaCollectRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmpresaCollectRequest $request)
    {
        $empresaCollect = $this->empresaCollectRepository->find($id);

        if (empty($empresaCollect)) {
            Flash::error('Empresa Collect not found');

            return redirect(route('empresaCollects.index'));
        }

        $empresaCollect = $this->empresaCollectRepository->update($request->all(), $id);

        Flash::success('Empresa Collect updated successfully.');

        return redirect(route('empresaCollects.index'));
    }

    /**
     * Remove the specified EmpresaCollect from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $empresaCollect = $this->empresaCollectRepository->find($id);

        if (empty($empresaCollect)) {
            Flash::error('Empresa Collect not found');

            return redirect(route('empresaCollects.index'));
        }

        $this->empresaCollectRepository->delete($id);

        Flash::success('Empresa Collect deleted successfully.');

        return redirect(route('empresaCollects.index'));
    }
}
