<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIngresoRequest;
use App\Http\Requests\UpdateIngresoRequest;
use App\Models\Ingreso;
use App\Repositories\IngresoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class IngresoController extends AppBaseController
{
    /** @var  IngresoRepository */
    private $ingresoRepository;

    public function __construct(IngresoRepository $ingresoRepo)
    {
        $this->ingresoRepository = $ingresoRepo;
    }

    /**
     * Display a listing of the Ingreso.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //$ingresos = $this->ingresoRepository->all();
        $sql = Ingreso::join('gerente','gerente.id','=','ingresos.id_gerente')
            ->join('parks','gerente.id_est','=','parks.no_est')
            ->join('empresa_colect','empresa_colect.id','=','ingresos.id_empresa')
            ->join('banco_colect','banco_colect.id','=','ingresos.id_banco')
            ->join('img_ingresos','img_ingresos.id_ingreso','=','ingresos.id')
            ->where('valido','=','pendiente')
            ->orwhere('valido','=','corregida')
            ->orwhere('valido','=','no_recolec')
            ->select(['ingresos.*','parks.no_est','parks.nombre','banco_colect.banco','empresa_colect.empresa','img_ingresos.foto','img_ingresos.firma'])
            ->get();
        return view('ingresos.index')
            ->with('ingresos', $sql);
    }

    /**
     * Show the form for creating a new Ingreso.
     *
     * @return Response
     */
    public function create()
    {
        return view('ingresos.create');
    }

    /**
     * Store a newly created Ingreso in storage.
     *
     * @param CreateIngresoRequest $request
     *
     * @return Response
     */
    public function store(CreateIngresoRequest $request)
    {
        $input = $request->all();

        $ingreso = $this->ingresoRepository->create($input);

        Flash::success('Ingreso saved successfully.');

        return redirect(route('ingresos.index'));
    }

    /**
     * Display the specified Ingreso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ingreso = $this->ingresoRepository->find($id);

        if (empty($ingreso)) {
            Flash::error('Ingreso not found');

            return redirect(route('ingresos.index'));
        }

        return view('ingresos.show')->with('ingreso', $ingreso);
    }

    /**
     * Show the form for editing the specified Ingreso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ingreso = $this->ingresoRepository->find($id);

        if (empty($ingreso)) {
            Flash::error('Ingreso not found');

            return redirect(route('ingresos.index'));
        }

        return view('ingresos.edit')->with('ingreso', $ingreso);
    }

    /**
     * Update the specified Ingreso in storage.
     *
     * @param int $id
     * @param UpdateIngresoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIngresoRequest $request)
    {
        $ingreso = $this->ingresoRepository->find($id);

        if (empty($ingreso)) {
            Flash::error('Ingreso not found');

            return redirect(route('ingresos.index'));
        }

        $ingreso = $this->ingresoRepository->update($request->all(), $id);

        Flash::success('Ingreso updated successfully.');

        return redirect(route('ingresos.index'));
    }

    /**
     * Remove the specified Ingreso from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ingreso = $this->ingresoRepository->find($id);

        if (empty($ingreso)) {
            Flash::error('Ingreso not found');

            return redirect(route('ingresos.index'));
        }

        $this->ingresoRepository->delete($id);

        Flash::success('Ingreso deleted successfully.');

        return redirect(route('ingresos.index'));
    }

    public function Frechazos(Ingreso $id){
        $input = \request()->validate([
            'motivo' => ['required', 'string']
        ]);
        $id->valido = 'pendiente_correcion';
        $id->coment = $input['motivo'];
        $id->save();
        Flash::success('Genero solicitud de corrección exitosamente');
        return redirect(route('ingresos.index'));
    }
    public function Fvalidos(Ingreso $id){
        $id->valido = 'valido';
        $id->save();
        Flash::success('Ficha validada');
        return redirect(route('ingresos.index'));
    }
    public function calendario(){
        return view('ingresos.calendario');
    }
}
