<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFormaPagoRequest;
use App\Http\Requests\UpdateFormaPagoRequest;
use App\Repositories\FormaPagoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class FormaPagoController extends AppBaseController
{
    /** @var  FormaPagoRepository */
    private $formaPagoRepository;

    public function __construct(FormaPagoRepository $formaPagoRepo)
    {
        $this->formaPagoRepository = $formaPagoRepo;
    }

    /**
     * Display a listing of the FormaPago.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $formaPagos = $this->formaPagoRepository->all();

        return view('forma_pagos.index')
            ->with('formaPagos', $formaPagos);
    }

    /**
     * Show the form for creating a new FormaPago.
     *
     * @return Response
     */
    public function create()
    {
        return view('forma_pagos.create');
    }

    /**
     * Store a newly created FormaPago in storage.
     *
     * @param CreateFormaPagoRequest $request
     *
     * @return Response
     */
    public function store(CreateFormaPagoRequest $request)
    {
        $input = $request->all();

        $formaPago = $this->formaPagoRepository->create($input);

        Flash::success('Forma Pago saved successfully.');

        return redirect(route('formaPagos.index'));
    }

    /**
     * Display the specified FormaPago.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $formaPago = $this->formaPagoRepository->find($id);

        if (empty($formaPago)) {
            Flash::error('Forma Pago not found');

            return redirect(route('formaPagos.index'));
        }

        return view('forma_pagos.show')->with('formaPago', $formaPago);
    }

    /**
     * Show the form for editing the specified FormaPago.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $formaPago = $this->formaPagoRepository->find($id);

        if (empty($formaPago)) {
            Flash::error('Forma Pago not found');

            return redirect(route('formaPagos.index'));
        }

        return view('forma_pagos.edit')->with('formaPago', $formaPago);
    }

    /**
     * Update the specified FormaPago in storage.
     *
     * @param int $id
     * @param UpdateFormaPagoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFormaPagoRequest $request)
    {
        $formaPago = $this->formaPagoRepository->find($id);

        if (empty($formaPago)) {
            Flash::error('Forma Pago not found');

            return redirect(route('formaPagos.index'));
        }

        $formaPago = $this->formaPagoRepository->update($request->all(), $id);

        Flash::success('Forma Pago updated successfully.');

        return redirect(route('formaPagos.index'));
    }

    /**
     * Remove the specified FormaPago from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $formaPago = $this->formaPagoRepository->find($id);

        if (empty($formaPago)) {
            Flash::error('Forma Pago not found');

            return redirect(route('formaPagos.index'));
        }

        $this->formaPagoRepository->delete($id);

        Flash::success('Forma Pago deleted successfully.');

        return redirect(route('formaPagos.index'));
    }
}
