<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCentralUserRequest;
use App\Http\Requests\UpdateCentralUserRequest;
use App\Models\CentralUser;
use App\Models\Permiso;
use App\Models\Roles;
use App\Repositories\CentralUserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Response;

class CentralUserController extends AppBaseController
{
    /** @var  CentralUserRepository */
    private $centralUserRepository;

    public function __construct(CentralUserRepository $centralUserRepo)
    {
        $this->centralUserRepository = $centralUserRepo;
    }

    /**
     * Display a listing of the CentralUser.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $centralUsers = DB::table('central_users')
            ->join('roles', 'central_users.id', '=', 'roles.id_central_user')
            ->join('permisos', 'roles.id_permiso', '=', 'permisos.id')
            ->select('central_users.id', 'central_users.name', 'central_users.email', 'permisos.nombre')
            ->paginate(20);
        return view('central_users.index')
            ->with('centralUsers', $centralUsers);
    }

    /**
     * Show the form for creating a new CentralUser.
     *
     * @return Response
     */
    public function create()
    {
        $permisos = Permiso::all();
        return view('central_users.create')
            ->with('permiso', $permisos);
        //return view('central_users.create');
    }

    /**
     * Store a newly created CentralUser in storage.
     *
     * @param CreateCentralUserRequest $request
     *
     * @return Response
     */
    public function store(CreateCentralUserRequest $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = $this->centralUserRepository->create($input);
        $rol = new Roles();
        $rol->id_central_user = $user->id;
        $rol->id_permiso = $input['permiso'];
        $rol->save();
        Flash::success('Usuario creado exitosamente');

        return redirect(route('central_users.index'));
    }

    /**
     * Display the specified CentralUser.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        return redirect(route('central_users.index'));
    }

    /**
     * Show the form for editing the specified CentralUser.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $centralUser = DB::table('central_users')
            ->join('roles', 'central_users.id', '=', 'roles.id_central_user')
            ->join('permisos', 'roles.id_permiso', '=', 'permisos.id')
            ->where('central_users.id', '=', $id)
            ->select('central_users.*', 'permisos.nombre')
            ->get();
        if (empty($centralUser[0])) {
            Flash::error('User not found');

            return redirect(route('central_users.index'));
        }
        $permisos = Permiso::all();
        return view('central_users.edit')
            ->with('centralUser', $centralUser[0])
            ->with('permiso', $permisos);
    }

    /**
     * Update the specified CentralUser in storage.
     *
     * @param int $id
     * @param UpdateCentralUserRequest $request
     *
     * @return Response
     */
    public function update($id)
    {
        $user = $this->centralUserRepository->find($id);
        if (empty($user)) {
            Flash::error('Usuario no encontrado');

            return redirect(route('central_users.index'));
        }
        $input = \request()->validate([
            'permiso' => ['required'],
            'password' => [''],
            'email' => ['required'],
            'name' => ['required'],
        ]);
        if ($input['password'] == null) {
            $user2 = CentralUser::find($id);
            $user2->name = $input['name'];
            $user2->email = $input['email'];
            $user2->save();
            $rol = DB::table('roles')
                ->where('id_central_user', '=', $id)
                ->get();
            $roles = Roles::find($rol[0]->id);
            $roles->id_permiso = $input['permiso'];
            $roles->save();
        } else {
            $input['password'] = Hash::make($input['password']);
            $user = $this->centralUserRepository->update($input, $id);
            $rol = DB::table('roles')
                ->where('id_central_user', '=', $id)
                ->get();
            $roles = Roles::find($rol[0]->id);
            $roles->id_permiso = $input['permiso'];
            $roles->save();
        }

        Flash::success('Usuario actulaizado exitosamente');

        return redirect(route('central_users.index'));
    }

    /**
     * Remove the specified CentralUser from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $centralUser = $this->centralUserRepository->find($id);

        if (empty($centralUser)) {
            Flash::error('Usuario No encontrado');

            return redirect(route('centralUsers.index'));
        }

        $this->centralUserRepository->delete($id);

        Flash::success('Usuario eliminado exitosamente');

        return redirect(route('central_users.index'));
    }
}
