<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFirmaRequest;
use App\Http\Requests\UpdateFirmaRequest;
use App\Models\Firma;
use App\Repositories\FirmaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class FirmaController extends AppBaseController
{
    /** @var  FirmaRepository */
    private $firmaRepository;

    public function __construct(FirmaRepository $firmaRepo)
    {
        $this->firmaRepository = $firmaRepo;
    }

    /**
     * Display a listing of the Firma.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $firmas = Firma::all();
        return view('firmas.index')
            ->with('firmas', $firmas);
    }

    /**
     * Show the form for creating a new Firma.
     *
     * @return Response
     */
    public function create()
    {
        return view('firmas.create');
    }


    public function store(CreateFirmaRequest $request)
    {
        $valores = request()->all();
        dd($valores);
        $input = $request->all();
        $firma = $this->firmaRepository->create($input);
        $foto = request()->file('foto');
        if($foto != null){
            $dataImg = $foto->get();
            $nombre_archivo = $foto->getBasename();
            $image = imagecreatefromstring($dataImg);
            ob_start();
            imagejpeg($image,NULL,100);
            $cont = ob_get_contents();
            ob_end_clean();
            imagedestroy($image);
            $content = imagecreatefromstring($cont);
            $output = 'imgFirmas/'.$nombre_archivo.'.webp';
            imagewebp($content,$output);
            imagedestroy($content);
            $im_webp = file_get_contents($output);
            $img = base64_encode($im_webp);
            unlink($output);
        }
        $firma->imagen = $img;
        $firma->save();
        Flash::success('Registro creado exitosamente');

        return redirect(route('firmas.index'));
    }

    /**
     * Display the specified Firma.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $firma = $this->firmaRepository->find($id);

        if (empty($firma)) {
            Flash::error('Firma not found');

            return redirect(route('firmas.index'));
        }

        return view('firmas.show')->with('firma', $firma);
    }

    /**
     * Show the form for editing the specified Firma.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $firma = $this->firmaRepository->find($id);

        if (empty($firma)) {
            Flash::error('Firma not found');

            return redirect(route('firmas.index'));
        }

        return view('firmas.edit')->with('firma', $firma);
    }

    /**
     * Update the specified Firma in storage.
     *
     * @param int $id
     * @param UpdateFirmaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFirmaRequest $request)
    {
        $firma = $this->firmaRepository->find($id);

        if (empty($firma)) {
            Flash::error('Firma not found');

            return redirect(route('firmas.index'));
        }

        $firma = $this->firmaRepository->update($request->all(), $id);

        Flash::success('Firma updated successfully.');

        return redirect(route('firmas.index'));
    }

    /**
     * Remove the specified Firma from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $firma = $this->firmaRepository->find($id);

        if (empty($firma)) {
            Flash::error('Firma not found');

            return redirect(route('firmas.index'));
        }

        $this->firmaRepository->delete($id);

        Flash::success('Firma deleted successfully.');

        return redirect(route('firmas.index'));
    }
}
