<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePensionRequest;
use App\Http\Requests\UpdatePensionRequest;
use App\Repositories\PensionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PensionController extends AppBaseController
{
    /** @var  PensionRepository */
    private $pensionRepository;

    public function __construct(PensionRepository $pensionRepo)
    {
        $this->pensionRepository = $pensionRepo;
    }

    /**
     * Display a listing of the Pension.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pensions = $this->pensionRepository->all();

        return view('pensions.index')
            ->with('pensions', $pensions);
    }

    /**
     * Show the form for creating a new Pension.
     *
     * @return Response
     */
    public function create()
    {
        return view('pensions.create');
    }

    /**
     * Store a newly created Pension in storage.
     *
     * @param CreatePensionRequest $request
     *
     * @return Response
     */
    public function store(CreatePensionRequest $request)
    {
        $input = $request->all();

        $pension = $this->pensionRepository->create($input);

        Flash::success('Pension añadida.');

        return redirect(route('pensions.index'));
    }

    /**
     * Display the specified Pension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pension = $this->pensionRepository->find($id);

        if (empty($pension)) {
            Flash::error('Pension not found');

            return redirect(route('pensions.index'));
        }

        return view('pensions.show')->with('pension', $pension);
    }

    /**
     * Show the form for editing the specified Pension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pension = $this->pensionRepository->find($id);

        if (empty($pension)) {
            Flash::error('Pension not found');

            return redirect(route('pensions.index'));
        }

        return view('pensions.edit')->with('pension', $pension);
    }

    /**
     * Update the specified Pension in storage.
     *
     * @param int $id
     * @param UpdatePensionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePensionRequest $request)
    {
        $pension = $this->pensionRepository->find($id);

        if (empty($pension)) {
            Flash::error('Pension not found');

            return redirect(route('pensions.index'));
        }

        $pension = $this->pensionRepository->update($request->all(), $id);

        Flash::success('Pension actualizada.');

        return redirect(route('pensions.index'));
    }

    /**
     * Remove the specified Pension from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pension = $this->pensionRepository->find($id);

        if (empty($pension)) {
            Flash::error('Pension not found');

            return redirect(route('pensions.index'));
        }

        $this->pensionRepository->delete($id);

        Flash::success('Pension eliminada.');

        return redirect(route('pensions.index'));
    }
}
