<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBancoCollectRequest;
use App\Http\Requests\UpdateBancoCollectRequest;
use App\Repositories\BancoCollectRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class BancoCollectController extends AppBaseController
{
    /** @var  BancoCollectRepository */
    private $bancoCollectRepository;

    public function __construct(BancoCollectRepository $bancoCollectRepo)
    {
        $this->bancoCollectRepository = $bancoCollectRepo;
    }

    /**
     * Display a listing of the BancoCollect.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $bancoCollects = $this->bancoCollectRepository->all();

        return view('banco_collects.index')
            ->with('bancoCollects', $bancoCollects);
    }

    /**
     * Show the form for creating a new BancoCollect.
     *
     * @return Response
     */
    public function create()
    {
        return view('banco_collects.create');
    }

    /**
     * Store a newly created BancoCollect in storage.
     *
     * @param CreateBancoCollectRequest $request
     *
     * @return Response
     */
    public function store(CreateBancoCollectRequest $request)
    {
        $input = $request->all();

        $bancoCollect = $this->bancoCollectRepository->create($input);

        Flash::success('Banco Collect saved successfully.');

        return redirect(route('bancoCollects.index'));
    }

    /**
     * Display the specified BancoCollect.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bancoCollect = $this->bancoCollectRepository->find($id);

        if (empty($bancoCollect)) {
            Flash::error('Banco Collect not found');

            return redirect(route('bancoCollects.index'));
        }

        return view('banco_collects.show')->with('bancoCollect', $bancoCollect);
    }

    /**
     * Show the form for editing the specified BancoCollect.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bancoCollect = $this->bancoCollectRepository->find($id);

        if (empty($bancoCollect)) {
            Flash::error('Banco Collect not found');

            return redirect(route('bancoCollects.index'));
        }

        return view('banco_collects.edit')->with('bancoCollect', $bancoCollect);
    }

    /**
     * Update the specified BancoCollect in storage.
     *
     * @param int $id
     * @param UpdateBancoCollectRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBancoCollectRequest $request)
    {
        $bancoCollect = $this->bancoCollectRepository->find($id);

        if (empty($bancoCollect)) {
            Flash::error('Banco Collect not found');

            return redirect(route('bancoCollects.index'));
        }

        $bancoCollect = $this->bancoCollectRepository->update($request->all(), $id);

        Flash::success('Banco Collect updated successfully.');

        return redirect(route('bancoCollects.index'));
    }

    /**
     * Remove the specified BancoCollect from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bancoCollect = $this->bancoCollectRepository->find($id);

        if (empty($bancoCollect)) {
            Flash::error('Banco Collect not found');

            return redirect(route('bancoCollects.index'));
        }

        $this->bancoCollectRepository->delete($id);

        Flash::success('Banco Collect deleted successfully.');

        return redirect(route('bancoCollects.index'));
    }
}
