<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrgRequest;
use App\Http\Requests\UpdateOrgRequest;
use App\Models\Org;
use App\Repositories\OrgRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OrgController extends AppBaseController
{
    /** @var  OrgRepository */
    private $orgRepository;

    public function __construct(OrgRepository $orgRepo)
    {
        $this->orgRepository = $orgRepo;
    }

    /**
     * Display a listing of the Org.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orgs = $this->orgRepository->all();

        return view('orgs.index')
            ->with('orgs', $orgs);
    }

    /**
     * Show the form for creating a new Org.
     *
     * @return Response
     */
    public function create()
    {
        return view('orgs.create');
    }

    /**
     * Store a newly created Org in storage.
     *
     * @param CreateOrgRequest $request
     *
     * @return Response
     */
    public function store(CreateOrgRequest $request)
    {
        $input = $request->all();

        $org = $this->orgRepository->create($input);

        Flash::success('Org saved successfully.');

        return redirect(route('orgs.index'));
    }

    /**
     * Display the specified Org.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $org = $this->orgRepository->find($id);

        if (empty($org)) {
            Flash::error('Org not found');

            return redirect(route('orgs.index'));
        }

        return view('orgs.show')->with('org', $org);
    }

    /**
     * Show the form for editing the specified Org.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $org = $this->orgRepository->find($id);

        if (empty($org)) {
            Flash::error('Org not found');

            return redirect(route('orgs.index'));
        }

        return view('orgs.edit')->with('org', $org);
    }

    /**
     * Update the specified Org in storage.
     *
     * @param int $id
     * @param UpdateOrgRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrgRequest $request)
    {
        $org = $this->orgRepository->find($id);

        if (empty($org)) {
            Flash::error('Org not found');

            return redirect(route('orgs.index'));
        }

        $org = $this->orgRepository->update($request->all(), $id);

        Flash::success('Org updated successfully.');

        return redirect(route('orgs.index'));
    }

    /**
     * Remove the specified Org from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $org = $this->orgRepository->find($id);

        if (empty($org)) {
            Flash::error('Org not found');

            return redirect(route('orgs.index'));
        }

        $this->orgRepository->delete($id);

        Flash::success('Org deleted successfully.');

        return redirect(route('orgs.index'));
    }

    public function  cambio(Org $id){
        if($id->Prod){
            $id->Prod = False;
        }else{
            $id->Prod = True;
        }
        $id->save();
        return redirect(route('orgs.index'));
    }
}
