<?php

namespace App\Http\Controllers;

use App\Models\Factura;
use App\Models\Ticket;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $date = new DateTime();
        $date->modify('+1 day');
        $date1 = new DateTime();
        $date1->modify('-1 day');
        $hoy = new DateTime();
        $dosDias = new DateTime();
        $dosDias->modify('-2 day');
        //DB::enableQueryLog(); // Enable query log
        $ticket = Ticket::whereBetween('created_at', [($dosDias->format('Y-m-d') . ' 23:59:00'), ($date->format('Y-m-d') . ' 00:00:00')])->count();

        //dd($date1,$date,$ticket);
        $factura = Ticket::whereBetween('created_at', [($dosDias->format('Y-m-d') . ' 23:59:00'), ($date->format('Y-m-d') . ' 00:00:00')])
            ->where('estatus', '=', 'valido')
            ->count();
        $rechazo = Ticket::whereBetween('created_at', [($dosDias->format('Y-m-d') . ' 23:59:00'), ($date->format('Y-m-d') . ' 00:00:00')])
            ->where('estatus', '=', 'Rechazo')
            ->count();
        $facturado = Factura::whereBetween('fecha_timbrado', [($dosDias->format('Y-m-d') . ' 23:59:00'), ($date->format('Y-m-d') . ' 00:00:00')])
            ->sum('total_factura');
        $est = DB::table('autof_tickets')
            ->join('parks', 'autof_tickets.id_est', '=', 'parks.no_est')
            ->where('estatus', '=', 'valido')
            ->whereBetween('autof_tickets.created_at', [($dosDias->format('Y-m-d') . ' 23:59:00'), ($date->format('Y-m-d') . ' 00:00:00')])
            ->select('parks.nombre', DB::raw('count(*) as fact'))
            ->groupBy('parks.nombre')
            ->orderBy('fact', 'desc')
            ->take(7)
            ->get();
        $graf = $ticket - $factura - $rechazo;
        //dd($graf,$ticket,$factura,$rechazo,$ticket_ayer,$ticketac_ayer,$Rechazo_ayer);
        $ticket_mes = Ticket::whereMonth('created_at', '=', $hoy->format('m'))->count();
        $rechazo_mes = Ticket::whereMonth('created_at', '=', $hoy->format('m'))
            ->where('estatus', '=', 'Rechazo')
            ->count();
        $facturas_mes = Ticket::whereMonth('created_at', '=', $hoy->format('m'))
            ->where('estatus', '=', 'valido')
            ->count();
        $graf_mes = $ticket_mes - $rechazo_mes - $facturas_mes;
        $est_mes = DB::table('autof_tickets')
            ->join('parks', 'autof_tickets.id_est', '=', 'parks.no_est')
            ->where('estatus', '=', 'valido')
            ->whereMonth('autof_tickets.created_at', $hoy->format('m'))
            ->select('parks.nombre', DB::raw('count(*) as fact'))
            ->groupBy('parks.nombre')
            ->orderBy('fact', 'desc')
            ->take(7)
            ->get();
        $est_menos = DB::table('autof_tickets')
            ->join('parks', 'autof_tickets.id_est', '=', 'parks.no_est')
            ->where('estatus', '=', 'valido')
            ->whereMonth('autof_tickets.created_at', $hoy->format('m'))
            ->select('parks.nombre', DB::raw('count(*) as fact'))
            ->groupBy('parks.nombre')
            ->orderBy('fact', 'asc')
            ->take(7)
            ->get();
        $facturado_mes = Factura::whereMonth('fecha_timbrado', '=', $hoy->format('m'))
            ->sum('total_factura');
        //SELECT YEAR(created_at) AS AÑO,
        //MONTH(created_at) AS MES,
        //MONTHNAME(created_at) AS NOMBRE,
        //COUNT(factura) AS Total_Facturas
        //FROM autof_tickets
        //GROUP BY AÑO DESC, MES DESC;
        /*$ticket_graf = Ticket::select(
            DB::raw('YEAR(created_at) AS AÑO'),
            DB::raw('MONTH(created_at) AS MES'),
            DB::raw('COUNT(*) AS Total_Tickets'),
            DB::raw('MONTHNAME(created_at) AS NOMBRE')
        )->groupBy(DB::raw('AÑO asc'), DB::raw(' MES asc'), DB::raw('NOMBRE'))->toSql()*/
        $desglose = "select YEAR(created_at) AS AÑO, MONTH(created_at) AS MES, COUNT(*) AS Total_Tickets, MONTHNAME(created_at) AS NOMBRE
            from autof_tickets where autof_tickets.deleted_at is null group by AÑO,  MES, NOMBRE order by AÑO ASC, MES ASC, NOMBRE ASC;";
        $ticket_graf = DB::SELECT($desglose);
        //dd($ticket_graf);
        //dd(DB::getQueryLog()); // Show results of log
        /*$fact_graf = Factura::select(
            DB::raw('YEAR(created_at) AS AÑO'),
            DB::raw('MONTH(created_at) AS MES'),
            DB::raw('COUNT(*) AS Total_Tickets'),
            DB::raw('MONTHNAME(created_at) AS NOMBRE')
        )->groupBy(DB::raw('AÑO asc'), DB::raw(' MES asc'), DB::raw('NOMBRE'))
            ->toSql();*/
        $desglose1 = "select YEAR(created_at) AS AÑO, MONTH(created_at) AS MES, COUNT(*) AS Total_Tickets,
        MONTHNAME(created_at) AS NOMBRE from auto_facts
        where auto_facts.deleted_at is null group by AÑO,  MES, NOMBRE ORDER BY AÑO ASC, MES ASC, NOMBRE ASC;";
        $fact_graf = DB::SELECT($desglose1);
        return view('Dashboard.dashboard')
            ->with('facturas', $factura)
            ->with('tickets', ($ticket))
            ->with('rechazos', ($rechazo))
            ->with('facturado', $facturado)
            ->with('est', $est)
            ->with('graf', $graf)
            ->with('facturas_mes', $facturas_mes)
            ->with('tickets_mes', $ticket_mes)
            ->with('rechazo_mes', $rechazo_mes)
            ->with('facturado_mes', $facturado_mes)
            ->with('est_mes', $est_mes)
            ->with('graf_mes', $graf_mes)
            ->with('est_menos', $est_menos)
            ->with('ticket_graf', $ticket_graf)
            ->with('fact_graf', $fact_graf);
    }

    public function AjaxRequest()
    {
        $date = new DateTime();
        $date->modify('+1 day');
        $date1 = new DateTime();
        $date1->modify('-1 day');
        $factura = Factura::whereBetween('fecha_timbrado', [($date1->format('Y-m-d') . ' 23:59:00'), ($date->format('Y-m-d') . ' 00:00:00')])->count();
        $ticket = Ticket::whereBetween('created_at', [($date1->format('Y-m-d') . ' 23:59:00'), ($date->format('Y-m-d') . ' 00:00:00')])->count();
        $rechazo = Ticket::whereBetween('created_at', [($date1->format('Y-m-d') . ' 23:59:00'), ($date->format('Y-m-d') . ' 00:00:00')])
            ->where('estatus', '=', 'Rechazo')
            ->count();
        $facturado = Factura::whereBetween('fecha_timbrado', [($date1->format('Y-m-d') . ' 23:59:00'), ($date->format('Y-m-d') . ' 00:00:00')])
            ->sum('total_factura');
        $est = DB::table('autof_tickets')
            ->join('parks', 'autof_tickets.id_est', '=', 'parks.no_est')
            ->where('estatus', '=', 'valido')
            ->whereBetween('autof_tickets.created_at', [($date1->format('Y-m-d') . ' 23:59:00'), ($date->format('Y-m-d') . ' 00:00:00')])
            ->select('parks.nombre', DB::raw('count(*) as fact'))
            ->groupBy('parks.nombre')
            ->orderBy('fact', 'desc')
            ->take(7)
            ->get();
        $graf = $ticket - $factura - $rechazo;
        return array($factura, $ticket, $rechazo, $facturado, $est, $graf);
    }

}
