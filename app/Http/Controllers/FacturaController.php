<?php

namespace App\Http\Controllers;

use App\Funciones\Timbrar;
use App\Http\Requests\CreateFacturaRequest;
use App\Http\Requests\UpdateFacturaRequest;
use App\Models\Estacionamiento;
use App\Models\Factura;
use App\Models\Ticket;
use App\Repositories\FacturaRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PHPMailer\PHPMailer\PHPMailer;
use Response;

class  FacturaController extends AppBaseController
{
    /** @var  FacturaRepository */
    private $facturaRepository;

    public function __construct(FacturaRepository $facturaRepo)
    {
        $this->facturaRepository = $facturaRepo;
    }

    /**
     * Display a listing of the Factura.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index()
    {
        $facturas = Factura::join('autof_tickets', 'autof_tickets.id', '=', 'auto_facts.id_ticketAF')
            ->select(['auto_facts.id',
                'auto_facts.serie',
                'auto_facts.folio',
                'auto_facts.fecha_timbrado',
                'auto_facts.subtotal_factura',
                'auto_facts.iva_factura',
                'auto_facts.total_factura',
                'autof_tickets.email',
                'autof_tickets.Razon_social'])
            ->orderBy('auto_facts.created_at', 'desc')
            ->paginate(10);
        $estacionamiento = Estacionamiento::select('no_est', 'nombre')
            ->get();
        return view('facturas.index')
            ->with('facturas', $facturas)
            ->with('estacionamientos', $estacionamiento)
            ->with('bus',True);
    }

    public function busqueda()
    {
        $input = request()->validate([
            "rs" => [''],
            "rfc" => [''],
            "total" => [''],
        ]);
        $facturas = Factura::query();
        $facturas = $facturas->join('autof_tickets', 'autof_tickets.id', '=', 'auto_facts.id_ticketAF')
            ->select(['auto_facts.id',
                'auto_facts.serie',
                'auto_facts.folio',
                'auto_facts.fecha_timbrado',
                'auto_facts.subtotal_factura',
                'auto_facts.iva_factura',
                'auto_facts.total_factura',
                'autof_tickets.email',
                'autof_tickets.Razon_social'])
            ->orderBy('auto_facts.created_at', 'desc');
        if ($input['rs'] != null) {
            $facturas = $facturas->where('Razon_social', '=', $input['rs']);
        }
        if ($input['rfc'] != null) {
            $facturas = $facturas->where('RFC', '=', $input['rfc']);
        }
        if ($input['total'] != null) {
            $facturas = $facturas->where('total_ticket', '=', $input['total']);
        }
        if($facturas->count() == 0){
            Flash::error('No se encontraron registros');
            return redirect(url('factura'));
        }
        $estacionamiento = Estacionamiento::select('no_est', 'nombre')
            ->get();
        return view('facturas.index')
            ->with('facturas', $facturas)
            ->with('estacionamientos', $estacionamiento)
            ->with('bus',false);
    }

    /**
     * Show the form for creating a new Factura.
     *
     * @return Response
     */
    public function create()
    {
        /*$request = request()->all();
        dd($request);*/
        $input = request()->validate([
            "estacionamiento" => ['required']
        ]);

        $estacionamiento = $input['estacionamiento'];

        $estacionamiento_info = Estacionamiento::join("orgs", "parks.id_org", "=", "orgs.id")
            ->where('no_est', '=', $estacionamiento)
            ->select(['orgs.*', 'parks.no_est', 'parks.nombre'])
            ->get();


        return view('facturas.create')
            ->with('estacionamiento', $estacionamiento_info);
    }

    /**
     * Store a newly created Factura in storage.
     *
     * @param CreateFacturaRequest $request
     *
     * @return Response
     */
    public function store()
    {
        $input = request()->all();
        dd($input);

        //$input = $request->all();

        $factura = $this->facturaRepository->create($input);

        Flash::success('Factura saved successfully.');

        return redirect(route('facturas.index'));
    }

    /**
     * Display the specified Factura.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $factura = $this->facturaRepository->find($id);

        if (empty($factura)) {
            Flash::error('Factura not found');

            return redirect(route('facturas.index'));
        }

        return view('facturas.show')->with('factura', $factura);
    }

    /**
     * Show the form for editing the specified Factura.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $factura = $this->facturaRepository->find($id);

        if (empty($factura)) {
            Flash::error('Factura not found');

            return redirect(route('facturas.index'));
        }

        return view('facturas.edit')->with('factura', $factura);
    }

    /**
     * Update the specified Factura in storage.
     *
     * @param int $id
     * @param UpdateFacturaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFacturaRequest $request)
    {
        $factura = $this->facturaRepository->find($id);

        if (empty($factura)) {
            Flash::error('Factura not found');

            return redirect(route('facturas.index'));
        }

        $factura = $this->facturaRepository->update($request->all(), $id);

        Flash::success('Factura updated successfully.');

        return redirect(route('facturas.index'));
    }

    /**
     * Remove the specified Factura from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $factura = $this->facturaRepository->find($id);

        if (empty($factura)) {
            Flash::error('Factura not found');

            return redirect(route('facturas.index'));
        }

        $this->facturaRepository->delete($id);

        Flash::success('Factura deleted successfully.');

        return redirect(route('facturas.index'));
    }

    public function downloadXML($id)
    {
        //$id = \request()->request;
        //dd($id);
        $xml = Factura::find($id);
        $archivo = $xml->XML;
        $uuid = $xml->uuid;
        $path = "xml_down/" . $uuid . ".xml";
        //dd($uuid);
        file_put_contents($path, $archivo);
        header("Content-disposition: attachment; filename=$uuid.xml");
        header("Content-type: application/xml");
        readfile($path);
        unlink($path);

        //Flash::success('Factura descargada');

        //return redirect(route('facturas.index'));
    }

    public function downloadPDF($id)
    {
        //$id = \request()->request;
        //dd($id);
        $pdf = Factura::find($id);
        $archivo = $pdf->PDF;
        $uuid = $pdf->uuid;

        $decoded = base64_decode($archivo);
        $path = "xml_down/" . $uuid . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=$uuid.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);

        //Flash::success('Factura descargada');

        //return redirect(route('facturas.index'));
    }

    public function enviarEmail()
    {
        $input = request()->validate([
            "tag" => ['integer', 'required'],
            "correo" => ['email', 'required'],
        ]);

        $correo = $input['correo'];
        $id = $input['tag'];

        $xml = Factura::find($id);
        $archivo = $xml->XML;
        $uuid = $xml->uuid;
        $path1 = "xml_down/" . $uuid . ".xml";
        //dd($uuid);
        file_put_contents($path1, $archivo);

        $pdf = Factura::find($id);
        $archivo = $pdf->PDF;
        $uuid = $pdf->uuid;
        $decoded = base64_decode($archivo);
        $path2 = "xml_down/" . $uuid . ".pdf";
        file_put_contents($path2, $decoded);

        $serie = $pdf->serie;

        $mail = new PHPMailer(true);
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = 'smtp.office365.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "facturacion-oce@central-mx.com";
        $mail->Password = "1t3gr4d0r2020*";
        $mail->setFrom('facturacion-oce@central-mx.com', 'Operadora central de estacionamientos');
        //$mail->From = "no-reply@central-mx.com";
        //$mail->FromName = "Central operadora de estacionamientos";
        $mail->Subject = "Factura" . $serie;
        $mail->Body = "<html>
                    <meta charset='utf-8'>
                    <h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
                    <p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: " . $uuid . " , Serie: " . $serie . ". Así como su representación impresa.</p>
                    <p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>
                    <p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico " . $correo . " a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p>
                    <p align='center;'>Estimado usuario le recomendamos realizar sus facturas en un lapso no mayor a 5 días hábiles</p>
                    <p align='center;'>Contactanos</p>
                    <p align='center;'>Teléfono: (55) 3640-3900</p>
                    <p align='center;'>Correo: facturacion@central-mx.com</p>
                    </html>";
        $mail->AddAddress($correo, $uuid);
        $archivo = $path1;
        $pdf1 = $path2;
        $mail->AddAttachment($archivo);
        $mail->AddAttachment($pdf1);
        $mail->IsHTML(true);
        $mail->Send();

        unlink($path1);
        unlink($path2);


        Flash::success('Correo enviado satisfactoriamente.');

        return redirect(route('factura.index'));
    }

    public function timbrado()
    {
        //dd("Empieza timbrado");
        $img = '';
        $fact = request()->validate([
            'no_est' => ['required'],
            'razonsocial' => ['required', 'string'],
            'rfc_empresa' => ['required', 'string'],
            'regimen_empresa' => ['required', 'string'],
            'Nombre_cliente' => ['required', 'string'],
            'RFC' => ['required', 'string', 'max:13'],
            'Correo' => ['required', 'email'],
            'Uso_CFDI' => ['required'],
            'concepto' => ['required'],
            'metodo_pago' => ['required'],
            'forma_pago' => ['required'],
            'Cantidad' => ['required', 'string'],
            'Precio' => ['required', 'string'],
            'Comentario' => ['required', 'string'],
        ]);

        //dd($fact);

        $cadena = $fact['RFC'];
        //dd($cadena);
        $RFC = trim(strtoupper($cadena));

        $foto = request()->file('foto');
        //dd($foto);

        if ($foto != null) {
            $dataImg = $foto->get();
            $nombre_archivo = $foto->getBasename();
            $image = imagecreatefromstring($dataImg);
            ob_start();
            imagejpeg($image, NULL, 100);
            $cont = ob_get_contents();
            ob_end_clean();
            imagedestroy($image);
            $content = imagecreatefromstring($cont);
            $output = 'Facturas/' . $nombre_archivo . '.webp';
            imagewebp($content, $output);
            imagedestroy($content);

            $im_webp = file_get_contents($output);

            $img = base64_encode($im_webp);
            unlink($output);
            //dd($img);
        }
        $fecha = date('Y-m-d');
        $fecha2 = date('H:i:s');

        $numfact = Ticket::max('factura');
        $RS = $fact['Nombre_cliente'];
        $email = $fact['Correo'];
        //dd($RS, $email);
        //DB::enableQueryLog();
        $ticket = new Ticket();
        $ticket->fill([
            'id_cliente' => 1,
            'id_est' => $fact['no_est'],
            'id_tipo' => 3,
            'id_CentralUser' => Auth::User()->id,
            'factura' => $numfact + 1,
            'total_ticket' => $fact['Precio'],
            'fecha_emision' => $fecha,
            'estatus' => 'validar',
            'UsoCFDI' => $fact['Uso_CFDI'],
            'metodo_pago' => $fact['metodo_pago'],
            'forma_pago' => $fact['forma_pago'],
            'RFC' => $RFC,
            'Razon_social' => $RS,
            'email' => $email,
            'imagen' => $img
        ]);
        //dd(DB::getQueryLog());
        $ticket->save();
        $timbrar = new Timbrar();
        $timbrar->Factura($ticket);
        return redirect(route('factura.index'));
    }//termina funcion

    public function pi()
    {
        $tickets = DB::table('autof_tickets')
            ->join('parks', 'parks.no_est', '=', 'autof_tickets.id_est')
            ->where('estatus', '=', 'factura_pi')
            ->select('autof_tickets.id', 'autof_tickets.id_cliente', 'autof_tickets.id_est', 'autof_tickets.id_tipo', 'autof_tickets.id_CentralUser', 'autof_tickets.factura', 'autof_tickets.no_ticket', 'autof_tickets.total_ticket', 'autof_tickets.fecha_emision', 'autof_tickets.estatus', 'autof_tickets.UsoCFDI', 'autof_tickets.metodo_pago', 'autof_tickets.forma_pago', 'autof_tickets.RFC', 'autof_tickets.Razon_social', 'autof_tickets.email', 'parks.nombre as park')
            ->orderBy('id')
            ->paginate(20);
        $est = Estacionamiento::select('no_est', 'nombre')
            ->get();
        return view('facturas.pi')
            ->with('tickets', $tickets)
            ->with('est', $est);
    }
}
