<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOperacionDiariaRequest;
use App\Http\Requests\UpdateOperacionDiariaRequest;
use App\Repositories\OperacionDiariaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OperacionDiariaController extends AppBaseController
{
    /** @var  OperacionDiariaRepository */
    private $operacionDiariaRepository;

    public function __construct(OperacionDiariaRepository $operacionDiariaRepo)
    {
        $this->operacionDiariaRepository = $operacionDiariaRepo;
    }

    /**
     * Display a listing of the OperacionDiaria.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $operacionDiarias = $this->operacionDiariaRepository->all();

        return view('operacion_diarias.index')
            ->with('operacionDiarias', $operacionDiarias);
    }

    /**
     * Show the form for creating a new OperacionDiaria.
     *
     * @return Response
     */
    public function create()
    {
        return view('operacion_diarias.create');
    }

    /**
     * Store a newly created OperacionDiaria in storage.
     *
     * @param CreateOperacionDiariaRequest $request
     *
     * @return Response
     */
    public function store(CreateOperacionDiariaRequest $request)
    {
        $input = $request->all();

        $operacionDiaria = $this->operacionDiariaRepository->create($input);

        Flash::success('Operacion Diaria saved successfully.');

        return redirect(route('operacionDiarias.index'));
    }

    /**
     * Display the specified OperacionDiaria.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $operacionDiaria = $this->operacionDiariaRepository->find($id);

        if (empty($operacionDiaria)) {
            Flash::error('Operacion Diaria not found');

            return redirect(route('operacionDiarias.index'));
        }

        return view('operacion_diarias.show')->with('operacionDiaria', $operacionDiaria);
    }

    /**
     * Show the form for editing the specified OperacionDiaria.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $operacionDiaria = $this->operacionDiariaRepository->find($id);

        if (empty($operacionDiaria)) {
            Flash::error('Operacion Diaria not found');

            return redirect(route('operacionDiarias.index'));
        }

        return view('operacion_diarias.edit')->with('operacionDiaria', $operacionDiaria);
    }

    /**
     * Update the specified OperacionDiaria in storage.
     *
     * @param int $id
     * @param UpdateOperacionDiariaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOperacionDiariaRequest $request)
    {
        $operacionDiaria = $this->operacionDiariaRepository->find($id);

        if (empty($operacionDiaria)) {
            Flash::error('Operacion Diaria not found');

            return redirect(route('operacionDiarias.index'));
        }

        $operacionDiaria = $this->operacionDiariaRepository->update($request->all(), $id);

        Flash::success('Operacion Diaria updated successfully.');

        return redirect(route('operacionDiarias.index'));
    }

    /**
     * Remove the specified OperacionDiaria from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $operacionDiaria = $this->operacionDiariaRepository->find($id);

        if (empty($operacionDiaria)) {
            Flash::error('Operacion Diaria not found');

            return redirect(route('operacionDiarias.index'));
        }

        $this->operacionDiariaRepository->delete($id);

        Flash::success('Operacion Diaria deleted successfully.');

        return redirect(route('operacionDiarias.index'));
    }
}
