<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateImgIngresoRequest;
use App\Http\Requests\UpdateImgIngresoRequest;
use App\Repositories\ImgIngresoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ImgIngresoController extends AppBaseController
{
    /** @var  ImgIngresoRepository */
    private $imgIngresoRepository;

    public function __construct(ImgIngresoRepository $imgIngresoRepo)
    {
        $this->imgIngresoRepository = $imgIngresoRepo;
    }

    /**
     * Display a listing of the ImgIngreso.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $imgIngresos = $this->imgIngresoRepository->all();

        return view('img_ingresos.index')
            ->with('imgIngresos', $imgIngresos);
    }

    /**
     * Show the form for creating a new ImgIngreso.
     *
     * @return Response
     */
    public function create()
    {
        return view('img_ingresos.create');
    }

    /**
     * Store a newly created ImgIngreso in storage.
     *
     * @param CreateImgIngresoRequest $request
     *
     * @return Response
     */
    public function store(CreateImgIngresoRequest $request)
    {
        $input = $request->all();

        $imgIngreso = $this->imgIngresoRepository->create($input);

        Flash::success('Img Ingreso saved successfully.');

        return redirect(route('imgIngresos.index'));
    }

    /**
     * Display the specified ImgIngreso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $imgIngreso = $this->imgIngresoRepository->find($id);

        if (empty($imgIngreso)) {
            Flash::error('Img Ingreso not found');

            return redirect(route('imgIngresos.index'));
        }

        return view('img_ingresos.show')->with('imgIngreso', $imgIngreso);
    }

    /**
     * Show the form for editing the specified ImgIngreso.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $imgIngreso = $this->imgIngresoRepository->find($id);

        if (empty($imgIngreso)) {
            Flash::error('Img Ingreso not found');

            return redirect(route('imgIngresos.index'));
        }

        return view('img_ingresos.edit')->with('imgIngreso', $imgIngreso);
    }

    /**
     * Update the specified ImgIngreso in storage.
     *
     * @param int $id
     * @param UpdateImgIngresoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateImgIngresoRequest $request)
    {
        $imgIngreso = $this->imgIngresoRepository->find($id);

        if (empty($imgIngreso)) {
            Flash::error('Img Ingreso not found');

            return redirect(route('imgIngresos.index'));
        }

        $imgIngreso = $this->imgIngresoRepository->update($request->all(), $id);

        Flash::success('Img Ingreso updated successfully.');

        return redirect(route('imgIngresos.index'));
    }

    /**
     * Remove the specified ImgIngreso from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $imgIngreso = $this->imgIngresoRepository->find($id);

        if (empty($imgIngreso)) {
            Flash::error('Img Ingreso not found');

            return redirect(route('imgIngresos.index'));
        }

        $this->imgIngresoRepository->delete($id);

        Flash::success('Img Ingreso deleted successfully.');

        return redirect(route('imgIngresos.index'));
    }
}
