<?php

namespace App\Http\Controllers;

use App\Funciones\Timbrar;
use App\Http\Requests\CreateTicketRequest;
use App\Http\Requests\UpdateTicketRequest;
use App\Models\Estacionamiento;
use App\Models\Ticket;
use App\Repositories\TicketRepository;
use Illuminate\Support\Facades\Response;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPMailer\PHPMailer\PHPMailer;


class TicketController extends AppBaseController
{
    /** @var  TicketRepository */
    private $ticketRepository;

    public function __construct(TicketRepository $ticketRepo)
    {
        $this->ticketRepository = $ticketRepo;
    }

    /**
     * Display a listing of the Ticket.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tickets = DB::table('autof_tickets')
            ->join('parks', 'parks.no_est', '=', 'autof_tickets.id_est')
            ->where('estatus', '=', 'validar')
            ->orWhere('estatus', '=', 'sin_fact')
            ->select('autof_tickets.id', 'autof_tickets.id_cliente', 'autof_tickets.id_est', 'autof_tickets.id_tipo', 'autof_tickets.id_CentralUser', 'autof_tickets.factura', 'autof_tickets.no_ticket', 'autof_tickets.total_ticket', 'autof_tickets.fecha_emision', 'autof_tickets.estatus', 'autof_tickets.UsoCFDI', 'autof_tickets.metodo_pago', 'autof_tickets.forma_pago', 'autof_tickets.RFC', 'autof_tickets.Razon_social', 'autof_tickets.email', 'parks.nombre as park')
            ->orderBy('id')
            ->paginate(30);
        //dd($tickets);
        $est = Estacionamiento::select('no_est', 'nombre')
            //->where('no_est', '!=', 1)
            ->get();
        return view('tickets.index')
            ->with('tickets', $tickets)
            ->with('est', $est);
    }


    public function list_rechazo()
    {
        $tickets = DB::table('autof_tickets')
            ->join('parks', 'parks.no_est', '=', 'autof_tickets.id_est')
            ->where('estatus', '=', 'Rechazo')
            ->select('autof_tickets.id', 'autof_tickets.id_cliente', 'autof_tickets.id_est', 'autof_tickets.id_tipo', 'autof_tickets.id_CentralUser', 'autof_tickets.factura', 'autof_tickets.no_ticket', 'autof_tickets.total_ticket', 'autof_tickets.fecha_emision', 'autof_tickets.estatus', 'autof_tickets.UsoCFDI', 'autof_tickets.metodo_pago', 'autof_tickets.forma_pago', 'autof_tickets.RFC', 'autof_tickets.Razon_social', 'autof_tickets.email','autof_tickets.coment', 'parks.nombre as park')
            ->orderBy('autof_tickets.updated_at', 'desc')
            ->paginate(30);
        return view('tickets.rechazo')
            ->with('tickets', $tickets);
    }

    /**
     * Show the form for creating a new Ticket.
     *
     * @return Response
     */
    public function create()
    {
        return view('tickets.create');
    }

    /**
     * Store a newly created Ticket in storage.
     *
     * @param CreateTicketRequest $request
     *
     * @return Response
     */
    public function store(CreateTicketRequest $request)
    {
        $input = $request->all();

        $ticket = $this->ticketRepository->create($input);

        Flash::success('Ticket saved successfully.');

        return redirect(route('tickets.index'));
    }

    /**
     * Display the specified Ticket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ticket = $this->ticketRepository->find($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');

            return redirect(route('tickets.index'));
        }

        return view('tickets.show')->with('ticket', $ticket);
    }

    /**
     * Show the form for editing the specified Ticket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ticket = $this->ticketRepository->find($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');

            return redirect(route('tickets.index'));
        }

        return view('tickets.edit')->with('ticket', $ticket);
    }

    /**
     * Update the specified Ticket in storage.
     *
     * @param int $id
     * @param UpdateTicketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTicketRequest $request)
    {
        $ticket = $this->ticketRepository->find($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');

            return redirect(route('tickets.index'));
        }

        $ticket = $this->ticketRepository->update($request->all(), $id);

        Flash::success('Ticket updated successfully.');

        return redirect(route('tickets.index'));
    }

    /**
     * Remove the specified Ticket from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $ticket = $this->ticketRepository->find($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');

            return redirect(route('tickets.index'));
        }

        $this->ticketRepository->delete($id);

        Flash::success('Ticket deleted successfully.');

        return redirect(route('tickets.index'));
    }

    public function actualiza($id)
    {
        $input = \request()->validate([
            'no_est' => ['required', 'integer']
        ]);
        $actualiza = Ticket::find($id);
        $actualiza->id_est = $input['no_est'];
        $actualiza->estatus = 'validar';
        $actualiza->save();
        return redirect(route('tickets.index'));
    }

    public function valido(Ticket $id)
    {
        $validar = DB::table('auto_facts')->where('id_ticketAF', '=', $id->id)->get();
        if ($validar->isNotEmpty()) {
            Flash::error('Ticket timbrado previamente');
            return redirect(route('tickets.index'));
        }
        /*  Linea para que no se timbre estacionamiento 1
         * if ($id->id_est == 1) {
            Flash::error('Ticket sin actualizar estacionamiento');
            return redirect(route('tickets.index'));
        }*/
        $timbrar = new Timbrar();
        $timbrar->Factura($id);
        return redirect(route('tickets.index'));
    }

    public function rechazo(Ticket $id)
    {
        $input = \request()->validate([
            'motivo' => ['required', 'string']
        ]);
        $id->estatus = 'Rechazo';
        $id->coment = $input['motivo'];
        $id->save();
        $mail = new PHPMailer(true);
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "facturacion-oce@central-mx.com";
        $mail->Password = "1t3gr4d0r2020*";
        $mail->setFrom('facturacion-oce@central-mx.com', 'Operadora central de estacionamientos');
        //$mail->From = "no-reply@central-mx.com";
        //$mail->FromName = "Central operadora de estacionamientos";
        $mail->Subject = "Rechazo Factura";
        $mail->Body = "<html><h4 style='font-color: green;' align='center;'>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
<p align='center;'>Esta rechazando su factura por " . $input['motivo'] . "</p>
<p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>
<p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico " . $id->email . " a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p></html>";
        $mail->AddAddress($id->email, $id->Razon_social);
        $mail->IsHTML(true);
        $mail->Send();
        return redirect(route('tickets.index'));
    }

    public function DownImg($id)
    {
        $imagen = Ticket::find($id);
        //dd($imagen->imagen);
        $bin = base64_decode($imagen->imagen);
        $path = "xml_down/" . $id . ".png";
        //dd($path);
        // Obtain the original content (usually binary data)
        // Load GD resource from binary data
        $im = imageCreateFromString($bin);
        //dd($im);

// Make sure that the GD library was able to load the image
// This is important, because you should not miss corrupted or unsupported images
        if (!$im) {
            die('Base64 value is not a valid image');
        }

        file_put_contents($path, $bin);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($path).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path));
        flush(); // Flush system output buffer
        readfile($path);
        unlink($path);

    }

}
