<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEstacionamientoRequest;
use App\Http\Requests\UpdateEstacionamientoRequest;
use App\Models\Estacionamiento;
use App\Models\Factura;
use App\Models\Marcas;
use App\Models\Org;
use App\Repositories\EstacionamientoRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use App\Models\Ticket;

class EstacionamientoController extends AppBaseController
{
    /** @var  EstacionamientoRepository */
    private $estacionamientoRepository;

    public function __construct(EstacionamientoRepository $estacionamientoRepo)
    {
        $this->estacionamientoRepository = $estacionamientoRepo;
    }

    /**
     * Display a listing of the Estacionamiento.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $estacionamientos = DB::table('parks')
            ->join('marcas', 'parks.id_marca', '=', 'marcas.id')
            ->select('parks.no_est', 'parks.nombre', 'parks.dist_regio', 'parks.cp', 'parks.escuela', 'marcas.marca', 'parks.Facturable', 'parks.Automatico')
            ->where('no_est','!=',1)
            ->paginate(20);

        return view('estacionamientos.index')
            ->with('estacionamientos', $estacionamientos);
    }

    /**
     * Show the form for creating a new Estacionamiento.
     *
     * @return Response
     */
    public function create()
    {
        $marcas = Marcas::all();
        $org = Org::all();
        return view('estacionamientos.create')
            ->with('marcas', $marcas)
            ->with('org', $org);
    }

    /**
     * Store a newly created Estacionamiento in storage.
     *
     * @param CreateEstacionamientoRequest $request
     *
     * @return Response
     */
    public function store()
    {
        $input = request()->validate([
            'nombre' => ['required', 'string'],
            'no_est' => ['required', 'integer'],
            'pais' => ['required', 'string'],
            'edo_prov' => ['required', 'string'],
            "mun_cd" => ['required', 'string'],
            "colonia" => ['required', 'string'],
            "calles" => ['required', 'string'],
            "numeroExt" => ['required', 'string'],
            "cp" => ['required', 'string'],
            "distrito" => ['required', 'string'],
            "marca" => ['required', 'integer'],
            "organizacion" => ['required', 'integer'],
            "escuela" => ['required', 'integer'],
            'automatico' => ['required', 'integer'],
            'faturable' => ['required', 'integer'],
            "direccion" => ['required', 'string'],
            "latitud" => ['required', 'string'],
            "longitud" => ['required', 'string'],
        ]);
        $est = new Estacionamiento();
        $est->fill([
        'id_marca' => $input['marca'],
        'id_org' => $input['organizacion'],
        'nombre' => $input['nombre'],
        'dist_regio' => $input['distrito'],
        'direccion' => $input['direccion'],
        'calle' => $input['calles'],
        'no_ext' => $input['numeroExt'],
        'colonia' => $input['colonia'],
        'municipio' => $input['mun_cd'],
        'estado' => $input['edo_prov'],
        'pais' => $input['pais'],
        'cp' => $input['cp'],
        'latitud' => $input['latitud'],
        'longitud' => $input['longitud'],
        'Facturable' => $input['faturable'],
        'Automatico' => $input['automatico'],
        'serie' => 'J' . $input['no_est'],
        'escuela' => $input['escuela']
    ]);
        $est->no_est = $input['no_est'];
        $est->save();

        Flash::success('Estacionamiento saved successfully.');

        return redirect(route('estacionamientos.index'));
    }

    /**
     * Display the specified Estacionamiento.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$estacionamiento = $this->estacionamientoRepository->find($id);

        $estacionamiento = Estacionamiento::all()->where('no_est', '=', $id);
        $ticket = Ticket::where('id_est','=',$id)->paginate(20);
        $facturas = Factura::join('autof_tickets','autof_tickets.id','=','auto_facts.id_ticketAF')
            ->select(['auto_facts.*','autof_tickets.email'])
            ->paginate(20);


        if (empty($estacionamiento)) {
            Flash::error('Estacionamiento no encontrado');

            return redirect(route('estacionamientos.index'));
        }

        return view('estacionamientos.show')
            ->with('estacionamiento', $estacionamiento)
            ->with('ticket', $ticket)
            ->with('facturas', $facturas);
    }


    /**
     * Show the form for editing the specified Estacionamiento.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $estacionamiento = DB::table('parks')
            ->join('orgs', 'parks.id_org', '=', 'orgs.id')
            ->join('marcas', 'marcas.id', '=', 'parks.id_marca')
            ->where('no_est', '=', $id)
            ->get();
        $marcas = Marcas::all();
        if (empty($estacionamiento)) {
            Flash::error('Estacionamiento not found');

            return redirect(route('estacionamientos.index'));
        }
        return view('estacionamientos.edit')
            ->with('proyecto', $estacionamiento[0])
            ->with('marcas', $marcas);
    }

    /**
     * Update the specified Estacionamiento in storage.
     *
     * @param int $id
     * @param UpdateEstacionamientoRequest $request
     *
     * @return Response
     */
    public function update($id)
    {
        $input = \request()->validate([
            "pais" => ['required'],
            'edo_prov' => ['required'],
            'mun_cd' => ['required'],
            'colonia' => ['required'],
            'calles' => ['required'],
            'numeroExt' => ['required'],
            'cp' => ['required'],
            'direccion' => ['required'],
            'latitud' => ['required'],
            'longitud' => ['required'],
            'marca' => [''],
            'escuela'=> [''],
            'faturable'=> [''],
            'automatico'=> [''],
        ]);
        $estacionamiento =
            DB::table('parks')
            ->where('no_est','=',$id)
            ->get();
        if ($estacionamiento->isEmpty()) {
            Flash::error('Estacionamiento no encontrado');
            return redirect(route('estacionamientos.index'));
        }
        $update = DB::table('parks')
            ->where('no_est','=',$id)
            ->update([
                'pais' => $input['pais'],
                'estado'=> $input['edo_prov'],
                'municipio'=> $input['mun_cd'],
                'colonia'=> $input['colonia'],
                'calle'=> $input['calles'],
                'no_ext'=> $input['numeroExt'],
                'cp'=> $input['cp'],
                'direccion'=> $input['direccion'],
                'latitud'=> $input['latitud'],
                'longitud'=> $input['longitud'],
            ]);
        if($input['marca'] != null) {
            $up = DB::table('parks')
                ->where('no_est','=',$id)
                ->update(['id_marca' => $input['marca']]);
        }
        if($input['escuela'] != null){
            $up = DB::table('parks')
                ->where('no_est','=',$id)
                ->update(['escuela' => $input['escuela']]);
        }
        if($input['faturable'] != null){
            $up = DB::table('parks')
                ->where('no_est','=',$id)
                ->update(['Facturable' => $input['faturable']]);
        }
        if($input['automatico'] != null){
            $up = DB::table('parks')
                ->where('no_est','=',$id)
                ->update(['Automatico' => $input['automatico']]);
        }
        Flash::success('Estacionamiento Actualizado exitosamente.');
        return redirect(route('estacionamientos.index'));
    }

    /**
     * Remove the specified Estacionamiento from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $estacionamiento = Estacionamiento::where('no_est','=',$id)->first();
        if (empty($estacionamiento)) {
            Flash::error('Estacionamiento no encontrado');

            return redirect(route('estacionamientos.index'));
        }
        $sql = DB::table('parks')->where('no_est','=',$id)->delete();
        Flash::success('Estacionamiento eliminado exitosamente.');

        return redirect(route('estacionamientos.index'));
    }

    public function graficas($id)
    {
        //$estacionamiento = $this->estacionamientoRepository->find($id);
        $estacionamiento = Estacionamiento::where('no_est', '=', $id)->get();
        $rfc = DB::table('autof_tickets')
            ->select('RFC', DB::raw('count(*) as fact'))
            ->where('estatus', '=', 'valido')
            ->where('id_est', '=', $id)
            ->groupBy('RFC')
            ->orderBy('fact', 'desc')
            ->take(5)
            ->get();
        $hoy = date('Y-m-d');
        $total = DB::table('autof_tickets')
            //->whereDate('fecha_emision','=',$hoy)
            ->where('id_est','=',$id)
            ->count();
        $fact = Ticket::all()
            ->where('estatus','=','valido')
            ->where('id_est','=',$id)
            ->count();
        $facturado = DB::table('autof_tickets')
            ->join('parks','autof_tickets.id_est','=','parks.no_est')
            ->where('autof_tickets.estatus','=','valido')
            ->where('parks.no_est','=',$id)
            ->sum('total_ticket');
        //->get();
        //->sum('auto_facts.total_factura');

        $rechazo = Ticket::all()
            ->where('estatus','=','Rechazo')
            ->where('id_est','=',$id)
            ->count();
        $TotalGraf = $total-$rechazo-$fact;
        $tickets = Ticket::where('id_est','=',$id)->count();
        $facturas = Factura::join('autof_tickets','autof_tickets.id','=','auto_facts.id_ticketAF')
            ->where('autof_tickets.id_est','=',$id)
            ->count();
        $rechazos  = Ticket::where('id_est','=',$id)
            ->where('estatus','=','Rechazo')
            ->count();
        //dd($busqueda);

        if (empty($estacionamiento)) {
            Flash::error('Estacionamiento no encontrado');

            return redirect(route('estacionamientos.index'));
        }
        return view('estacionamientos.estadistica')
            ->with('estacionamiento', $estacionamiento[0])
            ->with('rfc', $rfc)
            ->with('total', $total)
            ->with('factura', $fact)
            ->with('facturado', $facturado)
            ->with('rechazo', $rechazo)
            ->with('TotalGrafica', $TotalGraf)
            ->with('facturas',$facturas)
            ->with('rechazos',$rechazos)
            ->with('tickets',$tickets);
    }
}
