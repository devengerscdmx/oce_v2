<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInfoClienteRequest;
use App\Http\Requests\UpdateInfoClienteRequest;
use App\Repositories\InfoClienteRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class InfoClienteController extends AppBaseController
{
    /** @var  InfoClienteRepository */
    private $infoClienteRepository;

    public function __construct(InfoClienteRepository $infoClienteRepo)
    {
        $this->infoClienteRepository = $infoClienteRepo;
    }

    /**
     * Display a listing of the InfoCliente.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $infoClientes = $this->infoClienteRepository->all();

        return view('info_clientes.index')
            ->with('infoClientes', $infoClientes);
    }

    /**
     * Show the form for creating a new InfoCliente.
     *
     * @return Response
     */
    public function create()
    {
        return view('info_clientes.create');
    }

    /**
     * Store a newly created InfoCliente in storage.
     *
     * @param CreateInfoClienteRequest $request
     *
     * @return Response
     */
    public function store(CreateInfoClienteRequest $request)
    {
        $input = $request->all();

        $infoCliente = $this->infoClienteRepository->create($input);

        Flash::success('Info Cliente saved successfully.');

        return redirect(route('infoClientes.index'));
    }

    /**
     * Display the specified InfoCliente.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $infoCliente = $this->infoClienteRepository->find($id);

        if (empty($infoCliente)) {
            Flash::error('Info Cliente not found');

            return redirect(route('infoClientes.index'));
        }

        return view('info_clientes.show')->with('infoCliente', $infoCliente);
    }

    /**
     * Show the form for editing the specified InfoCliente.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $infoCliente = $this->infoClienteRepository->find($id);

        if (empty($infoCliente)) {
            Flash::error('Info Cliente not found');

            return redirect(route('infoClientes.index'));
        }

        return view('info_clientes.edit')->with('infoCliente', $infoCliente);
    }

    /**
     * Update the specified InfoCliente in storage.
     *
     * @param int $id
     * @param UpdateInfoClienteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInfoClienteRequest $request)
    {
        $infoCliente = $this->infoClienteRepository->find($id);

        if (empty($infoCliente)) {
            Flash::error('Info Cliente not found');

            return redirect(route('infoClientes.index'));
        }

        $infoCliente = $this->infoClienteRepository->update($request->all(), $id);

        Flash::success('Info Cliente updated successfully.');

        return redirect(route('infoClientes.index'));
    }

    /**
     * Remove the specified InfoCliente from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $infoCliente = $this->infoClienteRepository->find($id);

        if (empty($infoCliente)) {
            Flash::error('Info Cliente not found');

            return redirect(route('infoClientes.index'));
        }

        $this->infoClienteRepository->delete($id);

        Flash::success('Info Cliente deleted successfully.');

        return redirect(route('infoClientes.index'));
    }
}
