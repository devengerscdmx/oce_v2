<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTipoPensionRequest;
use App\Http\Requests\UpdateTipoPensionRequest;
use App\Repositories\TipoPensionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class TipoPensionController extends AppBaseController
{
    /** @var  TipoPensionRepository */
    private $tipoPensionRepository;

    public function __construct(TipoPensionRepository $tipoPensionRepo)
    {
        $this->tipoPensionRepository = $tipoPensionRepo;
    }

    /**
     * Display a listing of the TipoPension.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tipoPensions = $this->tipoPensionRepository->all();

        return view('tipo_pensions.index')
            ->with('tipoPensions', $tipoPensions);
    }

    /**
     * Show the form for creating a new TipoPension.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_pensions.create');
    }

    /**
     * Store a newly created TipoPension in storage.
     *
     * @param CreateTipoPensionRequest $request
     *
     * @return Response
     */
    public function store(CreateTipoPensionRequest $request)
    {
        $input = $request->all();

        $tipoPension = $this->tipoPensionRepository->create($input);

        Flash::success('Tipo Pension saved successfully.');

        return redirect(route('tipoPensions.index'));
    }

    /**
     * Display the specified TipoPension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoPension = $this->tipoPensionRepository->find($id);

        if (empty($tipoPension)) {
            Flash::error('Tipo Pension not found');

            return redirect(route('tipoPensions.index'));
        }

        return view('tipo_pensions.show')->with('tipoPension', $tipoPension);
    }

    /**
     * Show the form for editing the specified TipoPension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoPension = $this->tipoPensionRepository->find($id);

        if (empty($tipoPension)) {
            Flash::error('Tipo Pension not found');

            return redirect(route('tipoPensions.index'));
        }

        return view('tipo_pensions.edit')->with('tipoPension', $tipoPension);
    }

    /**
     * Update the specified TipoPension in storage.
     *
     * @param int $id
     * @param UpdateTipoPensionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipoPensionRequest $request)
    {
        $tipoPension = $this->tipoPensionRepository->find($id);

        if (empty($tipoPension)) {
            Flash::error('Tipo Pension not found');

            return redirect(route('tipoPensions.index'));
        }

        $tipoPension = $this->tipoPensionRepository->update($request->all(), $id);

        Flash::success('Tipo Pension updated successfully.');

        return redirect(route('tipoPensions.index'));
    }

    /**
     * Remove the specified TipoPension from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoPension = $this->tipoPensionRepository->find($id);

        if (empty($tipoPension)) {
            Flash::error('Tipo Pension not found');

            return redirect(route('tipoPensions.index'));
        }

        $this->tipoPensionRepository->delete($id);

        Flash::success('Tipo Pension deleted successfully.');

        return redirect(route('tipoPensions.index'));
    }
}
