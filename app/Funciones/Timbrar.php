<?php

namespace App\Funciones;

use App\Models\Factura;
use App\Models\Org;
use App\Models\Ticket;
use Barryvdh\DomPDF\Facade as PDF;
use CfdiUtils\CadenaOrigen\DOMBuilder;
use CfdiUtils\XmlResolver\XmlResolver;
use Endroid\QrCode\QrCode;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 08/10/2019
 * Time: 10:33 AM
 */
class Timbrar
{
    public function Factura(Ticket $ticket)
    {
        $est = DB::table('parks')->where('no_est', '=', $ticket['id_est'])->get();
        $org = Org::find($est[0]->id_org);
        //dd($org);
        $fecha = date('Y-m-d');
        $fecha2 = date('H:i:s');
        $url = '';
        $token = '';
        $certificado = new \CfdiUtils\Certificado\Certificado($org->ruta_cer);
        $comprobanteAtributos = [
            'xmlns:cfdi' => 'http://www.sat.gob.mx/cfd/3',
            'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'LugarExpedicion' => $org->cp,
            'MetodoPago' => $ticket['metodo_pago'],
            'TipoDeComprobante' => 'I',
            'Total' => number_format($ticket['total_ticket'], 2),
            'Moneda' => 'MXN',
            'SubTotal' => number_format(($ticket['total_ticket']/1.16), 2),
            'FormaPago' => $ticket['forma_pago'],
            'Fecha' => $fecha . "T" . $fecha2,
            'Folio' => $est[0]->folio + 1,
            'Serie' => $est[0]->serie,
            'Version' => '3.3',
            'xsi:schemaLocation' => 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd',
        ];
        $creator = new \CfdiUtils\CfdiCreator33($comprobanteAtributos, $certificado);
        $comprobante = $creator->comprobante();
        $comprobante->addEmisor([
            'RegimenFiscal' => $org['Regimen_fiscal'],
            'Nombre' => $org['Razon_social'],
            'Rfc' => $org['RFC'],
        ]);
        $comprobante->addReceptor([
            'UsoCFDI' => $ticket['UsoCFDI'],
            'Nombre' => $ticket['Razon_social'],
            'Rfc' => trim($ticket['RFC']),
        ]);
        $comprobante->addConcepto([
            'ClaveProdServ' => '78111807',
            'Cantidad' => '1.00',
            'ClaveUnidad' => 'E48',
            'Unidad' => 'Unidad de servicio',
            'Descripcion' => 'Tarifas del Parqueadero',
            'ValorUnitario' => str_replace(",", "", number_format($ticket['total_ticket'] / 1.16, 2)),
            'Importe' => str_replace(",", "", number_format($ticket['total_ticket'] / 1.16, 2)),
        ])->addTraslado([
            'Importe' => str_replace(",", "", number_format(($ticket['total_ticket']/1.16)*.16, 2)),
            'TasaOCuota' => '0.160000',
            'TipoFactor' => 'Tasa',
            'Impuesto' => '002',
            'Base' => str_replace(",", "", number_format($ticket['total_ticket'] / 1.16, 2)),
        ]);
        $creator->addSumasConceptos(NULL, 2);
        $key = file_get_contents($org->ruta_pem);
        $creator->addSello($key, 'OCE94120');
        //$creator->addSello($key, Crypt::decryptString($org->pass));
        //$creator->addSello($key, 'OCE94120');
        //$creator->saveXml('text.xml');
        $xml = $creator->asXml();
        $xml2 = base64_encode($xml);
        $body = array(
            'xml' => $xml2,
        );
        if ($org->Prod) {
            $url = $org->URL_prod;
            $token = $org->token_prod;
        } else {
            $url = $org->URL_dev;
            $token = $org->token_dev;
        }
        $client = new Client([
            'base_uri' => $url,
        ]);
        try {
            $response = $client->post('v1/stamp', [
                //'debug' => TRUE,
                'headers' => [
                    'Accept' => 'application/json; charset=UTF-8',
                    'Authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/json',
                ],
                \GuzzleHttp\RequestOptions::JSON => $body
            ]);
            $val2 = json_decode($response->getBody()->getContents());
        } catch (ClientException  $e) {
            //dd('Excepción capturada: ',  $e->getRequest(), $e->getResponse()->getBody()->getContents(), 'Papu ayuda a presentar errores');
            //
            $error = json_decode($e->getResponse()->getBody()->getContents());
            //dd($error, $xml);
            $errores_timb = $error->data->details;
            foreach ($errores_timb as $value) {
                $validation = $value->validation;
                Flash::error($validation);
            }
            //dd($validation);
            //$salida = FALSE;
            return;
        }
        if ($val2->data->status == "previously_stamped" || $val2->data->status == "stamped") {
            $xml3 = $val2->data->document_info->xml;
            $uuid = $val2->data->stamp_info->uuid;
            $xml3 = base64_decode($xml3);
            file_put_contents("Facturas/" . $uuid . ".xml", $xml3);
            $xmlContent = file_get_contents("Facturas/" . $uuid . ".xml");
            $resolver = new XmlResolver();
            $location = $resolver->resolveCadenaOrigenLocation('3.3');
            $builder = new DOMBuilder();
            $cadenaorigen = $builder->build($xmlContent, $location);
            /**/
            $xmlContents = $xml3;
            $cfdi = \CfdiUtils\Cfdi::newFromString($xmlContents);
            $cfdi->getVersion(); // (string) 3.3
            $cfdi->getDocument(); // clon del objeto DOMDocument
            $cfdi->getSource(); // (string) <cfdi:Comprobante...
            $complemento = $cfdi->getNode(); // Nodo de trabajo del nodo cfdi:Comprobante
            $tfd = $complemento->searchNode('cfdi:Complemento', 'tfd:TimbreFiscalDigital');
            $iva = number_format(($ticket['total_ticket']/1.16)*.16, 2);
            $data = array(
                'org' => [
                    'RS' => $org['Razon_social'],
                    'RFC' => $org['RFC'],
                    'dir' => $org['dir']
                ],
                'client' => [
                    'RS' => $ticket['Razon_social'],
                    'RFC' => trim($ticket['RFC']),
                ],
                'fact' => [
                    'SF' => $est[0]->serie . ($est[0]->folio + 1),
                    'FF' => $uuid,
                    'CSD' => $complemento['NoCertificado'],
                    'FHE' => $complemento['Fecha'],
                    'FHC' => $tfd['FechaTimbrado'],
                    'NSAT' => $tfd['NoCertificadoSAT'],
                    'UC' => $ticket['UsoCFDI'],
                    'MP' => $ticket['metodo_pago'],
                    'FP' => $ticket['forma_pago'],
                    'RF' => $org['Regimen_fiscal'],
                ],
                'con' => [
                    'CA' => '1.00',
                    'UN' => 'Unidad de servicio',
                    'PU' => number_format($ticket['total_ticket'] / 1.16, 2),
                    'DE' => '0.00',
                    'IM' => number_format($ticket['total_ticket'] / 1.16, 2),
                    'IVA' => $iva,
                    'IEPS' => '0.00',
                    'IMP_IEPS' => '0.00',
                ],
                'TT' => [
                    'T' => number_format($ticket['total_ticket'], 2),
                    'ST' => number_format(($ticket['total_ticket']/1.16), 2),
                    'IVA' => $iva,
                ],
                'FC' => [
                    'CO' => $cadenaorigen,
                    'SSAT' => wordwrap($tfd['SelloSAT'], 120, "\n", true),
                    'SCFD' => wordwrap($tfd['SelloCFD'], 120, "\n", true),
                ]
            );
            $ochocarct = substr($uuid, -8);
            $qrCode = new QrCode('https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&id=' . $uuid . '&r' . $org['RFC'] . '&rr=' . $ticket['RFC'] . '&tt=' . number_format($ticket['total_ticket'], 2) . '0000');
            header('Content-Type: ' . $qrCode->getContentType());
            $qrCode->writeString();
            $ubiQr = "pdf-template/" . $uuid . "qrcode.png";
            $qrCode->writeFile($ubiQr);
            $pdf = PDF::loadView('PDF.pdf', compact('data'))->save("Facturas/" . $uuid . '.pdf');
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->SMTPDebug = 0;
            $mail->Debugoutput = 'html';
            /*$mail->Host = 'smtp.gmail.com';
            $mail->Port = 587;
            $mail->SMTPSecure = 'tls';
            $mail->SMTPAuth = true;
            $mail->Username = "ocefacturacion@gmail.com";
            $mail->Password = "Integrador1";
            $mail->setFrom('ocefacturacion@gmail.com', 'Central operadora de estacionamientos');*/
            $mail->Host = 'smtp.office365.com';
            $mail->Port = 587;
            $mail->SMTPSecure = 'tls';
            $mail->SMTPAuth = true;
            $mail->Username = "facturacion-oce@central-mx.com";
            $mail->Password = "1t3gr4d0r2020*";
            $mail->setFrom('facturacion-oce@central-mx.com', 'Operadora central de estacionamientos');
            //$mail->From = "no-reply@central-mx.com";
            //$mail->FromName = "Central operadora de estacionamientos";
            $mail->Subject = "Factura" . $est[0]->serie . ($est[0]->folio + 1);
            $mail->Body = "<html>
                <meta charset='utf-8'>
                <h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
                <p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: " . $uuid . " , Serie: " . $est[0]->serie . " y Folio: " . ($est[0]->folio + 1) . ". Así como su representación impresa.</p>
                <p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>
                <p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico " . $ticket['email'] . " a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p>
                <p align='center;'>Estimado usuario le recomendamos realizar sus facturas en un lapso no mayor a 5 días hábiles</p>
                <p align='center;'>Contactanos</p>
                <p align='center;'>Teléfono: (55) 3640-3900</p>
                <p align='center;'>Correo: facturacion@central-mx.com</p>
                </html>";
            $mail->AddAddress($ticket['email']);
            $archivo = "Facturas/" . $uuid . ".xml";
            $pdf1 = "Facturas/" . $uuid . '.pdf';
            $mail->AddAttachment($archivo);
            $mail->AddAttachment($pdf1);
            $mail->IsHTML(true);
            $mail->Send();
            $contenido = file_get_contents("Facturas/" . $uuid . '.pdf');
            $ticket->estatus = "valido";
            $ticket->save();
            DB::table('parks')->where('no_est', '=', $est[0]->no_est)->update(['folio' => $est[0]->folio + 1]);
            $factura = new Factura();
            $factura->fill([
                'serie' => $est[0]->serie,
                'tipo_doc' => 'I',
                'id_ticketAF' => $ticket['id'],
                'folio' => $est[0]->folio + 1,
                'fecha_timbrado' => date("Y-m-d H:i:s"),
                'uuid' => $uuid,
                'subtotal_factura' => str_replace(",", "",  number_format(($ticket['total_ticket']/1.16), 2)),
                'iva_factura' => str_replace(",", "", $iva),
                'total_factura' => str_replace(",", "", number_format($ticket['total_ticket'], 2)),
                'XML' => $xml3,
                'PDF' => base64_encode($contenido),
                'estatus' => 'timbrada'
            ]);
            $factura->save();
            //dd($data);
            unlink($archivo);
            unlink($pdf1);
            unlink($ubiQr);
            Flash::success('Factura generada con éxito');
        }
    }
}
