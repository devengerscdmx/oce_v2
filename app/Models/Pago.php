<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pago
 * @package App\Models
 * @version January 20, 2021, 1:52 am CST
 *
 * @property number $total
 * @property number $subtotal
 * @property number $iva
 * @property string $tipoPago
 * @property integer $idPension
 * @property string $fechaPago
 */
class Pago extends Model
{
    public $table = 'pagos';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $connection = "mysql2";

    public $fillable = [
        'total',
        'subtotal',
        'iva',
        'tipoPago',
        'idPension',
        'fechaPago'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'total' => 'float',
        'subtotal' => 'float',
        'iva' => 'float',
        'tipoPago' => 'string',
        'idPension' => 'integer',
        'fechaPago' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'total' => 'nullable|numeric',
        'subtotal' => 'nullable|numeric',
        'iva' => 'nullable|numeric',
        'tipoPago' => 'nullable|string|max:25',
        'idPension' => 'nullable|integer',
        'fechaPago' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];


}
