<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * Class CentralUser
 * @package App\Models
 * @version September 19, 2019, 5:09 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection autofTickets
 * @property \Illuminate\Database\Eloquent\Collection logs
 * @property \Illuminate\Database\Eloquent\Collection roles
 * @property string name
 * @property string email
 * @property string|\Carbon\Carbon email_verified_at
 * @property string password
 * @property string remember_token
 */
class CentralUser extends Authenticatable
{
    use Notifiable;

    public $table = 'central_users';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        'password' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function Tickets()
    {
        return $this->hasMany(Ticket::class, 'id_CentralUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function logs()
    {
        return $this->hasMany(Log::class, 'id_central_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function roles()
    {
        return $this->hasOne(Roles::class, 'id_central_user');
    }

    public function rol()
    {
        $rol = Roles::join('central_users','central_users.id','=','roles.id_central_user')
            ->join('permisos','permisos.id','=','roles.id_permiso')
            ->where('id_central_user','=',$this->id)
            ->select('permisos.nombre')
            ->get();
        return $rol[0]['nombre'];
    }

    public function admin(){
        $rol = Roles::join('central_users','central_users.id','=','roles.id_central_user')
            ->join('permisos','permisos.id','=','roles.id_permiso')
            ->where('id_central_user','=',$this->id)
            ->select('permisos.id')
            ->get();
        switch ($rol[0]['id']){
            case(1):
                return true;
                break;
            default:
                return false;
        }
    }

    public function monitorista(){
        $rol = Roles::join('central_users','central_users.id','=','roles.id_central_user')
            ->join('permisos','permisos.id','=','roles.id_permiso')
            ->where('id_central_user','=', $this->id)
            ->select('permisos.id')
            ->get();
        switch ($rol[0]['id']){
            case(1):
                return true;
                break;
            case(2):
                return true;
                break;
            default:
                return false;
        }
    }

    public function firmas(){
        $rol = Roles::join('central_users','central_users.id','=','roles.id_central_user')
            ->join('permisos','permisos.id','=','roles.id_permiso')
            ->where('id_central_user','=', $this->id)
            ->select('permisos.id')
            ->get();
        switch ($rol[0]['id']){
            case(4):
                return true;
                break;
            default:
                return false;
        }
    }
}
