<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TipoTicket
 * @package App\Models
 * @version September 19, 2019, 5:16 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection autofTickets
 * @property string tipo
 */
class TipoTicket extends Model
{
    use SoftDeletes;

    public $table = 'ticket_types';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'tipo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tipo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tipo' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function autofTickets()
    {
        return $this->hasMany(\App\Models\AutofTicket::class, 'id_tipo');
    }
}
