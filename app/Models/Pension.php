<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pension
 * @package App\Models
 * @version January 20, 2021, 1:40 am CST
 *
 * @property number $montoPension
 * @property string $tipoPension
 * @property string $contrato
 * @property string $solicitudContrato
 * @property string $comprobanteDomicilio
 * @property string $ine
 * @property string $licencia
 * @property string $rfc
 * @property string $tarjetaCirculacion
 * @property integer $noTarjeta
 * @property string $status
 */
class Pension extends Model
{

    public $table = 'pensionesDoc';

    public $connection = "mysql2";

    public $fillable = [
        'montoPension',
        'tipoPension',
        'contrato',
        'solicitudContrato',
        'comprobanteDomicilio',
        'ine',
        'licencia',
        'rfc',
        'tarjetaCirculacion',
        'noTarjeta',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'montoPension' => 'float',
        'tipoPension' => 'string',
        'contrato' => 'string',
        'solicitudContrato' => 'string',
        'comprobanteDomicilio' => 'string',
        'ine' => 'string',
        'licencia' => 'string',
        'rfc' => 'string',
        'tarjetaCirculacion' => 'string',
        'noTarjeta' => 'integer',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'montoPension' => 'nullable|numeric',
        'tipoPension' => 'nullable|string|max:15',
        'contrato' => 'nullable|string',
        'solicitudContrato' => 'nullable|string',
        'comprobanteDomicilio' => 'nullable|string',
        'ine' => 'nullable|string',
        'licencia' => 'nullable|string',
        'rfc' => 'nullable|string',
        'tarjetaCirculacion' => 'nullable|string',
        'noTarjeta' => 'nullable|integer',
        'status' => 'nullable|string|max:2'
    ];


}
