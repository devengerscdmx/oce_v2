<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Estacionamiento
 * @package App\Models
 * @version September 23, 2019, 5:43 pm UTC
 *
 * @property \App\Models\Marca idMarca
 * @property \App\Models\Org idOrg
 * @property \Illuminate\Database\Eloquent\Collection autofTickets
 * @property \Illuminate\Database\Eloquent\Collection ingresos
 * @property integer id_marca
 * @property integer id_org
 * @property string nombre
 * @property string dist_regio
 * @property string direccion
 * @property string calle
 * @property string no_ext
 * @property string colonia
 * @property string municipio
 * @property string estado
 * @property string cp
 * @property string latitud
 * @property string longitud
 * @property boolean Facturable
 * @property boolean Automatico
 * @property integer cajones
 * @property integer folio
 * @property string serie
 * @property string correo
 * @property integer pensiones
 * @property integer cortesias
 * @property integer cobro
 * @property string observaciones
 * @property boolean escuela
 */
class Estacionamiento extends Model
{
   //use SoftDeletes;

    public $table = 'parks';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_marca',
        'id_org',
        'nombre',
        'dist_regio',
        'direccion',
        'calle',
        'no_ext',
        'colonia',
        'municipio',
        'estado',
        'pais',
        'cp',
        'latitud',
        'longitud',
        'Facturable',
        'Automatico',
        'cajones',
        'folio',
        'serie',
        'correo',
        'pensiones',
        'cortesias',
        'observaciones',
        'escuela'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'no_est' => 'integer',
        'id_marca' => 'integer',
        'id_org' => 'integer',
        'nombre' => 'string',
        'dist_regio' => 'string',
        'direccion' => 'string',
        'calle' => 'string',
        'no_ext' => 'string',
        'colonia' => 'string',
        'municipio' => 'string',
        'estado' => 'string',
        'pais' => 'string',
        'cp' => 'string',
        'latitud' => 'string',
        'longitud' => 'string',
        'Facturable' => 'boolean',
        'Automatico' => 'boolean',
        'cajones' => 'integer',
        'folio' => 'integer',
        'serie' => 'string',
        'correo' => 'string',
        'pensiones' => 'integer',
        'cortesias' => 'integer',
        'observaciones' => 'string',
        'escuela' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_marca' => 'required',
        'id_org' => 'required',
        'nombre' => 'required',
        'dist_regio' => 'required',
        'direccion' => 'required',
        'cp' => 'required',
        'Facturable' => 'required',
        'Automatico' => 'required',
        'folio' => 'required',
        'serie' => 'required',
        'pensiones' => 'required',
        'cortesias' => 'required',
        'escuela' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idMarca()
    {
        return $this->belongsTo(\App\Models\Marca::class, 'id_marca');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idOrg()
    {
        return $this->belongsTo(\App\Models\Org::class, 'id_org');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function autofTickets()
    {
        return $this->hasMany(\App\Models\AutofTicket::class, 'id_est');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ingresos()
    {
        return $this->hasMany(\App\Models\Ingreso::class, 'id_est');
    }
}
