<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cliente
 * @package App\Models
 * @version January 20, 2021, 2:10 am CST
 *
 * @property string $apellido_paterno
 * @property string $apellido_materno
 * @property string $nombres
 * @property string $email
 * @property string $estatus
 * @property string $RFC
 */
class Cliente extends Model
{
    public $table = 'Clientes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $connection = "mysql2";

    public $fillable = [
        'apellido_paterno',
        'apellido_materno',
        'nombres',
        'email',
        'estatus',
        'RFC'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'apellido_paterno' => 'string',
        'apellido_materno' => 'string',
        'nombres' => 'string',
        'email' => 'string',
        'estatus' => 'string',
        'RFC' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'apellido_paterno' => 'nullable|string|max:100',
        'apellido_materno' => 'nullable|string|max:100',
        'nombres' => 'nullable|string|max:200',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'email' => 'nullable|string|max:150',
        'estatus' => 'nullable|string|max:1',
        'RFC' => 'nullable|string|max:14'
    ];


}
