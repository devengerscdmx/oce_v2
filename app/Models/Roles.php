<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Roles
 * @package App\Models
 * @version September 19, 2019, 5:12 pm UTC
 *
 * @property \App\Models\CentralUser idCentralUser
 * @property \App\Models\Permiso idPermiso
 * @property integer id_central_user
 * @property integer id_permiso
 */
class Roles extends Model
{


    public $table = 'roles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_central_user',
        'id_permiso'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_central_user' => 'integer',
        'id_permiso' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_central_user' => 'required',
        'id_permiso' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idCentralUser()
    {
        return $this->belongsTo(CentralUser::class, 'id_central_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Permiso()
    {
        return $this->belongsTo(Permiso::class, 'id_permiso');
    }

}
