<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Ticket
 * @package App\Models
 * @version September 19, 2019, 5:16 pm UTC
 *
 * @property \App\Models\CentralUser idCentraluser
 * @property \App\Models\User idCliente
 * @property \App\Models\Park idEst
 * @property \App\Models\TicketType idTipo
 * @property \Illuminate\Database\Eloquent\Collection autoFacts
 * @property integer id_cliente
 * @property integer id_est
 * @property integer id_tipo
 * @property integer id_CentralUser
 * @property integer factura
 * @property string no_ticket
 * @property number total_ticket
 * @property string fecha_emision
 * @property string imagen
 * @property string estatus
 * @property string UsoCFDI
 * @property string metodo_pago
 * @property string forma_pago
 * @property string RFC
 * @property string Razon_social
 * @property string email
 * @property string calle
 * @property string no_ext
 * @property string no_int
 * @property string colonia
 * @property string municipio
 * @property string estado
 * @property string cp
 * @property string coment
 */
class Ticket extends Model
{
    use SoftDeletes;

    public $table = 'autof_tickets';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_cliente',
        'id_est',
        'id_tipo',
        'id_CentralUser',
        'factura',
        'no_ticket',
        'total_ticket',
        'fecha_emision',
        'imagen',
        'estatus',
        'UsoCFDI',
        'metodo_pago',
        'forma_pago',
        'RFC',
        'Razon_social',
        'email',
        'calle',
        'no_ext',
        'no_int',
        'colonia',
        'municipio',
        'estado',
        'cp',
        'coment'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_cliente' => 'integer',
        'id_est' => 'integer',
        'id_tipo' => 'integer',
        'id_CentralUser' => 'integer',
        'factura' => 'integer',
        'no_ticket' => 'string',
        'total_ticket' => 'float',
        'fecha_emision' => 'date',
        'imagen' => 'string',
        'estatus' => 'string',
        'UsoCFDI' => 'string',
        'metodo_pago' => 'string',
        'forma_pago' => 'string',
        'RFC' => 'string',
        'Razon_social' => 'string',
        'email' => 'string',
        'calle' => 'string',
        'no_ext' => 'string',
        'no_int' => 'string',
        'colonia' => 'string',
        'municipio' => 'string',
        'estado' => 'string',
        'cp' => 'string',
        'coment' => 'string',
        'timb' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_cliente' => 'required',
        'id_est' => 'required',
        'id_tipo' => 'required',
        'id_CentralUser' => 'required',
        'factura' => 'required',
        'total_ticket' => 'required',
        'fecha_emision' => 'required',
        'estatus' => 'required',
        'UsoCFDI' => 'required',
        'metodo_pago' => 'required',
        'forma_pago' => 'required',
        'RFC' => 'required',
        'Razon_social' => 'required',
        'email' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Centraluser()
    {
        return $this->belongsTo(CentralUser::class, 'id_CentralUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idCliente()
    {
        return $this->belongsTo(\App\Models\User::class, 'id_cliente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Estacionamiento()
    {
        return $this->belongsTo(Estacionamiento::class, 'id_est');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idTipo()
    {
        return $this->belongsTo(\App\Models\TicketType::class, 'id_tipo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function autoFacts()
    {
        return $this->hasMany(\App\Models\AutoFact::class, 'id_ticketAF');
    }
}
