<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FormaPago
 * @package App\Models
 * @version September 19, 2019, 5:24 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection pensions
 * @property string forma
 */
class FormaPago extends Model
{
    use SoftDeletes;

    public $table = 'forma_pago';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'forma'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'forma' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'forma' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pensions()
    {
        return $this->hasMany(\App\Models\Pension::class, 'id_forma_pago');
    }
}
