<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Firma
 * @package App\Models
 * @version November 25, 2019, 11:17 am CST
 *
 * @property \App\Models\CentralUser idCentraluser
 * @property integer id_CentralUser
 * @property string nombre
 * @property string tipo
 * @property string asunto
 * @property string responsable
 */
class Firma extends Model
{
    use SoftDeletes;

    public $table = 'firmas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_CentralUser',
        'nombre',
        'tipo',
        'asunto',
        'responsable',
        'imagen'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_CentralUser' => 'integer',
        'nombre' => 'string',
        'tipo' => 'string',
        'asunto' => 'string',
        'responsable' => 'string',
        'imagen' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_CentralUser' => 'required',
        'nombre' => 'required',
        'tipo' => 'required',
        'asunto' => 'required',
        'responsable' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idCentraluser()
    {
        return $this->belongsTo(\App\Models\CentralUser::class, 'id_CentralUser');
    }
}
