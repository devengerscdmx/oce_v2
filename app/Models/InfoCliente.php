<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class InfoCliente
 * @package App\Models
 * @version September 19, 2019, 5:04 pm UTC
 *
 * @property \App\Models\User idCliente
 * @property integer id_cliente
 * @property string RFC
 * @property string Razon_social
 * @property string calle
 * @property string no_ext
 * @property string no_int
 * @property string colonia
 * @property string municipio
 * @property string estado
 * @property string pais
 * @property string cp
 * @property string act_na
 * @property string ine
 * @property string tar_circ
 * @property string placas
 * @property string tel
 * @property boolean pension
 */
class InfoCliente extends Model
{
    use SoftDeletes;

    public $table = 'user_infos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_cliente',
        'RFC',
        'Razon_social',
        'calle',
        'no_ext',
        'no_int',
        'colonia',
        'municipio',
        'estado',
        'pais',
        'cp',
        'act_na',
        'ine',
        'tar_circ',
        'placas',
        'tel',
        'pension'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_cliente' => 'integer',
        'RFC' => 'string',
        'Razon_social' => 'string',
        'calle' => 'string',
        'no_ext' => 'string',
        'no_int' => 'string',
        'colonia' => 'string',
        'municipio' => 'string',
        'estado' => 'string',
        'pais' => 'string',
        'cp' => 'string',
        'act_na' => 'string',
        'ine' => 'string',
        'tar_circ' => 'string',
        'placas' => 'string',
        'tel' => 'string',
        'pension' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_cliente' => 'required',
        'RFC' => 'required',
        'Razon_social' => 'required',
        'pension' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idCliente()
    {
        return $this->belongsTo(\App\Models\User::class, 'id_cliente');
    }
}
