<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FacturaPension
 * @package App\Models
 * @version January 20, 2021, 2:03 am CST
 *
 * @property integer $id_pen
 * @property string $serie
 * @property string $folio
 * @property string|\Carbon\Carbon $fecha_timbrado
 * @property string $uuid
 * @property number $subtotal_factura
 * @property number $iva_factura
 * @property number $total_factura
 * @property string $XML
 * @property string $PDF
 * @property string $estatus
 * @property string $usoCFDI
 * @property integer $metodoPago
 */
class FacturaPension extends Model
{
    public $table = 'facturaPensiones';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $connection = "mysql2";

    public $fillable = [
        'id_pen',
        'serie',
        'folio',
        'fecha_timbrado',
        'uuid',
        'subtotal_factura',
        'iva_factura',
        'total_factura',
        'XML',
        'PDF',
        'estatus',
        'usoCFDI',
        'metodoPago'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_pen' => 'integer',
        'serie' => 'string',
        'folio' => 'string',
        'fecha_timbrado' => 'datetime',
        'uuid' => 'string',
        'subtotal_factura' => 'decimal:2',
        'iva_factura' => 'decimal:2',
        'total_factura' => 'decimal:2',
        'XML' => 'string',
        'PDF' => 'string',
        'estatus' => 'string',
        'usoCFDI' => 'string',
        'metodoPago' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_pen' => 'required|integer',
        'serie' => 'required|string|max:255',
        'folio' => 'required|string|max:255',
        'fecha_timbrado' => 'nullable',
        'uuid' => 'nullable|string|max:255',
        'subtotal_factura' => 'nullable|numeric',
        'iva_factura' => 'nullable|numeric',
        'total_factura' => 'nullable|numeric',
        'XML' => 'nullable|string',
        'PDF' => 'nullable|string',
        'estatus' => 'nullable|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'usoCFDI' => 'nullable|string|max:10',
        'metodoPago' => 'nullable|integer'
    ];


}
