<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Gerente
 * @package App\Models
 * @version September 23, 2019, 7:24 pm UTC
 *
 * @property \App\Models\Park idEst
 * @property integer id_est
 * @property string name
 * @property string email
 * @property string password
 * @property string remember_token
 */
class Gerente extends Model
{

    public $table = 'gerente';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_est',
        'name',
        'email',
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_est' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_est' => 'required',
        'name' => 'required',
        'email' => 'required',
        'password' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idEst()
    {
        return $this->belongsTo(\App\Models\Park::class, 'id_est');
    }
}
