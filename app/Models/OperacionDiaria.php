<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OperacionDiaria
 * @package App\Models
 * @version January 20, 2021, 1:55 am CST
 *
 * @property integer $tarjeta_fisica
 * @property integer $transaccion
 * @property string $tipo_operacion
 * @property number $monto
 * @property string $estancia
 */
class OperacionDiaria extends Model
{
    public $table = 'operacionesDiarias';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $connection = "mysql2";

    public $fillable = [
        'tarjeta_fisica',
        'transaccion',
        'tipo_operacion',
        'monto',
        'estancia'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tarjeta_fisica' => 'integer',
        'transaccion' => 'integer',
        'tipo_operacion' => 'string',
        'monto' => 'decimal:2',
        'estancia' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tarjeta_fisica' => 'nullable|integer',
        'transaccion' => 'nullable|integer',
        'tipo_operacion' => 'nullable|string|max:45',
        'monto' => 'nullable|numeric',
        'estancia' => 'nullable|string|max:12',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];


}
