<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ImgIngreso
 * @package App\Models
 * @version September 19, 2019, 5:27 pm UTC
 *
 * @property \App\Models\Ingreso idIngreso
 * @property integer id_ingreso
 * @property string foto
 * @property string firma
 */
class ImgIngreso extends Model
{
    use SoftDeletes;

    public $table = 'img_ingresos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_ingreso',
        'foto',
        'firma'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_ingreso' => 'integer',
        'foto' => 'string',
        'firma' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_ingreso' => 'required',
        'foto' => 'required',
        'firma' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idIngreso()
    {
        return $this->belongsTo(\App\Models\Ingreso::class, 'id_ingreso');
    }
}
