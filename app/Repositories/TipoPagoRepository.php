<?php

namespace App\Repositories;

use App\Models\TipoPago;
use App\Repositories\BaseRepository;

/**
 * Class TipoPagoRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:19 pm UTC
*/

class TipoPagoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoPago::class;
    }
}
