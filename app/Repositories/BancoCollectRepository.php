<?php

namespace App\Repositories;

use App\Models\BancoCollect;
use App\Repositories\BaseRepository;

/**
 * Class BancoCollectRepository
 * @package App\Repositories
 * @version September 19, 2019, 7:26 pm UTC
*/

class BancoCollectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'banco'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BancoCollect::class;
    }
}
