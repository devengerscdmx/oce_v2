<?php

namespace App\Repositories;

use App\Models\Ticket;
use App\Repositories\BaseRepository;

/**
 * Class TicketRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:16 pm UTC
*/

class TicketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_cliente',
        'id_est',
        'id_tipo',
        'id_CentralUser',
        'factura',
        'no_ticket',
        'total_ticket',
        'fecha_emision',
        'imagen',
        'estatus',
        'UsoCFDI',
        'metodo_pago',
        'forma_pago',
        'RFC',
        'Razon_social',
        'email',
        'calle',
        'no_ext',
        'no_int',
        'colonia',
        'municipio',
        'estado',
        'cp',
        'coment'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ticket::class;
    }
}
