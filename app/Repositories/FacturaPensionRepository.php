<?php

namespace App\Repositories;

use App\Models\FacturaPension;
use App\Repositories\BaseRepository;

/**
 * Class FacturaPensionRepository
 * @package App\Repositories
 * @version January 20, 2021, 2:03 am CST
*/

class FacturaPensionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_pen',
        'serie',
        'folio',
        'fecha_timbrado',
        'uuid',
        'subtotal_factura',
        'iva_factura',
        'total_factura',
        'XML',
        'PDF',
        'estatus',
        'usoCFDI',
        'metodoPago'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FacturaPension::class;
    }
}
