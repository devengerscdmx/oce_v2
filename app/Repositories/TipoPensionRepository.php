<?php

namespace App\Repositories;

use App\Models\TipoPension;
use App\Repositories\BaseRepository;

/**
 * Class TipoPensionRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:20 pm UTC
*/

class TipoPensionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoPension::class;
    }
}
