<?php

namespace App\Repositories;

use App\Models\TipoTicket;
use App\Repositories\BaseRepository;

/**
 * Class TipoTicketRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:16 pm UTC
*/

class TipoTicketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoTicket::class;
    }
}
