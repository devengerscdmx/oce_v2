<?php

namespace App\Repositories;

use App\Models\EmpresaCollect;
use App\Repositories\BaseRepository;

/**
 * Class EmpresaCollectRepository
 * @package App\Repositories
 * @version September 19, 2019, 7:27 pm UTC
*/

class EmpresaCollectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'empresa'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EmpresaCollect::class;
    }
}
