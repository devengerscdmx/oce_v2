<?php

namespace App\Repositories;

use App\Models\InfoCliente;
use App\Repositories\BaseRepository;

/**
 * Class InfoClienteRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:04 pm UTC
*/

class InfoClienteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_cliente',
        'RFC',
        'Razon_social',
        'calle',
        'no_ext',
        'no_int',
        'colonia',
        'municipio',
        'estado',
        'pais',
        'cp',
        'act_na',
        'ine',
        'tar_circ',
        'placas',
        'tel',
        'pension'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InfoCliente::class;
    }
}
