<?php

namespace App\Repositories;

use App\Models\FormaPago;
use App\Repositories\BaseRepository;

/**
 * Class FormaPagoRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:24 pm UTC
*/

class FormaPagoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'forma'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FormaPago::class;
    }
}
