<?php

namespace App\Repositories;

use App\Models\Firma;
use App\Repositories\BaseRepository;

/**
 * Class FirmaRepository
 * @package App\Repositories
 * @version November 25, 2019, 11:17 am CST
*/

class FirmaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_CentralUser',
        'nombre',
        'tipo',
        'asunto',
        'responsable'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Firma::class;
    }
}
