<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OrgsTableSeeder::class);
        //$this->call(ParksTableSeeder::class);
        $this->call(PensionTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CentralUserTableSeeder::class);
    }
}
