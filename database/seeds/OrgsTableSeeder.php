<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrgsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orgs')->insert([
            'RFC'=>'OCE9412073L3',
            'Razon_social'=>'OPERADORA CENTRAL DE ESTACIONAMIENTOS, SAPI DE C.V.',
            'Regimen_fiscal'=>'601',
            'URL_dev' => 'https://api-stage.emite.dev',
            'URL_prod' => 'https://api.emite.app',
            'token_dev' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJCNG5HdWEtbkZ1S1ZkaDhHN1pYUmdtTXE1LXdQeEpNTmhRTEMxb0hKIiwic3ViIjoiQ2VudHJhbCBbU1RBR0VdIiwiaXNzIjoiRU1JVEUgW1NUQUdFXSIsImF1ZCI6IkJFTkdBTEEgW1NUQUdFXSJ9.p6lmdlIGKB5Z2FedsUusN8U3CkY5sEUcwz4BHx_VBIM',
            'token_prod' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJUVExlaTVwWW52RHNQVWF1elkyci1rTDJaTmhNUHk3dWNYV3RrYUFYIiwic3ViIjoiSW50ZWdyYWRvci1UZWNobm9sb2d5IFtQUk9EXSIsImlzcyI6IkVNSVRFIFtQUk9EXSIsImF1ZCI6IkJFTkdBTEEgW1BST0RdIn0.MMHzeCtRo3E8rTLnP4bsaCSh7m13_u4MlZ7E23MXZCI',
            'pass' => encrypt('oce94120'),
            'ruta_pem' => 'doc/CSD_OPERADORA_CENTRAL_DE_ESTACIONAMIENTOS_SAPI_DE_CV_OCE9412073L3_20160520_091056.key.pem',
            'ruta_cer' => 'doc/00001000000402646722.cer',
            'cp' => '01020',
            'dir' => 'AV INSURGENTES SUR 1863 301-B GUADALUPE INN. ALVARO OBREGON CIUDAD DE MÉXICO CP 01020'
        ]);
        /*DB::table('orgs')->insert([
            'RFC'=>'SCE940913BX0',
            'Razon_social'=>'SERVICIOS CORPORATIVOS PARA ESTACIONAMIENTOS SA DE CV',
            'Regimen_fiscal'=>'601',
            'URL_dev' => 'https://api-stage.emite.dev/v1/stamp',
            'URL_prod' => 'https://api.emite.app/v1/stamp',
            'token_dev' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJCNG5HdWEtbkZ1S1ZkaDhHN1pYUmdtTXE1LXdQeEpNTmhRTEMxb0hKIiwic3ViIjoiQ2VudHJhbCBbU1RBR0VdIiwiaXNzIjoiRU1JVEUgW1NUQUdFXSIsImF1ZCI6IkJFTkdBTEEgW1NUQUdFXSJ9.p6lmdlIGKB5Z2FedsUusN8U3CkY5sEUcwz4BHx_VBIM',
            'token_prod' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJUVExlaTVwWW52RHNQVWF1elkyci1rTDJaTmhNUHk3dWNYV3RrYUFYIiwic3ViIjoiSW50ZWdyYWRvci1UZWNobm9sb2d5IFtQUk9EXSIsImlzcyI6IkVNSVRFIFtQUk9EXSIsImF1ZCI6IkJFTkdBTEEgW1BST0RdIn0.MMHzeCtRo3E8rTLnP4bsaCSh7m13_u4MlZ7E23MXZCI',
        ]);
        DB::table('orgs')->insert([
            'RFC'=>'OEP150722QNA',
            'Razon_social'=>'OPERADORA DE ESTACIONAMIENTOS PUMASA, SAPI DE C.V.',
            'Regimen_fiscal'=>'601',
            'URL_dev' => 'https://api-stage.emite.dev/v1/stamp',
            'URL_prod' => 'https://api.emite.app/v1/stamp',
            'token_dev' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJCNG5HdWEtbkZ1S1ZkaDhHN1pYUmdtTXE1LXdQeEpNTmhRTEMxb0hKIiwic3ViIjoiQ2VudHJhbCBbU1RBR0VdIiwiaXNzIjoiRU1JVEUgW1NUQUdFXSIsImF1ZCI6IkJFTkdBTEEgW1NUQUdFXSJ9.p6lmdlIGKB5Z2FedsUusN8U3CkY5sEUcwz4BHx_VBIM',
            'token_prod' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJUVExlaTVwWW52RHNQVWF1elkyci1rTDJaTmhNUHk3dWNYV3RrYUFYIiwic3ViIjoiSW50ZWdyYWRvci1UZWNobm9sb2d5IFtQUk9EXSIsImlzcyI6IkVNSVRFIFtQUk9EXSIsImF1ZCI6IkJFTkdBTEEgW1BST0RdIn0.MMHzeCtRo3E8rTLnP4bsaCSh7m13_u4MlZ7E23MXZCI',
        ]);*/
    }
}
