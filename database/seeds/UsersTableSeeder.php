<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'Sin registro',
            'email'=> 'email@correo.com',
            'password'=> Hash::make('Administrador1'),
        ]);
        DB::table('ticket_types')->insert([
            'tipo' => 'Manual',
        ]);
        DB::table('ticket_types')->insert([
            'tipo' => 'Automatico',
        ]);
        DB::table('ticket_types')->insert([
            'tipo' => 'Emergencia',
        ]);
    }
}
