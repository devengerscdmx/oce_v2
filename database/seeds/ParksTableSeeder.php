<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marcas')->insert([
            'marca' => 'patito'
        ]);

        DB::table('parks')->insert([
            'no_est' => 1,
            'nombre' => 'Sin estacionamiento',
            'dist_regio' => 'N/A',
            'calle' => 'N/A',
            'no_ext' => 'N/A',
            'colonia' => 'N/A',
            'municipio' => 'N/A',
            'estado' => 'N/A',
            'cp' => '00000',
            'Facturable' => true,
            'id_marca' => 1,
            'Automatico' => false,
            'id_org' => 1,
            'serie' => 'N/A',
            'direccion' => 'test',
        ]);
        DB::table('parks')->insert([
            'no_est' => 8,
            'nombre' => 'PABELLON ALTAVISTA',
            'dist_regio' => 'DF1',
            'calle' => 'camino al desierto de los leones',
            'no_ext' => '52',
            'colonia' => 'San Angel',
            'municipio' => 'Alvaro Obregon',
            'estado' => 'CIUDAD DE MEXICO',
            'cp' => '01000',
            'Facturable' => true,
            'id_marca' => 1,
            'Automatico' => true,
            'id_org' => 1,
            'serie' => 'J8',
            'direccion' => 'test',
        ]);
        DB::table('parks')->insert([
            'no_est' => 7,
            'nombre' => 'CORPORATIVO SANTA FE II',
            'dist_regio' => 'DF1',
            'calle' => 'AV. PROL. PASEO DE LA REFORMA',
            'no_ext' => '490',
            'colonia' => 'SANTA FE',
            'municipio' => 'Alvaro Obregon',
            'estado' => 'CIUDAD DE MEXICO',
            'cp' => '01210',
            'Facturable' => true,
            'id_marca' => 1,
            'Automatico' => false,
            'id_org' => 1,
            'serie' => 'J7',
            'direccion' => 'test',
        ]);
    }
}
