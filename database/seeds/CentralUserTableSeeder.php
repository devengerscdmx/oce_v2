<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CentralUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('central_users')->insert([
            'name'=>'Victor Rojas',
            'email'=> 'vrojas@integrador-technology.mx',
            'password'=> Hash::make('123456789'),
        ]);

        DB::table('central_users')->insert([
            'name'=>'John Vargas',
            'email'=> 'lfernando@integrador-technology.mx',
            'password'=> Hash::make('Administrador1'),
        ]);

        /*DB::table('gerente')->insert([
            'id_est' => 7,
            'name'=>'John Vargas',
            'email'=> 'john@gmail.com',
            'password'=> Hash::make('Administrador1'),
        ]);*/

        DB::table('permisos')->insert([
            'nombre' => 'Administrador'
        ]);

        DB::table('roles')->insert([
            'id_central_user' => 1,
            'id_permiso' => 1
        ]);

        DB::table('roles')->insert([
            'id_central_user' => 2,
            'id_permiso' => 1
        ]);
        DB::table('banco_colect')->insert([
            'banco' => 'Bancomer'
        ]);
        DB::table('empresa_colect')->insert([
            'empresa' => 'Lock'
        ]);
    }
}
