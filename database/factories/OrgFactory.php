<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Org;
use Faker\Generator as Faker;

$factory->define(Org::class, function (Faker $faker) {

    return [
        'RFC' => $faker->word,
        'Razon_social' => $faker->word,
        'Regimen_fiscal' => $faker->word,
        'URL_dev' => $faker->word,
        'URL_prod' => $faker->word,
        'token_dev' => $faker->text,
        'token_prod' => $faker->text,
        'Prod' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
