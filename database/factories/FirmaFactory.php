<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Firma;
use Faker\Generator as Faker;

$factory->define(Firma::class, function (Faker $faker) {

    return [
        'id_CentralUser' => $faker->word,
        'nombre' => $faker->word,
        'tipo' => $faker->word,
        'asunto' => $faker->word,
        'responsable' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
