<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pago;
use Faker\Generator as Faker;

$factory->define(Pago::class, function (Faker $faker) {

    return [
        'total' => $faker->randomDigitNotNull,
        'subtotal' => $faker->randomDigitNotNull,
        'iva' => $faker->randomDigitNotNull,
        'tipoPago' => $faker->word,
        'idPension' => $faker->randomDigitNotNull,
        'fechaPago' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
