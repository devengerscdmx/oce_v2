<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Gerente;
use Faker\Generator as Faker;

$factory->define(Gerente::class, function (Faker $faker) {

    return [
        'id_est' => $faker->randomDigitNotNull,
        'name' => $faker->word,
        'email' => $faker->word,
        'password' => $faker->word,
        'remember_token' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
