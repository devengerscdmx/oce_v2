<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Cliente;
use Faker\Generator as Faker;

$factory->define(Cliente::class, function (Faker $faker) {

    return [
        'apellido_paterno' => $faker->word,
        'apellido_materno' => $faker->word,
        'nombres' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'email' => $faker->word,
        'estatus' => $faker->word,
        'RFC' => $faker->word
    ];
});
