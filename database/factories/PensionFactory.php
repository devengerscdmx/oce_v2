<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pension;
use Faker\Generator as Faker;

$factory->define(Pension::class, function (Faker $faker) {

    return [
        'montoPension' => $faker->randomDigitNotNull,
        'tipoPension' => $faker->word,
        'contrato' => $faker->text,
        'solicitudContrato' => $faker->text,
        'comprobanteDomicilio' => $faker->text,
        'ine' => $faker->text,
        'licencia' => $faker->text,
        'rfc' => $faker->text,
        'tarjetaCirculacion' => $faker->text,
        'noTarjeta' => $faker->randomDigitNotNull,
        'status' => $faker->word
    ];
});
