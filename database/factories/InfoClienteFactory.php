<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\InfoCliente;
use Faker\Generator as Faker;

$factory->define(InfoCliente::class, function (Faker $faker) {

    return [
        'id_cliente' => $faker->word,
        'RFC' => $faker->word,
        'Razon_social' => $faker->word,
        'calle' => $faker->word,
        'no_ext' => $faker->word,
        'no_int' => $faker->word,
        'colonia' => $faker->word,
        'municipio' => $faker->word,
        'estado' => $faker->word,
        'pais' => $faker->word,
        'cp' => $faker->word,
        'act_na' => $faker->text,
        'ine' => $faker->text,
        'tar_circ' => $faker->text,
        'placas' => $faker->word,
        'tel' => $faker->word,
        'pension' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
