<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Ingreso;
use Faker\Generator as Faker;

$factory->define(Ingreso::class, function (Faker $faker) {

    return [
        'id_empresa' => $faker->word,
        'id_banco' => $faker->word,
        'numero' => $faker->randomDigitNotNull,
        'venta' => $faker->word,
        'fecha_recoleccion' => $faker->word,
        'importe' => $faker->word,
        'valido' => $faker->word,
        'coment' => $faker->text,
        'tipo' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'id_gerente' => $faker->word
    ];
});
