<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OperacionDiaria;
use Faker\Generator as Faker;

$factory->define(OperacionDiaria::class, function (Faker $faker) {

    return [
        'tarjeta_fisica' => $faker->randomDigitNotNull,
        'transaccion' => $faker->randomDigitNotNull,
        'tipo_operacion' => $faker->word,
        'monto' => $faker->word,
        'estancia' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
