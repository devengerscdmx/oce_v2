<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Roles;
use Faker\Generator as Faker;

$factory->define(Roles::class, function (Faker $faker) {

    return [
        'id_central_user' => $faker->word,
        'id_permiso' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
