<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\FacturaPension;
use Faker\Generator as Faker;

$factory->define(FacturaPension::class, function (Faker $faker) {

    return [
        'id_pen' => $faker->randomDigitNotNull,
        'serie' => $faker->word,
        'folio' => $faker->word,
        'fecha_timbrado' => $faker->date('Y-m-d H:i:s'),
        'uuid' => $faker->word,
        'subtotal_factura' => $faker->word,
        'iva_factura' => $faker->word,
        'total_factura' => $faker->word,
        'XML' => $faker->text,
        'PDF' => $faker->text,
        'estatus' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'usoCFDI' => $faker->word,
        'metodoPago' => $faker->randomDigitNotNull
    ];
});
