<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orgs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('RFC');
            $table->string('Razon_social');
            $table->string('Regimen_fiscal');
            $table->string('URL_dev');
            $table->string('URL_prod');
            $table->text('token_dev');
            $table->text('token_prod');
            $table->string('pass');
            $table->string('ruta_pem');
            $table->string('ruta_cer');
            $table->string('cp');
            $table->tinyInteger('Prod')->default(0);
            $table->string('dir');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orgs');
    }
}
