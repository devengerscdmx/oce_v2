<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutofTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autof_tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_cliente')->index('autof_tickets_id_cliente_foreign');
            $table->integer('id_est')->index('autof_tickets_id_est_foreign');
            $table->unsignedBigInteger('id_tipo')->index('autof_tickets_id_tipo_foreign');
            $table->unsignedBigInteger('id_CentralUser')->index('autof_tickets_id_centraluser_foreign');
            $table->integer('factura');
            $table->string('no_ticket')->nullable();
            $table->decimal('total_ticket');
            $table->date('fecha_emision');
            $table->longText('imagen')->nullable();
            $table->string('estatus');
            $table->string('UsoCFDI');
            $table->string('metodo_pago')->default('PUE');
            $table->string('forma_pago');
            $table->string('RFC');
            $table->string('Razon_social');
            $table->string('email');
            $table->string('calle')->nullable();
            $table->string('no_ext')->nullable();
            $table->string('no_int')->nullable();
            $table->string('colonia')->nullable();
            $table->string('municipio')->nullable();
            $table->string('estado')->nullable();
            $table->string('cp')->nullable();
            $table->text('coment')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->tinyInteger('timb')->default(0);
            $table->integer('pi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autof_tickets');
    }
}
