<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutoFactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_facts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_ticketAF')->index('auto_facts_id_ticketaf_foreign');
            $table->string('serie');
            $table->string('tipo_doc');
            $table->string('folio');
            $table->dateTime('fecha_timbrado')->nullable();
            $table->string('uuid')->nullable();
            $table->decimal('subtotal_factura');
            $table->decimal('iva_factura');
            $table->decimal('total_factura');
            $table->longText('XML');
            $table->longText('PDF');
            $table->string('estatus');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_facts');
    }
}
