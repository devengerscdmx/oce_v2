<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryFactErrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_fact_err', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('rfc', 13)->nullable();
            $table->integer('id_est')->nullable();
            $table->string('email_user', 50)->nullable();
            $table->integer('folio')->nullable();
            $table->string('razon_social')->nullable();
            $table->timestamp('fecha_error')->useCurrent();
            $table->tinyInteger('estatus')->nullable();
            $table->bigInteger('id_trans')->nullable()->index('fk_id_trans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_fact_err');
    }
}
