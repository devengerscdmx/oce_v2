<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToImgIngresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('img_ingresos', function (Blueprint $table) {
            $table->foreign('id_ingreso')->references('id')->on('ingresos')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('img_ingresos', function (Blueprint $table) {
            $table->dropForeign('img_ingresos_id_ingreso_foreign');
        });
    }
}
