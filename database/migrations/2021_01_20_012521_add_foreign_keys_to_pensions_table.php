<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToPensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pensions', function (Blueprint $table) {
            $table->foreign('id_cliente')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('id_fecha_limite')->references('id')->on('fecha_limite')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('id_forma_pago')->references('id')->on('forma_pago')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('id_tipo_pago')->references('id')->on('tipo_pago')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('id_tipo_pen')->references('id')->on('tipo_pen')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pensions', function (Blueprint $table) {
            $table->dropForeign('pensions_id_cliente_foreign');
            $table->dropForeign('pensions_id_fecha_limite_foreign');
            $table->dropForeign('pensions_id_forma_pago_foreign');
            $table->dropForeign('pensions_id_tipo_pago_foreign');
            $table->dropForeign('pensions_id_tipo_pen_foreign');
        });
    }
}
