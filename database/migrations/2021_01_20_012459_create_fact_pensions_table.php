<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFactPensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_pensions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_pen')->index('fact_pensions_id_pen_foreign');
            $table->string('serie');
            $table->string('tipo_doc');
            $table->string('folio');
            $table->dateTime('fecha_timbrado')->nullable();
            $table->string('uuid')->nullable();
            $table->decimal('subtotal_factura');
            $table->decimal('iva_factura');
            $table->decimal('total_factura');
            $table->longText('XML');
            $table->longText('PDF');
            $table->string('estatus');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_pensions');
    }
}
