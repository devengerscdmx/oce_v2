<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBombas1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bombas_1', function (Blueprint $table) {
            $table->bigInteger('id')->primary();
            $table->string('no_est');
            $table->integer('num_pen');
            $table->integer('folio');
            $table->string('rs');
            $table->string('rfc');
            $table->string('clave_sat');
            $table->string('clave_unidad');
            $table->string('descripcion');
            $table->double('total', 8, 2);
            $table->string('uso_cfdi');
            $table->string('forma_pago');
            $table->string('email_user');
            $table->string('email_gerente');
            $table->tinyInteger('tmbrada');
            $table->tinyInteger('valida_rfc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bombas_1');
    }
}
