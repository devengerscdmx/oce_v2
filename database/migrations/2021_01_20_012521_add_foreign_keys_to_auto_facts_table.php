<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToAutoFactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_facts', function (Blueprint $table) {
            $table->foreign('id_ticketAF')->references('id')->on('autof_tickets')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auto_facts', function (Blueprint $table) {
            $table->dropForeign('auto_facts_id_ticketaf_foreign');
        });
    }
}
