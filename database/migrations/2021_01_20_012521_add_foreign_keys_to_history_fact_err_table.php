<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToHistoryFactErrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_fact_err', function (Blueprint $table) {
            $table->foreign('id_trans', 'fk_id_trans')->references('id')->on('bombas_1')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_fact_err', function (Blueprint $table) {
            $table->dropForeign('fk_id_trans');
        });
    }
}
