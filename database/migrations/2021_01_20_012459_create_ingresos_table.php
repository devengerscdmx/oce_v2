<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingresos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_empresa')->index('ingresos_id_empresa_foreign');
            $table->unsignedBigInteger('id_banco')->index('ingresos_id_banco_foreign');
            $table->integer('numero')->nullable();
            $table->date('venta')->nullable();
            $table->date('fecha_recoleccion')->nullable();
            $table->decimal('importe')->nullable();
            $table->enum('valido', ['valido', 'pendiente', 'rechazada', 'validando', 'corregida', 'pendiente_correcion', 'no_recolec'])->default('pendiente');
            $table->text('coment')->nullable();
            $table->enum('tipo', ['ficha', 'voucher']);
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('id_gerente')->index('ingresos_id_gerente_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingresos');
    }
}
