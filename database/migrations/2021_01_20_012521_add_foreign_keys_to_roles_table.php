<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->foreign('id_central_user')->references('id')->on('central_users')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('id_permiso')->references('id')->on('permisos')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->dropForeign('roles_id_central_user_foreign');
            $table->dropForeign('roles_id_permiso_foreign');
        });
    }
}
