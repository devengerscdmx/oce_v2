<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToParksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parks', function (Blueprint $table) {
            $table->foreign('id_marca')->references('id')->on('marcas')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('id_org')->references('id')->on('orgs')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parks', function (Blueprint $table) {
            $table->dropForeign('parks_id_marca_foreign');
            $table->dropForeign('parks_id_org_foreign');
        });
    }
}
