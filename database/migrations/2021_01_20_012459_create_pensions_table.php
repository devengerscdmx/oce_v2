<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pensions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_cliente')->index('pensions_id_cliente_foreign');
            $table->unsignedBigInteger('id_tipo_pen')->index('pensions_id_tipo_pen_foreign');
            $table->unsignedBigInteger('id_tipo_pago')->index('pensions_id_tipo_pago_foreign');
            $table->unsignedBigInteger('id_forma_pago')->index('pensions_id_forma_pago_foreign');
            $table->unsignedBigInteger('id_fecha_limite')->index('pensions_id_fecha_limite_foreign');
            $table->integer('num_pen');
            $table->longText('foto_comprobante')->nullable();
            $table->integer('no_est');
            $table->tinyInteger('factura');
            $table->decimal('costo_pension');
            $table->decimal('recargos');
            $table->decimal('venta_tarjeta');
            $table->decimal('repo_tarjeta');
            $table->decimal('impor_pago');
            $table->integer('mes_pago');
            $table->string('cargado_por');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pensions');
    }
}
