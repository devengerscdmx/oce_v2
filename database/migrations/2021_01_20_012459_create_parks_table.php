<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parks', function (Blueprint $table) {
            $table->integer('no_est')->primary();
            $table->unsignedBigInteger('id_marca')->index('parks_id_marca_foreign');
            $table->unsignedBigInteger('id_org')->index('parks_id_org_foreign');
            $table->string('nombre');
            $table->string('dist_regio');
            $table->string('direccion');
            $table->string('calle')->nullable();
            $table->string('no_ext')->nullable();
            $table->string('colonia')->nullable();
            $table->string('municipio')->nullable();
            $table->string('estado')->nullable();
            $table->string('pais')->nullable();
            $table->string('cp');
            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();
            $table->tinyInteger('Facturable');
            $table->tinyInteger('Automatico');
            $table->integer('cajones')->nullable();
            $table->integer('folio')->default(0);
            $table->string('serie');
            $table->string('correo')->nullable();
            $table->integer('pensiones')->default(0);
            $table->integer('cortesias')->default(0);
            $table->string('observaciones')->nullable();
            $table->tinyInteger('escuela')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parks');
    }
}
