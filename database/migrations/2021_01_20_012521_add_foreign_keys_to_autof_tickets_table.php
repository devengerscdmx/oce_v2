<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToAutofTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autof_tickets', function (Blueprint $table) {
            $table->foreign('id_CentralUser')->references('id')->on('central_users')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('id_cliente')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('id_est')->references('no_est')->on('parks')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('id_tipo')->references('id')->on('ticket_types')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autof_tickets', function (Blueprint $table) {
            $table->dropForeign('autof_tickets_id_centraluser_foreign');
            $table->dropForeign('autof_tickets_id_cliente_foreign');
            $table->dropForeign('autof_tickets_id_est_foreign');
            $table->dropForeign('autof_tickets_id_tipo_foreign');
        });
    }
}
