<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToIngresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ingresos', function (Blueprint $table) {
            $table->foreign('id_banco')->references('id')->on('banco_colect')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('id_empresa')->references('id')->on('empresa_colect')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('id_gerente')->references('id')->on('gerente')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ingresos', function (Blueprint $table) {
            $table->dropForeign('ingresos_id_banco_foreign');
            $table->dropForeign('ingresos_id_empresa_foreign');
            $table->dropForeign('ingresos_id_gerente_foreign');
        });
    }
}
