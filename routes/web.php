<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');
Auth::Routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('clientes', 'ClienteController');
    Route::resource('infoClientes', 'InfoClienteController');
    Route::resource('central_users', 'CentralUserController');
    Route::resource('logs', 'LogController');
    Route::resource('permisos', 'PermisoController');
    Route::resource('roles', 'RolesController');
    Route::resource('marcas', 'MarcasController');
    Route::resource('orgs', 'OrgController');
    Route::resource('estacionamientos', 'EstacionamientoController');
    Route::resource('tipoTickets', 'TipoTicketController');
    Route::resource('tickets', 'TicketController');
    Route::resource('factura', 'FacturaController');
    Route::resource('tipoPagos', 'TipoPagoController');
    Route::resource('tipoPensions', 'TipoPensionController');
    Route::resource('formaPagos', 'FormaPagoController');
    Route::resource('imgIngresos', 'ImgIngresoController');
    Route::resource('ingresos', 'IngresoController');
    Route::resource('bancoCollects', 'BancoCollectController');
    Route::resource('empresaCollects', 'EmpresaCollectController');
    Route::resource('gerentes', 'GerenteController');
    Route::resource('dashboard', 'DashboardController');
    Route::resource('diarioingreso', 'DiarioIngresoController');
    Route::resource('firmas', 'FirmaController');
    Route::get('/xml/{id}', 'FacturaController@downloadXML')->name('xml');
    Route::get('/pdf/{id}', 'FacturaController@downloadPDF')->name('pdf');
    Route::get('/img/{id}', 'TicketController@DownImg')->name('img');
    Route::post('/generar/', 'FacturaController@create')->name('generar');
    Route::post('/buscar/facturas','FacturaController@busqueda')->name('busqueda');
    Route::post('/email', 'FacturaController@enviarEmail')->name('email');
    Route::post('/actualiza/{id}', 'TicketController@actualiza')->name('no_est');
    Route::post('/tickets/timbrado/', 'FacturaController@timbrado')->name('timbrar');
    Route::get('/tickets/validar/{id}', 'TicketController@valido')->name('valido');
    Route::post('/tickets/rechazo/{id}', 'TicketController@rechazo')->name('rechazo');
    Route::get('/rechazados', 'TicketController@list_rechazo')->name('list_rechazo');
    Route::post('dashboard', 'DashboardController@AjaxRequest')->name('dashboard');
    Route::get('/graficas/{id}', 'EstacionamientoController@graficas')->name('graficas');
    Route::get('/ingresos/rechazados', 'IngresoController@Frechazos')->name('ingresos.rechazo');
    Route::get('/facturas/pi','FacturaController@pi')->name('pi');
    Route::get('/orgs/cambio/{id}','OrgController@cambio')->name('cambio');
    Route::get('/fichas/validar/{id}', 'IngresoController@Fvalidos')->name('valido_ficha');
    Route::post('/fichas/rechazo/{id}', 'IngresoController@Frechazos')->name('rechazo_ficha');
    Route::get('/calendario','IngresoController@calendario')->name('calendar');
});



Route::resource('pagos', 'PagoController');
Route::resource('operacionDiarias', 'OperacionDiariaController');
Route::resource('pensions', 'PensionController');


Route::resource('operacionDiarias', 'OperacionDiariaController');

Route::resource('facturaPensions', 'FacturaPensionController');