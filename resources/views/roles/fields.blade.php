<!-- Id Central User Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_central_user', 'Id Central User:') !!}
    {!! Form::number('id_central_user', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Permiso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_permiso', 'Id Permiso:') !!}
    {!! Form::number('id_permiso', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('roles.index') !!}" class="btn btn-default">Cancel</a>
</div>
