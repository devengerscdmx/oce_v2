<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $roles->id !!}</p>
</div>

<!-- Id Central User Field -->
<div class="form-group">
    {!! Form::label('id_central_user', 'Id Central User:') !!}
    <p>{!! $roles->id_central_user !!}</p>
</div>

<!-- Id Permiso Field -->
<div class="form-group">
    {!! Form::label('id_permiso', 'Id Permiso:') !!}
    <p>{!! $roles->id_permiso !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $roles->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $roles->updated_at !!}</p>
</div>

