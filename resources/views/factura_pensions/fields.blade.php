<!-- Id Pen Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_pen', 'Id Pen:') !!}
    {!! Form::number('id_pen', null, ['class' => 'form-control']) !!}
</div>

<!-- Serie Field -->
<div class="form-group col-sm-6">
    {!! Form::label('serie', 'Serie:') !!}
    {!! Form::text('serie', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Folio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('folio', 'Folio:') !!}
    {!! Form::text('folio', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Fecha Timbrado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_timbrado', 'Fecha Timbrado:') !!}
    {!! Form::date('fecha_timbrado', null, ['class' => 'form-control','id'=>'fecha_timbrado']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fecha_timbrado').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Uuid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('uuid', 'Uuid:') !!}
    {!! Form::text('uuid', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Subtotal Factura Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtotal_factura', 'Subtotal Factura:') !!}
    {!! Form::number('subtotal_factura', null, ['class' => 'form-control']) !!}
</div>

<!-- Iva Factura Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iva_factura', 'Iva Factura:') !!}
    {!! Form::number('iva_factura', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Factura Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_factura', 'Total Factura:') !!}
    {!! Form::number('total_factura', null, ['class' => 'form-control']) !!}
</div>

<!-- Xml Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('XML', 'Xml:') !!}
    {!! Form::textarea('XML', null, ['class' => 'form-control']) !!}
</div>

<!-- Pdf Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('PDF', 'Pdf:') !!}
    {!! Form::textarea('PDF', null, ['class' => 'form-control']) !!}
</div>

<!-- Estatus Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estatus', 'Estatus:') !!}
    {!! Form::text('estatus', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Usocfdi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('usoCFDI', 'Usocfdi:') !!}
    {!! Form::text('usoCFDI', null, ['class' => 'form-control','maxlength' => 10,'maxlength' => 10]) !!}
</div>

<!-- Metodopago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('metodoPago', 'Metodopago:') !!}
    {!! Form::number('metodoPago', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('facturaPensions.index') }}" class="btn btn-default">Cancel</a>
</div>
