<div class="table-responsive">
    <table class="table" id="facturaPensions-table">
        <thead>
        <tr>
            <th>Id Pen</th>
            <th>Serie</th>
            <th>Folio</th>
            <th>Fecha Timbrado</th>
            <th>Uuid</th>
            <th>Subtotal Factura</th>
            <th>Iva Factura</th>
            <th>Total Factura</th>
            <th>Xml</th>
            <th>Pdf</th>
            <th>Estatus</th>
            <th>Usocfdi</th>
            <th>Metodopago</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($facturaPensions as $facturaPension)
            <tr>
                <td>{{ $facturaPension->id_pen }}</td>
                <td>{{ $facturaPension->serie }}</td>
                <td>{{ $facturaPension->folio }}</td>
                <td>{{ $facturaPension->fecha_timbrado }}</td>
                <td>{{ $facturaPension->uuid }}</td>
                <td>{{ $facturaPension->subtotal_factura }}</td>
                <td>{{ $facturaPension->iva_factura }}</td>
                <td>{{ $facturaPension->total_factura }}</td>
                <td>{{ $facturaPension->XML }}</td>
                <td>{{ $facturaPension->PDF }}</td>
                <td>{{ $facturaPension->estatus }}</td>
                <td>{{ $facturaPension->usoCFDI }}</td>
                <td>{{ $facturaPension->metodoPago }}</td>
                <td>
                    {!! Form::open(['route' => ['facturaPensions.destroy', $facturaPension->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('facturaPensions.edit', [$facturaPension->id]) }}"
                           class='btn btn-default btn-xs'>Editar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
