@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Factura Pension
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($facturaPension, ['route' => ['facturaPensions.update', $facturaPension->id], 'method' => 'patch']) !!}

                        @include('factura_pensions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection