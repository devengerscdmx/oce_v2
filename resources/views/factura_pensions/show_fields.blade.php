<!-- Id Pen Field -->
<div class="form-group">
    {!! Form::label('id_pen', 'Id Pen:') !!}
    <p>{{ $facturaPension->id_pen }}</p>
</div>

<!-- Serie Field -->
<div class="form-group">
    {!! Form::label('serie', 'Serie:') !!}
    <p>{{ $facturaPension->serie }}</p>
</div>

<!-- Folio Field -->
<div class="form-group">
    {!! Form::label('folio', 'Folio:') !!}
    <p>{{ $facturaPension->folio }}</p>
</div>

<!-- Fecha Timbrado Field -->
<div class="form-group">
    {!! Form::label('fecha_timbrado', 'Fecha Timbrado:') !!}
    <p>{{ $facturaPension->fecha_timbrado }}</p>
</div>

<!-- Uuid Field -->
<div class="form-group">
    {!! Form::label('uuid', 'Uuid:') !!}
    <p>{{ $facturaPension->uuid }}</p>
</div>

<!-- Subtotal Factura Field -->
<div class="form-group">
    {!! Form::label('subtotal_factura', 'Subtotal Factura:') !!}
    <p>{{ $facturaPension->subtotal_factura }}</p>
</div>

<!-- Iva Factura Field -->
<div class="form-group">
    {!! Form::label('iva_factura', 'Iva Factura:') !!}
    <p>{{ $facturaPension->iva_factura }}</p>
</div>

<!-- Total Factura Field -->
<div class="form-group">
    {!! Form::label('total_factura', 'Total Factura:') !!}
    <p>{{ $facturaPension->total_factura }}</p>
</div>

<!-- Xml Field -->
<div class="form-group">
    {!! Form::label('XML', 'Xml:') !!}
    <p>{{ $facturaPension->XML }}</p>
</div>

<!-- Pdf Field -->
<div class="form-group">
    {!! Form::label('PDF', 'Pdf:') !!}
    <p>{{ $facturaPension->PDF }}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{{ $facturaPension->estatus }}</p>
</div>

<!-- Usocfdi Field -->
<div class="form-group">
    {!! Form::label('usoCFDI', 'Usocfdi:') !!}
    <p>{{ $facturaPension->usoCFDI }}</p>
</div>

<!-- Metodopago Field -->
<div class="form-group">
    {!! Form::label('metodoPago', 'Metodopago:') !!}
    <p>{{ $facturaPension->metodoPago }}</p>
</div>

