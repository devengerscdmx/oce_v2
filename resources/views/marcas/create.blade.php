@extends('layouts.app')
@section('title','Marcas')
@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Añadir Marca</h3>
        </div>
    </div>
    <hr>

    <div class="col-12">
        @include('layouts.errors')

        {!! Form::open(['route' => 'marcas.store']) !!}
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                @include('marcas.fields')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
