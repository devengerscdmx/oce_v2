<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Número</th>
            <th>Estacionamiento</th>
            <th>Fecha recolección</th>
            <th>Importe TH</th>
            <th>Datos</th>
            <th>Imagenes</th>
            <th>Estado</th>
            <th colspan="2">Acción</th>
        </tr>
        <tr class="warning no-result">
            <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>
        <tbody>
        @foreach($ingresos as $ingreso)
            <tr>
                <td>{!! $ingreso->id !!}</td>
                <td>({!! $ingreso->no_est !!}) {!! $ingreso->nombre !!}</td>
                <td>{!! $ingreso->fecha_recoleccion !!}</td>
                <td>{!! $ingreso->importe !!}</td>
                <td>
                    <a type="button" class="btn-floating btn-sm default-color" data-toggle="modal"
                       data-target="#ModalUser{{$ingreso->id}}"><i class="fas fa-edit"></i></a>
                    <!-- Modal -->
                    <div class="modal fade" id="ModalUser{{$ingreso->id}}" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Información de la Ficha</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open() !!}
                                    <div class="md-form">
                                        {!! Form::label('emp', 'Empresa que recoge:') !!}
                                        {!! Form::text('emp', $ingreso->empresa, ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                    <div class="md-form">
                                        {!! Form::label('banco', 'Banco:') !!}
                                        {!! Form::text('banco', $ingreso->banco, ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                    <div class="md-form">
                                        {!! Form::label('vent', 'Venta del :') !!}
                                        {!! Form::text('vent', date_format($ingreso->venta, 'd-m-Y'), ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                    <div class="md-form">
                                        {!! Form::label('tipo', 'Tipo:') !!}
                                        {!! Form::text('tipo', $ingreso->tipo, ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    <a type="button" class="btn-floating btn-sm default-color" data-toggle="modal"
                       data-target="#basicExampleModal{{$ingreso->id}}"><i class="far fa-file-image"></i></a>

                    <!-- Modal -->

                    <div class="modal fade" id="basicExampleModal{{$ingreso->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Imagen</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body view overlay zoom">
                                    <img src="data:image/jpg;base64,{{$ingreso->foto}}" style="width: 100%;">
                                    <br>
                                    <img src="data:image/jpg;base64,{{$ingreso->firma}}" style="width: 100%;">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    @switch($ingreso->valido)
                        @case('pendiente')
                        <a type="button" class="btn-floating btn-sm primary-color material-tooltip-main" data-toggle="tooltip"
                           data-placement="bottom" title="Pendiente"><i class="fas fa-clock"></i></a>
                        @break
                        @case('corregida')
                        <a type="button" class="btn-floating btn-sm warning-color material-tooltip-main" data-toggle="tooltip"
                           data-placement="bottom" title="Pendiente de corrección"><i class="fas fa-exclamation"></i></a>
                        @break
                        @case('no_recolec')
                        <a type="button" class="btn-floating btn-sm danger-color material-tooltip-main" data-toggle="tooltip"
                           data-placement="bottom" title="No paso el camión"><i class="fas fa-truck"></i></a>
                        @break
                    @endswitch
                </td>
                <td>
                    <a type="button" class="btn-floating btn-sm success-color" href="{{route('valido_ficha',$ingreso->id)}}"><i class="far fa-check-circle"></i></a>
                </td>
                <td>
                    <a type="button" class="btn-floating btn-sm danger-color material-tooltip-main" data-toggle="modal"
                       data-target="#ModalRechazo_{!! $ingreso->id !!}"><i class="fas fa-exclamation"></i></a>

                    <!-- Modal -->
                    <div class="modal fade" id="ModalRechazo_{!! $ingreso->id !!}" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">¿Qué tiene que corregir?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                {!! Form::open(['route' => ['rechazo_ficha',$ingreso->id]]) !!}
                                <div class="modal-body">
                                    <div class="md-form">
                                        <textarea id="form7" class="md-textarea form-control" rows="3" name="motivo"></textarea>
                                        <label for="form7">Motivo</label>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    {!! Form::submit('Corregir', ['class' => 'btn btn-danger']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<br>
<br>
