<!-- Id Est Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_est', 'Id Est:') !!}
    {!! Form::number('id_est', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Empresa Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_empresa', 'Id Empresa:') !!}
    {!! Form::number('id_empresa', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Banco Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_banco', 'Id Banco:') !!}
    {!! Form::number('id_banco', null, ['class' => 'form-control']) !!}
</div>

<!-- Numero Field -->
<div class="form-group col-sm-6">
    {!! Form::label('numero', 'Numero:') !!}
    {!! Form::number('numero', null, ['class' => 'form-control']) !!}
</div>

<!-- Venta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('venta', 'Venta:') !!}
    {!! Form::date('venta', null, ['class' => 'form-control','id'=>'venta']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#venta').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Fecha Recoleccion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_recoleccion', 'Fecha Recoleccion:') !!}
    {!! Form::date('fecha_recoleccion', null, ['class' => 'form-control','id'=>'fecha_recoleccion']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha_recoleccion').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Importe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('importe', 'Importe:') !!}
    {!! Form::number('importe', null, ['class' => 'form-control']) !!}
</div>

<!-- Valido Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valido', 'Valido:') !!}
    {!! Form::text('valido', null, ['class' => 'form-control']) !!}
</div>

<!-- Coment Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('coment', 'Coment:') !!}
    {!! Form::textarea('coment', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo', 'Tipo:') !!}
    {!! Form::text('tipo', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ingresos.index') !!}" class="btn btn-default">Cancel</a>
</div>
