<div class="table-responsive">
    <table class="table" id="tipoTickets-table">
        <thead>
            <tr>
                <th>Tipo</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tipoTickets as $tipoTicket)
            <tr>
                <td>{!! $tipoTicket->tipo !!}</td>
                <td>
                    {!! Form::open(['route' => ['tipoTickets.destroy', $tipoTicket->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('tipoTickets.show', [$tipoTicket->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('tipoTickets.edit', [$tipoTicket->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
