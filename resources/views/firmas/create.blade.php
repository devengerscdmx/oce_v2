@extends('layouts.app')
@section('title', 'Firmas')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Firmas</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row justify-content-md-center">
            <div class="col-md-6">
                {!! Form::open(['route' => 'firmas.store', 'enctype' => 'multipart/form-data']) !!}
                @include('firmas.fields')
                {!! Form::close() !!}
            </div>
        </div>
        <br>
    </section>
@endsection
