<!-- Id Centraluser Field -->
{!! Form::hidden('id_CentralUser', Auth::user()->id) !!}

<!-- Nombre Field -->
<div class="md-form">
    <i class="fas fa-user-circle prefix"></i>
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Field -->
<div class="md-form">
    <i class="fas fa-file-alt prefix"></i>
    {!! Form::label('tipo', 'Tipo de Documento:') !!}
    {!! Form::text('tipo', null, ['class' => 'form-control']) !!}
</div>

<!-- Asunto Field -->
<div class="md-form">
    <i class="fas fa-file-signature prefix"></i>
    {!! Form::label('asunto', 'Asunto:') !!}
    {!! Form::text('asunto', null, ['class' => 'form-control']) !!}
</div>

<!-- Responsable Field -->
<select class="mdb-select md-form colorful-select dropdown-success" name="responsable">
    <option value="" selected disabled>Responsable</option>
    <optgroup label="Directores">
        <option value="Cesar Morales">Cesar Morales</option>
        <option value="Jose Ignacio Barajas">Jose Ignacio Barajas</option>
        <option value="Luis Sandoval">Luis Sandoval</option>
        <option value="Sergio Jurado">Sergio Jurado</option>
    </optgroup>
    <optgroup label="Sub directores">
        <option value="Miguel Aragon">Miguel Aragon</option>
    </optgroup>
</select>

<div class="file-field">
    <div class="btn btn-primary btn-sm float-left">
        <span>Imagen de Firma</span>
        <input type="file" name="foto" id="foto"
               accept="image/*">
    </div>
    <div class="file-path-wrapper">
        <input class="file-path validate" name="foto"
               id="foto" accept="image/*" type="text"
               placeholder="Cargar archivo">
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('firmas.index') !!}" class="btn btn-default">Cancelar</a>
</div>

<script>
    @push('scripts')
    $(document).ready(function () {
        $('.mdb-select').materialSelect();
    });
    @endpush
</script>
