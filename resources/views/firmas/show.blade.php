@extends('layouts.app')
@section('title', 'Firmas')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Firmas</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            @include('firmas.show_fields')
            <a href="{!! route('firmas.index') !!}" class="btn btn-default">Atras</a>
        </div>
        <br>
    </section>
@endsection
