<!-- Id Centraluser Field -->
<div class="form-group">
    {!! Form::label('id_CentralUser', 'Id Centraluser:') !!}
    <p>{!! $firma->id_CentralUser !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $firma->nombre !!}</p>
</div>

<!-- Tipo Field -->
<div class="form-group">
    {!! Form::label('tipo', 'Tipo:') !!}
    <p>{!! $firma->tipo !!}</p>
</div>

<!-- Asunto Field -->
<div class="form-group">
    {!! Form::label('asunto', 'Asunto:') !!}
    <p>{!! $firma->asunto !!}</p>
</div>

<!-- Responsable Field -->
<div class="form-group">
    {!! Form::label('responsable', 'Responsable:') !!}
    <p>{!! $firma->responsable !!}</p>
</div>

