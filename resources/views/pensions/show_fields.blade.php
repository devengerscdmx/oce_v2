<!-- Montopension Field -->
<div class="form-group">
    {!! Form::label('montoPension', 'Montopension:') !!}
    <p>{{ $pension->montoPension }}</p>
</div>

<!-- Tipopension Field -->
<div class="form-group">
    {!! Form::label('tipoPension', 'Tipopension:') !!}
    <p>{{ $pension->tipoPension }}</p>
</div>

<!-- Contrato Field -->
<div class="form-group">
    {!! Form::label('contrato', 'Contrato:') !!}
    <p>{{ $pension->contrato }}</p>
</div>

<!-- Solicitudcontrato Field -->
<div class="form-group">
    {!! Form::label('solicitudContrato', 'Solicitudcontrato:') !!}
    <p>{{ $pension->solicitudContrato }}</p>
</div>

<!-- Comprobantedomicilio Field -->
<div class="form-group">
    {!! Form::label('comprobanteDomicilio', 'Comprobantedomicilio:') !!}
    <p>{{ $pension->comprobanteDomicilio }}</p>
</div>

<!-- Ine Field -->
<div class="form-group">
    {!! Form::label('ine', 'Ine:') !!}
    <p>{{ $pension->ine }}</p>
</div>

<!-- Licencia Field -->
<div class="form-group">
    {!! Form::label('licencia', 'Licencia:') !!}
    <p>{{ $pension->licencia }}</p>
</div>

<!-- Rfc Field -->
<div class="form-group">
    {!! Form::label('rfc', 'Rfc:') !!}
    <p>{{ $pension->rfc }}</p>
</div>

<!-- Tarjetacirculacion Field -->
<div class="form-group">
    {!! Form::label('tarjetaCirculacion', 'Tarjetacirculacion:') !!}
    <p>{{ $pension->tarjetaCirculacion }}</p>
</div>

<!-- Notarjeta Field -->
<div class="form-group">
    {!! Form::label('noTarjeta', 'Notarjeta:') !!}
    <p>{{ $pension->noTarjeta }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $pension->status }}</p>
</div>

