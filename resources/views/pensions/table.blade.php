<div class="table-responsive">
    <table class="table" id="pensions-table">
        <thead>
            <tr>
                <th>Montopension</th>
        <th>Tipopension</th>
        <th>Contrato</th>
        <th>Solicitudcontrato</th>
        <th>Comprobantedomicilio</th>
        <th>Ine</th>
        <th>Licencia</th>
        <th>Rfc</th>
        <th>Tarjetacirculacion</th>
        <th>Notarjeta</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($pensions as $pension)
            <tr>
                <td>{{ $pension->montoPension }}</td>
            <td>{{ $pension->tipoPension }}</td>
            <td>{{ $pension->contrato }}</td>
            <td>{{ $pension->solicitudContrato }}</td>
            <td>{{ $pension->comprobanteDomicilio }}</td>
            <td>{{ $pension->ine }}</td>
            <td>{{ $pension->licencia }}</td>
            <td>{{ $pension->rfc }}</td>
            <td>{{ $pension->tarjetaCirculacion }}</td>
            <td>{{ $pension->noTarjeta }}</td>
            <td>{{ $pension->status }}</td>
                <td>
                    {!! Form::open(['route' => ['pensions.destroy', $pension->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('pensions.edit', [$pension->id]) }}" class='btn btn-default btn-xs'>EDITAR</a>
                        {!! Form::button('ELIMINAR', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<br><br>
