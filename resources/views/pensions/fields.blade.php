<div class="container">
    <!-- Montopension Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('montoPension', 'Montopension:') !!}
        {!! Form::number('montoPension', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Tipopension Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('tipoPension', 'Tipopension:') !!}
        {!! Form::text('tipoPension', null, ['class' => 'form-control','maxlength' => 15,'maxlength' => 15]) !!}
    </div>

    <!-- Contrato Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('contrato', 'Contrato:') !!}
        {!! Form::textarea('contrato', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Solicitudcontrato Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('solicitudContrato', 'Solicitudcontrato:') !!}
        {!! Form::textarea('solicitudContrato', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Comprobantedomicilio Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('comprobanteDomicilio', 'Comprobantedomicilio:') !!}
        {!! Form::textarea('comprobanteDomicilio', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Ine Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('ine', 'Ine:') !!}
        {!! Form::textarea('ine', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Licencia Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('licencia', 'Licencia:') !!}
        {!! Form::textarea('licencia', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Rfc Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('rfc', 'Rfc:') !!}
        {!! Form::textarea('rfc', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Tarjetacirculacion Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('tarjetaCirculacion', 'Tarjetacirculacion:') !!}
        {!! Form::textarea('tarjetaCirculacion', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Notarjeta Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('noTarjeta', 'Notarjeta:') !!}
        {!! Form::number('noTarjeta', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Status Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('status', 'Status:') !!}
        {!! Form::text('status', null, ['class' => 'form-control','maxlength' => 2,'maxlength' => 2]) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('pensions.index') }}" class="btn btn-default">Cancelar</a>
    </div>
</div>
