@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Editar pension</h3>
            </div>
        </div>
        <hr>
        @include('flash::message')
        {!! Form::model($pension, ['route' => ['pensions.update', $pension->id], 'method' => 'patch']) !!}

        @include('pensions.fields')

        {!! Form::close() !!}
    </section>
@endsection

