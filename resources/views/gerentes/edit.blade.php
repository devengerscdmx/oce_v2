@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Editar gerente</h3>
            </div>
        </div>
        <hr>
        @include('flash::message')
        {!! Form::model($gerente, ['route' => ['gerentes.update', $gerente->id], 'method' => 'patch']) !!}

        <br>
        <div class="md-form input-group">
            <select class="mdb-select colorful-select dropdown-default form-control" searchable="Buscar..."
                    name="Estacionamiento">
                <option value="0" selected disabled>Estacionamiento</option>
                @forelse($estacionamientos as $item)
                    <option value="{{$item->no_est}}">{{$item->no_est}}</option>
                @empty
                    <option value="0">Sin estacionamientos registrados</option>
                @endforelse
            </select>
            <label class="mdb-main-label">Estacionamiento</label>
            {!! Form::text('Nombre', $gerente->name, ['class' => 'form-control', 'placeholder' => 'Nombre']) !!}
            {!! Form::email('Email', $gerente->email, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
            {!! Form::password('Contraseña', ['class' => 'form-control', 'placeholder' => 'Contraseña', 'type' => 'password', 'id' => 'password']) !!}
        </div>

        <div class="alert alert-warning" role="alert" id="contra_erronea" style="display:none;">
            Por favor, escriba la contraseña con el formato indicado
        </div>
        @include('layouts.errors')


        <section id="pswd_info" style="padding-bottom: 100px;">
            <h4>La contraseña debe contener lo siguiente:</h4>
            <ul>
                <li id="letter" class="invalid">Al menos: <strong>Una letra</strong>
                </li>
                <li id="capital" class="invalid">Al menos: <strong>Una letra en mayuscula</strong>
                </li>
                <li id="number" class="invalid">Al menos: <strong>Un numero</strong>
                </li>
                <li id="length" class="invalid">Al menos: <strong>8 caracteres</strong>
                </li>
            </ul>
        </section>

        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <button type="submit" value="Cargar" class="btn btn-primary" id="cargar" onclick="validar()">Registrar</button>
            <a href="{!! route('gerentes.index') !!}" class="btn btn-default">Cancelar</a>
        </div>

        <br><br><br>

        <script>
            @push('scripts')
            // Material Select Initialization
            $(document).ready(function () {
                $('.mdb-select').materialSelect();
            });
            @endpush

            @push('scripts')
            $(document).ready(function () {
                var longitud = false,
                    minuscula = false,
                    numero = false,
                    mayuscula = false;
                $('input[type=password]').keyup(function () {
                    var pswd = $(this).val();
                    if (pswd.length < 8) {
                        $('#length').removeClass('valid').addClass('invalid');
                        longitud = false;
                    } else {
                        $('#length').removeClass('invalid').addClass('valid');
                        longitud = true;
                    }

                    //validate letter
                    if (pswd.match(/[A-z]/)) {
                        $('#letter').removeClass('invalid').addClass('valid');
                        minuscula = true;
                    } else {
                        $('#letter').removeClass('valid').addClass('invalid');
                        minuscula = false;
                    }

                    //validate capital letter
                    if (pswd.match(/[A-Z]/)) {
                        $('#capital').removeClass('invalid').addClass('valid');
                        mayuscula = true;
                    } else {
                        $('#capital').removeClass('valid').addClass('invalid');
                        mayuscula = false;
                    }

                    //validate number
                    if (pswd.match(/\d/)) {
                        $('#number').removeClass('invalid').addClass('valid');
                        numero = true;
                    } else {
                        $('#number').removeClass('valid').addClass('invalid');
                        numero = false;
                    }
                }).focus(function () {
                    $('#pswd_info').show();
                }).blur(function () {
                    $('#pswd_info').hide();
                });

                if (longitud == true && minuscula == true && mayuscula == true && numero == true) {
                    document.getElementById("cargar").submit();
                } else {
                    $('#contra_erronea').show();
                    event.preventDefault();

                }

            });
            @endpush
        </script>

        <style>

            #pswd_info {
                position: absolute;
                bottom: 30px;
                /* IE Specific */
                right: 105px;
                width: 300px;
                padding: 15px;
                background: #fefefe;
                font-size: .875em;
            }

            #pswd_info h4 {
                margin: 0 0 10px 0;
                padding: 0;
                font-weight: normal;
            }

            #pswd_info::before {
                content: "\25B2";
                position: absolute;
                top: -15px;
                left: 45%;
                font-size: 12px;
                line-height: 12px;
                color: #ddd;
                text-shadow: none;
                display: block;
            }

            .invalid {
                color: #ec3f41;
            }

            .valid {
                color: #3a7d34;
            }

            #pswd_info {
                display: none;
            }
        </style>

        {!! Form::close() !!}
    </section>
@endsection