<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Estacionamiento</th>
            <th>Name</th>
            <th>Email</th>
            <th>Acción</th>
        </tr>
        <tr class="warning no-result">
            <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>
        <tbody>
        @foreach($gerentes as $gerente)
            <tr>
                <td>{!! $gerente->id_est !!}</td>
                <td>{!! $gerente->name !!}</td>
                <td>{!! $gerente->email !!}</td>
                <td>
                    {!! Form::open(['route' => ['gerentes.destroy', $gerente->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('gerentes.edit', [$gerente->id]) !!}"
                           class='btn btn-default btn-xs white-text'>visualizar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
</div>

<br>
<br>
