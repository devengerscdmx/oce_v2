@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Tipo Pension
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($tipoPension, ['route' => ['tipoPensions.update', $tipoPension->id], 'method' => 'patch']) !!}

                        @include('tipo_pensions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection