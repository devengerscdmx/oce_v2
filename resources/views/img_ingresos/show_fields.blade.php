<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $imgIngreso->id !!}</p>
</div>

<!-- Id Ingreso Field -->
<div class="form-group">
    {!! Form::label('id_ingreso', 'Id Ingreso:') !!}
    <p>{!! $imgIngreso->id_ingreso !!}</p>
</div>

<!-- Foto Field -->
<div class="form-group">
    {!! Form::label('foto', 'Foto:') !!}
    <p>{!! $imgIngreso->foto !!}</p>
</div>

<!-- Firma Field -->
<div class="form-group">
    {!! Form::label('firma', 'Firma:') !!}
    <p>{!! $imgIngreso->firma !!}</p>
</div>

