<!-- Id Ingreso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_ingreso', 'Id Ingreso:') !!}
    {!! Form::number('id_ingreso', null, ['class' => 'form-control']) !!}
</div>

<!-- Foto Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('foto', 'Foto:') !!}
    {!! Form::textarea('foto', null, ['class' => 'form-control']) !!}
</div>

<!-- Firma Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('firma', 'Firma:') !!}
    {!! Form::textarea('firma', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('imgIngresos.index') !!}" class="btn btn-default">Cancel</a>
</div>
