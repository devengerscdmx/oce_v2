@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Info Cliente
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($infoCliente, ['route' => ['infoClientes.update', $infoCliente->id], 'method' => 'patch']) !!}

                        @include('info_clientes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection