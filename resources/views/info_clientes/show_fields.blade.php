<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $infoCliente->id !!}</p>
</div>

<!-- Id Cliente Field -->
<div class="form-group">
    {!! Form::label('id_cliente', 'Id Cliente:') !!}
    <p>{!! $infoCliente->id_cliente !!}</p>
</div>

<!-- Rfc Field -->
<div class="form-group">
    {!! Form::label('RFC', 'Rfc:') !!}
    <p>{!! $infoCliente->RFC !!}</p>
</div>

<!-- Razon Social Field -->
<div class="form-group">
    {!! Form::label('Razon_social', 'Razon Social:') !!}
    <p>{!! $infoCliente->Razon_social !!}</p>
</div>

<!-- Calle Field -->
<div class="form-group">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{!! $infoCliente->calle !!}</p>
</div>

<!-- No Ext Field -->
<div class="form-group">
    {!! Form::label('no_ext', 'No Ext:') !!}
    <p>{!! $infoCliente->no_ext !!}</p>
</div>

<!-- No Int Field -->
<div class="form-group">
    {!! Form::label('no_int', 'No Int:') !!}
    <p>{!! $infoCliente->no_int !!}</p>
</div>

<!-- Colonia Field -->
<div class="form-group">
    {!! Form::label('colonia', 'Colonia:') !!}
    <p>{!! $infoCliente->colonia !!}</p>
</div>

<!-- Municipio Field -->
<div class="form-group">
    {!! Form::label('municipio', 'Municipio:') !!}
    <p>{!! $infoCliente->municipio !!}</p>
</div>

<!-- Estado Field -->
<div class="form-group">
    {!! Form::label('estado', 'Estado:') !!}
    <p>{!! $infoCliente->estado !!}</p>
</div>

<!-- Pais Field -->
<div class="form-group">
    {!! Form::label('pais', 'Pais:') !!}
    <p>{!! $infoCliente->pais !!}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{!! $infoCliente->cp !!}</p>
</div>

<!-- Act Na Field -->
<div class="form-group">
    {!! Form::label('act_na', 'Act Na:') !!}
    <p>{!! $infoCliente->act_na !!}</p>
</div>

<!-- Ine Field -->
<div class="form-group">
    {!! Form::label('ine', 'Ine:') !!}
    <p>{!! $infoCliente->ine !!}</p>
</div>

<!-- Tar Circ Field -->
<div class="form-group">
    {!! Form::label('tar_circ', 'Tar Circ:') !!}
    <p>{!! $infoCliente->tar_circ !!}</p>
</div>

<!-- Placas Field -->
<div class="form-group">
    {!! Form::label('placas', 'Placas:') !!}
    <p>{!! $infoCliente->placas !!}</p>
</div>

<!-- Tel Field -->
<div class="form-group">
    {!! Form::label('tel', 'Tel:') !!}
    <p>{!! $infoCliente->tel !!}</p>
</div>

<!-- Pension Field -->
<div class="form-group">
    {!! Form::label('pension', 'Pension:') !!}
    <p>{!! $infoCliente->pension !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $infoCliente->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $infoCliente->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $infoCliente->updated_at !!}</p>
</div>

