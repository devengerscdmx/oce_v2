@extends('layouts.app')
@section('title', 'Usuarios')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Nuevo Usuario</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                {!! Form::open(['route' => 'central_users.store']) !!}
                @include('central_users.fields')
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection
