<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Rol</th>
            <th>Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($centralUsers as $centralUser)
            <tr>
                <td>{!! $centralUser->name !!}</td>
                <td>{!! $centralUser->email !!}</td>
                <td>{!! $centralUser->nombre !!}</td>
                <td>
                    {!! Form::open(['route' => ['central_users.destroy', $centralUser->id], 'method' => 'delete' ]) !!}
                    <div class='btn-group'>
                        <a href="{!! route('central_users.edit', [$centralUser->id]) !!}"
                           class='btn btn-default btn-xs white-text' onclick ="return confirm('¿Estas seguro?')">visualizar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$centralUsers->links()}}
</div>
<br>
