<div class="table-responsive ">
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Serie</th>
            <th>Folio</th>
            <th>Fecha Timbrado</th>
            <th>Razón Social</th>
            <th>Subtotal Factura</th>
            <th>Iva Factura</th>
            <th>Total Factura</th>
            <th>XML</th>
            <th>PDF</th>
            <th>Estatus</th>
            <th colspan="3">Reenviar</th>
        </tr>
        <tr class="warning no-result">
            <td colspan="14" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>
        <tbody>
        @foreach($facturas as $factura)
            <tr>
                <td>{!! $factura->serie !!}</td>
                <td>{!! $factura->folio !!}</td>
                <td>{!! $factura->fecha_timbrado !!}</td>
                <td>{!! $factura->Razon_social !!}</td>
                <td>{!! $factura->subtotal_factura !!}</td>
                <td>{!! $factura->iva_factura !!}</td>
                <td>{!! $factura->total_factura !!}</td>
                <td><a href="{!! route('xml', [$factura->id]) !!}"
                       class='btn-floating btn-sm btn-blue-grey'><i class="fas fa-download"></i></a></td>
                <td><a href="{!! route('pdf', [$factura->id]) !!}"
                       class='btn-floating btn-sm btn-blue-grey'><i class="fas fa-download"></i></a></td>
                <td><a type="button" class="btn-floating btn-sm success-color material-tooltip-main" data-toggle="tooltip"
                       data-placement="bottom" title="Factura Timbrada"><i class="fas fa-money-check-alt"></i></a></td>
                <td><!-- Button trigger modal -->
                    <a type="button" class="btn-floating btn-sm btn-info" data-toggle="modal"
                       data-target="#basicExampleModal{{$factura->id}}">
                        <i class="fas fa-envelope"></i>
                    </a>

                    <!-- Modal -->
                    <div class="modal fade" id="basicExampleModal{{$factura->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Escriba correo a enviar</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                {!! Form::open(['route' => 'email']) !!}
                                <div class="modal-body">
                                    {!! Form::text('correo', $factura->email , ['class' => 'form-control', 'name' => 'correo']) !!}
                                    {!! Form::hidden('tag', $factura->id , ['class' => 'form-control', 'name' => 'tag']) !!}
                                </div>
                                <div class="modal-footer" id="carga">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    {!! Form::submit('Enviar', ['class' => 'btn btn-primary', 'id' => 'enviar']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
    @if($bus)
        {{ $facturas->links() }}
    @endif
</div>
