@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                @foreach($estacionamiento as $estacionamiento)
                    <h4 class="card-header-title">Generar factura para
                        estacionamiento: {!! $estacionamiento->nombre !!}</h4>
            </div>
        </div>
        <hr>
        @include('flash::message')
        @include('layouts.errors')

        {!! Form::open(['route' => 'timbrar', 'method' => 'post',  'enctype' => 'multipart/form-data']) !!}

        <h5 class="card-header-title text-center">Datos del cliente</h5>

        <div class="md-form input-group">
            {!! Form::hidden('no_est', $estacionamiento->no_est, ['class' => 'form-control']) !!}
            {!! Form::hidden('razonsocial', $estacionamiento->Razon_social, ['class' => 'form-control']) !!}
            {!! Form::hidden('rfc_empresa', $estacionamiento->RFC, ['class' => 'form-control']) !!}
            {!! Form::hidden('regimen_empresa', $estacionamiento->Regimen_fiscal, ['class' => 'form-control']) !!}

            {!! Form::text('Nombre_cliente', null, ['class' => 'form-control', 'id' => 'nombre_cliente', 'placeholder' => 'Razon Social', 'required']) !!}
            {!! Form::text('RFC', null, ['class' => 'form-control', 'id' => 'RFC', 'placeholder' => 'RFC', 'required']) !!}
            {!! Form::email('Correo', null, ['class' => 'form-control', 'id' => 'Correo', 'placeholder' => 'Correo', 'required']) !!}
            <select class="mdb-select colorful-select dropdown-default md-form input-group"
                    searchable="Buscar aquí.." id="Uso_CFDI" name="Uso_CFDI" required>
                <option value="">Elija un Uso CFDI*</option>
                <option value="G01">(G01) Adquisición de mercancias</option>
                <option value="G02">(G02) Devoluciones, descuentos o bonificaciones
                </option>
                <option value="G03">(G03) Gastos en general</option>
                <option value="I01">(I01) Construcciones</option>
                <option value="I02">(I02) Mobilario y equipo de oficina por
                    inversiones
                </option>
                <option value="I03">(I03) Equipo de transporte</option>
                <option value="I04">(I04) Equipo de computo y accesorios</option>
                <option value="I05">(I05) Dados, troqueles, moldes, matrices y
                    herramental
                </option>
                <option value="I06">(I06) Comunicaciones telefónicas</option>
                <option value="I07">(I07) Comunicaciones satelitales</option>
                <option value="I08">(I08) Otra maquinaria y equipo</option>
                <option value="D01">(D01) Honorarios médicos, dentales y gastos
                    hospitalarios.
                </option>
                <option value="D02">(D02) Gastos médicos por incapacidad o
                    discapacidad
                </option>
                <option value="D03">(D03) Gastos funerales.</option>
                <option value="D04">(D04) Donativos.</option>
                <option value="D05">(D05) Intereses reales efectivamente pagados por
                    créditos hipotecarios (casa habitación).
                </option>
                <option value="D06">(D06) Aportaciones voluntarias al SAR.</option>
                <option value="D07">(D07) Primas por seguros de gastos médicos.
                </option>
                <option value="D08">(D08) Gastos de transportación escolar
                    obligatoria.
                </option>
                <option value="D09">(D09) Depósitos en cuentas para el ahorro, primas
                    que
                    tengan como base planes de pensiones.
                </option>
                <option value="D10">(D10) Pagos por servicios educativos
                    (colegiaturas)
                </option>
                <option value="P01">(P01) Por definir</option>
            </select>
        </div>
        <!--Termina primer div de form-->

        <h5 class="card-header-title text-center">Datos del pago</h5>

        <div class="md-form input-group">
            <select class="browser-default custom-select" name="concepto" required>
                <option value="" disabled selected>Conceptos</option>
                <option value="1">Tarifas del Parqueadero</option>
            </select>
            <select class="browser-default custom-select" name="metodo_pago" required>
                <option value="" disabled selected>Método de Pago</option>
                <option value="PUE">Pago en una sola exhibición</option>
                <option value="PPD">Pago en parcialidades o diferido</option>
            </select>
            <select class="browser-default custom-select" name="forma_pago" required>
                <option value="" disabled selected>Forma de pago</option>
                <option value="01">(01) Efectivo</option>
                <option value="02">(02) Cheque nominativo</option>
                <option value="03">(03) Transferencia electrónica de fondos</option>
                <option value="04">(04) Tarjeta de crédito</option>
                <option value="05">(05) Monedero electrónico</option>
                <option value="06">(06) Dinero electrónico</option>
                <option value="08">(08) Vales de despensa</option>
                <option value="28">(28) Tarjeta de débito</option>
                <option value="29">(29) Tarjeta de servicio</option>
                <option value="99">(99) Otros</option>
            </select>
        </div>

        <h5 class="card-header-title text-center">Conceptos</h5>

        <div class="md-form input-group">
            {!! Form::hidden('Cantidad', 1, ['class' => 'form-control', 'id' => 'cantidad', 'placeholder' => 'Cantidad', 'min' => '1', 'pattern' => '^[0-9]+', 'required']) !!}
            {!! Form::number('Precio', null, ['class' => 'form-control', 'id' => 'precio', 'placeholder' => 'Precio', 'min' => '1', 'pattern' => '^[0-9]+', 'required', 'step' => '0.01']) !!}
        </div>

        <h5 class="card-header-title text-center"></h5>

        <div class="md-form input-group">
            {!! Form::text('Comentario', null, ['class' => 'form-control', 'id' => 'comentario', 'placeholder' => 'Comentario']) !!}

                <div class="file-field">
                    <div class="btn btn-primary btn-sm float-left">
                        <span>Cargar foto</span>
                        <input type="file" name="foto" id="foto"
                               accept="image/*">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" name="foto"
                               id="foto" accept="image/*" type="text"
                               placeholder="Cargar archivo">
                    </div>
                </div>

        </div>
        <div class="md-form">
            {!! Form::submit('Timbrar factura', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('factura.index') !!}" class="btn btn-default">Cancelar</a>
        </div>
        {!! Form::close(); !!}

        <!--Termina segundo div de form-->
        @endforeach

    </section>
    <br>
@endsection

<script>
    @push('scripts')
    $(document).ready(function () {
        $('.mdb-select').materialSelect();
    });
    @endpush
</script>

