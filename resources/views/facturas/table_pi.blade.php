<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th># Estacionamiento</th>
            <th>Total</th>
            <th>Fecha de solicitud</th>
            <th>Razón Social</th>
            <th>Datos</th>
            <th>Imagen</th>
            <th>Estado</th>
            <th colspan="2">Acción</th>
        </tr>
        <tr class="warning no-result">
            <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>
        <tbody>
            <tr>
                @foreach($tickets as $ticket)
                <td>({!! $ticket->id_est !!}){!! $ticket->park !!}</td>
                <td>{!! $ticket->total_ticket !!}</td>
                <td>{!! $ticket->fecha_emision !!}</td>
                <td>{!! $ticket->Razon_social !!}</td>
                <td>
                    <a type="button" class="btn-floating btn-sm default-color" data-toggle="modal"
                       data-target="#ModalUser{{$ticket->id}}"><i class="fas fa-edit"></i></a>
                    <!-- Modal -->
                    <div class="modal fade" id="ModalUser{{$ticket->id}}" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Usuario</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open() !!}
                                    <div class="md-form">
                                        {!! Form::label('RFC', 'RFC:') !!}
                                        {!! Form::text('RFC', $ticket->RFC, ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                    <div class="md-form">
                                        {!! Form::label('RS', 'Razón Social:') !!}
                                        {!! Form::text('RS', $ticket->Razon_social, ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                    <div class="md-form">
                                        {!! Form::label('Email', 'Email:') !!}
                                        {!! Form::text('Email', $ticket->email, ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                    <div class="md-form">
                                        {!! Form::label('est', 'Estacionamientos:') !!}
                                        {!! Form::text('est', $ticket->park, ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    <a href="{!! route('img', [$ticket->id]) !!}"
                       class='btn-floating btn-sm btn-blue-grey'><i class="fas fa-download"></i></a>
                </td>
                <td>
                    @if($ticket->id_est == 1 || $ticket->estatus == 'sin_fact')
                        <a type="button" class="btn-floating btn-sm warning-color" data-toggle="modal"
                           data-target="#ModalModificar_{!! $ticket->id !!}"><i class="fas fa-exclamation-triangle"></i></a>
                    @else
                        <a type="button" class="btn-floating btn-sm success-color" data-toggle="modal"
                           data-target="#ModalModificar_{!! $ticket->id !!}"><i class="fas fa-clipboard-check"></i></a>
                @endif
                <!-- Modal -->
                    <div class="modal fade" id="ModalModificar_{!! $ticket->id !!}" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modificar Estacionamiento</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                {!! Form::open(['route' => ['no_est',$ticket->id]]) !!}
                                <div class="modal-body">
                                    <select class="mdb-select md-form colorful-select dropdown-success" searchable="Buscar..." name="no_est">
                                        <option value="" selected disabled>Seleccione un estacionamiento</option>
                                        @forelse($est as $item)
                                            <option value="{{$item->no_est}}">({!! $item->no_est !!}) {{ $item->nombre}}</option>
                                        @empty
                                            <option value="">Sin estacionamientos</option>
                                        @endforelse
                                    </select>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    <a type="button" class="btn-floating btn-sm success-color" href="{{route('valido',$ticket->id)}}"><i class="far fa-check-circle"></i></a>
                </td>
                <td>
                    <a type="button" class="btn-floating btn-sm danger-color material-tooltip-main" data-toggle="modal"
                       data-target="#ModalRechazo_{!! $ticket->id !!}"><i class="far fa-times-circle"></i></a>

                    <!-- Modal -->
                    <div class="modal fade" id="ModalRechazo_{!! $ticket->id !!}" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">¿Por que se rechaza?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                {!! Form::open(['route' => ['rechazo',$ticket->id]]) !!}
                                <div class="modal-body">
                                    <div class="md-form">
                                        <textarea id="form7" class="md-textarea form-control" rows="3" name="motivo"></textarea>
                                        <label for="form7">Motivo</label>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    {!! Form::submit('Rechazar', ['class' => 'btn btn-danger']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$tickets->links()}}
</div>

<br>
<br>

<script>
    @push('scripts')
    $(document).ready(function () {
        $('.mdb-select').materialSelect();
    });
    @endpush
</script>
