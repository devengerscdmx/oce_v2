<div class="table-responsive">
    <table class="table" id="tipoPagos-table">
        <thead>
            <tr>
                <th>Tipo</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tipoPagos as $tipoPago)
            <tr>
                <td>{!! $tipoPago->tipo !!}</td>
                <td>
                    {!! Form::open(['route' => ['tipoPagos.destroy', $tipoPago->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('tipoPagos.show', [$tipoPago->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('tipoPagos.edit', [$tipoPago->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
