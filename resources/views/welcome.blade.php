@extends('layouts.app')

@section('title', 'Inicio')

@section('content')
    <div class="row justify-content-center">
        <!-- Jumbotron -->
        <!-- Card -->
        <div class="card testimonial-card ">

            <!-- Background color -->
            <div class="card-up default-color"></div>

            <!-- Avatar -->
            <div class="avatar mx-auto white">
                <img src="{{asset('logo/Logo.jpg')}}" class="rounded-circle" alt="OCE">
            </div>

            <!-- Content -->
            <div class="card-body">
                <!-- Name -->
                <h4 class="card-title">OPERADORA CENTRAL DE ESTACIONAMIENTOS</h4>
                <hr>
                <!-- Quotation -->
                <p style="color:blue;">¡Bienvenido {{ Auth::user()->name }}!</p>
                <p>{!! Auth::user()->rol() !!}</p>
            </div>

        </div>
        <!-- Card -->
        <!-- Jumbotron -->
    </div>
@endsection()
