<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Número de Ticket</th>
            <th>Total</th>
            <th>Fecha de solicitud</th>
            <th>Razón social</th>
            <th>Datos</th>
            <th>Imagen</th>
            <th>Estado</th>
            <th colspan="2">Motivo</th>
        </tr>
        <tr class="warning no-result">
            <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>
        <tbody>
        @foreach($tickets as $ticket)
            <tr>
                <td>{!! $ticket->no_ticket !!}</td>
                <td>{!! $ticket->total_ticket !!}</td>
                <td>{!! $ticket->fecha_emision !!}</td>
                <td>{!! $ticket->Razon_social !!}</td>
                <td>
                    <a type="button" class="btn-floating btn-sm default-color" data-toggle="modal"
                       data-target="#ModalUser{{$ticket->id}}"><i class="fas fa-edit"></i></a>
                    <!-- Modal -->
                    <div class="modal fade" id="ModalUser{{$ticket->id}}" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Usuario</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open() !!}
                                    <div class="md-form">
                                        {!! Form::label('RFC', 'RFC:') !!}
                                        {!! Form::text('RFC', $ticket->RFC, ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                    <div class="md-form">
                                        {!! Form::label('RS', 'Razón Social:') !!}
                                        {!! Form::text('RS', $ticket->Razon_social, ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                    <div class="md-form">
                                        {!! Form::label('Email', 'Email:') !!}
                                        {!! Form::text('Email', $ticket->email, ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                    <div class="md-form">
                                        {!! Form::label('est', 'Estacionamiento:') !!}
                                        {!! Form::text('est', $ticket->park, ['class' => 'form-control', 'disabled']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td><a href="{!! route('img', [$ticket->id]) !!}"
                       class='btn-floating btn-sm btn-blue-grey'><i class="fas fa-download"></i></a></td>
                <td>
                    <a type="button" class="btn-floating btn-sm danger-color material-tooltip-main"
                       data-toggle="tooltip"
                       data-placement="bottom" title="Ticket rechazado"><i class="far fa-times-circle"></i></a>
                </td>
                <td>
                    {!! $ticket->coment !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$tickets->links()}}
</div>

<br>
<br>

<script>
    @push('scripts')
    $(document).ready(function () {
        $('.mdb-select').materialSelect();
    });
    @endpush
</script>
