<!-- Id Cliente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_cliente', 'Id Cliente:') !!}
    {!! Form::number('id_cliente', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Est Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_est', 'Id Est:') !!}
    {!! Form::number('id_est', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Tipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_tipo', 'Id Tipo:') !!}
    {!! Form::number('id_tipo', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Centraluser Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_CentralUser', 'Id Centraluser:') !!}
    {!! Form::number('id_CentralUser', null, ['class' => 'form-control']) !!}
</div>

<!-- Factura Field -->
<div class="form-group col-sm-6">
    {!! Form::label('factura', 'Factura:') !!}
    {!! Form::number('factura', null, ['class' => 'form-control']) !!}
</div>

<!-- No Ticket Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_ticket', 'No Ticket:') !!}
    {!! Form::text('no_ticket', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Ticket Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_ticket', 'Total Ticket:') !!}
    {!! Form::number('total_ticket', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Emision Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_emision', 'Fecha Emision:') !!}
    {!! Form::date('fecha_emision', null, ['class' => 'form-control','id'=>'fecha_emision']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha_emision').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Imagen Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('imagen', 'Imagen:') !!}
    {!! Form::textarea('imagen', null, ['class' => 'form-control']) !!}
</div>

<!-- Estatus Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estatus', 'Estatus:') !!}
    {!! Form::text('estatus', null, ['class' => 'form-control']) !!}
</div>

<!-- Usocfdi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('UsoCFDI', 'Usocfdi:') !!}
    {!! Form::text('UsoCFDI', null, ['class' => 'form-control']) !!}
</div>

<!-- Metodo Pago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('metodo_pago', 'Metodo Pago:') !!}
    {!! Form::text('metodo_pago', null, ['class' => 'form-control']) !!}
</div>

<!-- Forma Pago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('forma_pago', 'Forma Pago:') !!}
    {!! Form::text('forma_pago', null, ['class' => 'form-control']) !!}
</div>

<!-- Rfc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('RFC', 'Rfc:') !!}
    {!! Form::text('RFC', null, ['class' => 'form-control']) !!}
</div>

<!-- Razon Social Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Razon_social', 'Razon Social:') !!}
    {!! Form::text('Razon_social', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Calle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('calle', 'Calle:') !!}
    {!! Form::text('calle', null, ['class' => 'form-control']) !!}
</div>

<!-- No Ext Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_ext', 'No Ext:') !!}
    {!! Form::text('no_ext', null, ['class' => 'form-control']) !!}
</div>

<!-- No Int Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_int', 'No Int:') !!}
    {!! Form::text('no_int', null, ['class' => 'form-control']) !!}
</div>

<!-- Colonia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('colonia', 'Colonia:') !!}
    {!! Form::text('colonia', null, ['class' => 'form-control']) !!}
</div>

<!-- Municipio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('municipio', 'Municipio:') !!}
    {!! Form::text('municipio', null, ['class' => 'form-control']) !!}
</div>

<!-- Estado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estado', 'Estado:') !!}
    {!! Form::text('estado', null, ['class' => 'form-control']) !!}
</div>

<!-- Cp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cp', 'Cp:') !!}
    {!! Form::text('cp', null, ['class' => 'form-control']) !!}
</div>

<!-- Coment Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('coment', 'Coment:') !!}
    {!! Form::textarea('coment', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tickets.index') !!}" class="btn btn-default">Cancel</a>
</div>
