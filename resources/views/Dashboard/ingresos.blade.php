@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h4 class="card-header-title">Diario de ingresos</h4>
            </div>
        </div>
        <hr>
    </section>

    <!--<ul class="nav md-pills nav-justified pills-rounded pills-default movimiento">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#dia" role="tab">Dia</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#mes" role="tab">Mes</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#fact" role="tab">Facturas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#est" role="tab">Estacionamientos</a>
        </li>
    </ul>-->

    <!-- Tab panels -->
    <div class="tab-content pt-0">

        <!--Dia-->
        <div class="tab-pane fade in show active" id="dia" role="tabpanel">
            <br>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating indigo"><i class="fas fa-ticket-alt"
                                                                                        aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col text-center">
                                        <p style="font-size: 16px;">Pendientes del dia: <span
                                                    class="badge badge-pill badge-info"
                                                    style="font-size: 16px;">30</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col text-center">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-check-circle"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col">
                                        <p style="font-size: 16px;">Ingresos:<p><span
                                                    class="badge badge-pill badge-secondary"
                                                    style="font-size: 16px;">$10,000</span></p></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-barcode"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col text-center">
                                        <p style="font-size: 16px;">Rechazados: <p><span
                                                    class="badge badge-pill badge-success"
                                                    style="font-size: 16px;">5</span></p></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>

                </div>
            </div>
            <br>
            <!--graficas-->
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card card-cascade narrower">
                            <!-- Card image -->
                            <div class="view view-cascade gradient-card-header aqua-gradient">
                                <!-- Title -->
                                <h4 class="card-header-title">Estacionamientos con más ingresos</h4>
                            </div>
                            <!-- Card content -->
                            <div class="card-body card-body-cascade text-center">
                                <!-- Text -->
                                <canvas id="horizontalBar"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card card-cascade narrower">
                            <!-- Card image -->
                            <div class="view view-cascade gradient-card-header aqua-gradient">
                                <!-- Title -->
                                <h4 class="card-header-title">Estacionamientos con menor ingreso</h4>
                            </div>
                            <!-- Card content -->
                            <div class="card-body card-body-cascade text-center">
                                <!-- Text -->
                                <canvas id="horizontalBarMenor"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--graficas-->
            <br>
            <br>

        </div>
        <!--/.Dia-->

        <br>
        <!--/.Estacionamientos-->

    </div>
@endsection

<script>
    @push('scripts')
            ////Mas cantidad
    new Chart(document.getElementById("horizontalBar"), {
        "type": "horizontalBar",
        "data": {
            "labels": ["Red", "Orange", "Yellow", "Green", "Blue", "Purple", "Grey"],
            "datasets": [{
                "label": "My First Dataset",
                "data": [22, 33, 55, 12, 86, 23, 14],
                "fill": false,
                "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
                    "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
                    "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
                ],
                "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
                ],
                "borderWidth": 1
            }]
        },
        "options": {
            "scales": {
                "xAxes": [{
                    "ticks": {
                        "beginAtZero": true
                    }
                }]
            }
        }
    });

    ////Menor cantidad
    new Chart(document.getElementById("horizontalBarMenor"), {
        "type": "horizontalBar",
        "data": {
            "labels": ["Red", "Orange", "Yellow", "Green", "Blue", "Purple", "Grey"],
            "datasets": [{
                "label": "My First Dataset",
                "data": [22, 33, 55, 12, 86, 23, 14],
                "fill": false,
                "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
                    "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
                    "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
                ],
                "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
                ],
                "borderWidth": 1
            }]
        },
        "options": {
            "scales": {
                "xAxes": [{
                    "ticks": {
                        "beginAtZero": true
                    }
                }]
            }
        }
    });
    @endpush
</script>
