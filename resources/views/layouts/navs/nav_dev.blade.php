<nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar black">
    <div class="container smooth-scroll">
        <a class="navbar-brand " href="{{route('home')}}"><img src="{{asset('logo/logo/login-logo.png')}}"
                                                               style="width: 40px;"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#centralModalSm"><span
                            class="btn btn-default btn-rounded btn-sm">Menú</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><span class="btn btn-default btn-rounded btn-sm">
                    <i class="fas fa-user" aria-hidden="true"></i>{{ Auth::user()->name }}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" type="#logout-form" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <span class="btn btn-default btn-rounded btn-sm">
                    <i class="fas fa-power-off" aria-hidden="true"></i>Cerrar sesión</span></a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </ul>
        </div>
    </div>
</nav>


<!-- Central Modal Small -->
<div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <!-- Change class .modal-sm to change the size of the modal -->
    <div class="modal-dialog modal-lg" role="document">


        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100" id="myModalLabel">Seleccione una opción</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="nav md-pills nav-justified pills-success">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="true"
                           aria-expanded="false">Pensiones</a>
                        <div class="dropdown-menu dropdown-success">
                            <a class="dropdown-item" href="{{url('pensions')}}">Pensiones</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{url('pagos')}}">Pagos</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{url('operacionDiarias')}}">Operaciones Diarias</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('facturaPensions.index') }}">Facturas</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('clientes.index') }}">Clientes</a>
                        </div>
                    </li>
                    @if(Auth::user()->monitorista())
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">Informe</a>
                            <div class="dropdown-menu dropdown-success">
                                <a class="dropdown-item" href="{{url('dashboard')}}">Facturación</a>
                            <!--<div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{url('diarioingreso')}}">Diario ingresos</a>-->
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">Facturas</a>
                            <div class="dropdown-menu dropdown-success">
                                <a class="dropdown-item" href="{{ url('factura') }}">Facturas</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('pi')}}">Facturas PI</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">Tickets</a>
                            <div class="dropdown-menu dropdown-success">
                                <a class="dropdown-item" href="{{url('tickets')}}">Tickets</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{!! route('list_rechazo') !!}">Rechazos</a>
                            </div>
                        </li>
                    <!--<li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="true"
                           aria-expanded="false">Ingresos</a>
                        <div class="dropdown-menu dropdown-success">
                            <a class="dropdown-item" href="{{route('ingresos.index')}}">Fichas</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{route('calendar')}}">Calendario</a>
                        </div>
                    </li>-->
                    @endif
                    @if(Auth::user()->admin())
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">Usuarios</a>
                            <div class="dropdown-menu dropdown-success">
                                <a class="dropdown-item" href="{{url('central_users')}}">Usuarios</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{url('gerentes')}}">Gerentes</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">Estacionamiento</a>
                            <div class="dropdown-menu dropdown-success">
                                <a class="dropdown-item" href="{{url('estacionamientos')}}">Estacionamientos</a>
                                <!--<div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Pensiones</a>-->
                            </div>
                        </li>
                        <!--<li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">Organizaciones</a>
                            <div class="dropdown-menu dropdown-success">
                                <a class="dropdown-item" href="#">Organización</a>
                            </div>
                        </li>-->
                    @endif
                    @if(Auth::user()->firmas() == 1)
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">Firmas</a>
                            <div class="dropdown-menu dropdown-success">
                                <a class="dropdown-item" href="{{url('firmas')}}">Firmas</a>
                            </div>
                        </li>
                    @endif
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- Central Modal Small -->


