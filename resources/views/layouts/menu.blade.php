<li class="{{ Request::is('clientes*') ? 'active' : '' }}">
    <a href="{!! route('clientes.index') !!}"><i class="fa fa-edit"></i><span>Clientes</span></a>
</li>

<li class="{{ Request::is('infoClientes*') ? 'active' : '' }}">
    <a href="{!! route('infoClientes.index') !!}"><i class="fa fa-edit"></i><span>Info Clientes</span></a>
</li>

<li class="{{ Request::is('centralUsers*') ? 'active' : '' }}">
    <a href="{!! route('centralUsers.index') !!}"><i class="fa fa-edit"></i><span>Central Users</span></a>
</li>

<li class="{{ Request::is('logs*') ? 'active' : '' }}">
    <a href="{!! route('logs.index') !!}"><i class="fa fa-edit"></i><span>Logs</span></a>
</li>

<li class="{{ Request::is('permisos*') ? 'active' : '' }}">
    <a href="{!! route('permisos.index') !!}"><i class="fa fa-edit"></i><span>Permisos</span></a>
</li>

<li class="{{ Request::is('roles*') ? 'active' : '' }}">
    <a href="{!! route('roles.index') !!}"><i class="fa fa-edit"></i><span>Roles</span></a>
</li>

<li class="{{ Request::is('marcas*') ? 'active' : '' }}">
    <a href="{!! route('marcas.index') !!}"><i class="fa fa-edit"></i><span>Marcas</span></a>
</li>

<li class="{{ Request::is('orgs*') ? 'active' : '' }}">
    <a href="{!! route('orgs.index') !!}"><i class="fa fa-edit"></i><span>Orgs</span></a>
</li>

<li class="{{ Request::is('estacionamientos*') ? 'active' : '' }}">
    <a href="{!! route('estacionamientos.index') !!}"><i class="fa fa-edit"></i><span>Estacionamientos</span></a>
</li>

<li class="{{ Request::is('tipoTickets*') ? 'active' : '' }}">
    <a href="{!! route('tipoTickets.index') !!}"><i class="fa fa-edit"></i><span>Tipo Tickets</span></a>
</li>

<li class="{{ Request::is('tickets*') ? 'active' : '' }}">
    <a href="{!! route('tickets.index') !!}"><i class="fa fa-edit"></i><span>Tickets</span></a>
</li>

<li class="{{ Request::is('facturas*') ? 'active' : '' }}">
    <a href="{!! route('facturas.index') !!}"><i class="fa fa-edit"></i><span>Facturas</span></a>
</li>

<li class="{{ Request::is('tipoPagos*') ? 'active' : '' }}">
    <a href="{!! route('tipoPagos.index') !!}"><i class="fa fa-edit"></i><span>Tipo Pagos</span></a>
</li>

<li class="{{ Request::is('tipoPensions*') ? 'active' : '' }}">
    <a href="{!! route('tipoPensions.index') !!}"><i class="fa fa-edit"></i><span>Tipo Pensions</span></a>
</li>

<li class="{{ Request::is('formaPagos*') ? 'active' : '' }}">
    <a href="{!! route('formaPagos.index') !!}"><i class="fa fa-edit"></i><span>Forma Pagos</span></a>
</li>

<li class="{{ Request::is('pensions*') ? 'active' : '' }}">
    <a href="{!! route('pensions.index') !!}"><i class="fa fa-edit"></i><span>Pensions</span></a>
</li>

<li class="{{ Request::is('ingresos*') ? 'active' : '' }}">
    <a href="{!! route('ingresos.index') !!}"><i class="fa fa-edit"></i><span>Ingresos</span></a>
</li>

<li class="{{ Request::is('imgIngresos*') ? 'active' : '' }}">
    <a href="{!! route('imgIngresos.index') !!}"><i class="fa fa-edit"></i><span>Img Ingresos</span></a>
</li>

<li class="{{ Request::is('ingresos*') ? 'active' : '' }}">
    <a href="{!! route('ingresos.index') !!}"><i class="fa fa-edit"></i><span>Ingresos</span></a>
</li>

<li class="{{ Request::is('bancoCollects*') ? 'active' : '' }}">
    <a href="{!! route('bancoCollects.index') !!}"><i class="fa fa-edit"></i><span>Banco Collects</span></a>
</li>

<li class="{{ Request::is('empresaCollects*') ? 'active' : '' }}">
    <a href="{!! route('empresaCollects.index') !!}"><i class="fa fa-edit"></i><span>Empresa Collects</span></a>
</li>

<li class="{{ Request::is('estacionamientos*') ? 'active' : '' }}">
    <a href="{!! route('estacionamientos.index') !!}"><i class="fa fa-edit"></i><span>Estacionamientos</span></a>
</li>

<li class="{{ Request::is('gerentes*') ? 'active' : '' }}">
    <a href="{!! route('gerentes.index') !!}"><i class="fa fa-edit"></i><span>Gerentes</span></a>
</li>

<li class="{{ Request::is('firmas*') ? 'active' : '' }}">
    <a href="{!! route('firmas.index') !!}"><i class="fa fa-edit"></i><span>Firmas</span></a>
</li>

<li class="{{ Request::is('pagos*') ? 'active' : '' }}">
    <a href="{{ route('pagos.index') }}"><i class="fa fa-edit"></i><span>Pagos</span></a>
</li>

<li class="{{ Request::is('operacionDiarias*') ? 'active' : '' }}">
    <a href="{{ route('operacionDiarias.index') }}"><i class="fa fa-edit"></i><span>Operacion Diarias</span></a>
</li>

<li class="{{ Request::is('facturaPensions*') ? 'active' : '' }}">
    <a href="{{ route('facturaPensions.index') }}"><i class="fa fa-edit"></i><span>Factura Pensions</span></a>
</li>

