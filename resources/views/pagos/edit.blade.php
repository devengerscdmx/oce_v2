@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Editar pago</h3>
            </div>
        </div>
        <hr>
        @include('flash::message')
        {!! Form::model($pago, ['route' => ['pagos.update', $pago->id], 'method' => 'patch']) !!}

        @include('pagos.fields')

        {!! Form::close() !!}
    </section>
@endsection
