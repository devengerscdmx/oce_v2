<!-- Total Field -->
<div class="form-group">
    {!! Form::label('total', 'Total:') !!}
    <p>{{ $pago->total }}</p>
</div>

<!-- Subtotal Field -->
<div class="form-group">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    <p>{{ $pago->subtotal }}</p>
</div>

<!-- Iva Field -->
<div class="form-group">
    {!! Form::label('iva', 'Iva:') !!}
    <p>{{ $pago->iva }}</p>
</div>

<!-- Tipopago Field -->
<div class="form-group">
    {!! Form::label('tipoPago', 'Tipopago:') !!}
    <p>{{ $pago->tipoPago }}</p>
</div>

<!-- Idpension Field -->
<div class="form-group">
    {!! Form::label('idPension', 'Idpension:') !!}
    <p>{{ $pago->idPension }}</p>
</div>

<!-- Fechapago Field -->
<div class="form-group">
    {!! Form::label('fechaPago', 'Fechapago:') !!}
    <p>{{ $pago->fechaPago }}</p>
</div>

