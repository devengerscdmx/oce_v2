<!-- Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total', 'Total:') !!}
    {!! Form::number('total', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtotal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    {!! Form::number('subtotal', null, ['class' => 'form-control']) !!}
</div>

<!-- Iva Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iva', 'Iva:') !!}
    {!! Form::number('iva', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipopago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipoPago', 'Tipopago:') !!}
    {!! Form::text('tipoPago', null, ['class' => 'form-control','maxlength' => 25,'maxlength' => 25]) !!}
</div>

<!-- Idpension Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idPension', 'Idpension:') !!}
    {!! Form::number('idPension', null, ['class' => 'form-control']) !!}
</div>

<!-- Fechapago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fechaPago', 'Fechapago:') !!}
    {!! Form::date('fechaPago', null, ['class' => 'form-control','id'=>'fechaPago']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fechaPago').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('pagos.index') }}" class="btn btn-default">Cancelar</a>
</div>
