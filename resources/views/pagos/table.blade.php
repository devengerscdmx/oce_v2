<div class="table-responsive">
    <table class="table" id="pagos-table">
        <thead>
        <tr>
            <th>Total</th>
            <th>Subtotal</th>
            <th>Iva</th>
            <th>Tipopago</th>
            <th>Idpension</th>
            <th>Fechapago</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pagos as $pago)
            <tr>
                <td>{{ $pago->total }}</td>
                <td>{{ $pago->subtotal }}</td>
                <td>{{ $pago->iva }}</td>
                <td>{{ $pago->tipoPago }}</td>
                <td>{{ $pago->idPension }}</td>
                <td>{{ $pago->fechaPago }}</td>
                <td>
                    {!! Form::open(['route' => ['pagos.destroy', $pago->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('pagos.edit', [$pago->id]) }}" class='btn btn-default btn-xs'>Editar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
