<!-- Rfc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('RFC', 'Rfc:') !!}
    {!! Form::text('RFC', null, ['class' => 'form-control']) !!}
</div>

<!-- Razon Social Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Razon_social', 'Razon Social:') !!}
    {!! Form::text('Razon_social', null, ['class' => 'form-control']) !!}
</div>

<!-- Regimen Fiscal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Regimen_fiscal', 'Regimen Fiscal:') !!}
    {!! Form::text('Regimen_fiscal', null, ['class' => 'form-control']) !!}
</div>

<!-- Url Dev Field -->
<div class="form-group col-sm-6">
    {!! Form::label('URL_dev', 'Url Dev:') !!}
    {!! Form::text('URL_dev', null, ['class' => 'form-control']) !!}
</div>

<!-- Url Prod Field -->
<div class="form-group col-sm-6">
    {!! Form::label('URL_prod', 'Url Prod:') !!}
    {!! Form::text('URL_prod', null, ['class' => 'form-control']) !!}
</div>

<!-- Token Dev Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('token_dev', 'Token Dev:') !!}
    {!! Form::textarea('token_dev', null, ['class' => 'form-control']) !!}
</div>

<!-- Token Prod Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('token_prod', 'Token Prod:') !!}
    {!! Form::textarea('token_prod', null, ['class' => 'form-control']) !!}
</div>

<!-- Prod Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Prod', 'Prod:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('Prod', 0) !!}
        {!! Form::checkbox('Prod', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('orgs.index') !!}" class="btn btn-default">Cancel</a>
</div>
