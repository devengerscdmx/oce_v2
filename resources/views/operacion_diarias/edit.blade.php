@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Operacion Diaria
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($operacionDiaria, ['route' => ['operacionDiarias.update', $operacionDiaria->id], 'method' => 'patch']) !!}

                        @include('operacion_diarias.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection