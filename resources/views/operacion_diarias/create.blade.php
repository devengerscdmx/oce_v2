@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Añadir operación</h3>
            </div>
        </div>
        <hr>
        @include('flash::message')
        {!! Form::open(['route' => 'operacionDiarias.store']) !!}

        @include('operacion_diarias.fields')

        {!! Form::close() !!}
    </section>
@endsection
