<div class="table-responsive">
    <table class="table" id="operacionDiarias-table">
        <thead>
        <tr>
            <th>Tarjeta Fisica</th>
            <th>Transaccion</th>
            <th>Tipo Operacion</th>
            <th>Monto</th>
            <th>Estancia</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($operacionDiarias as $operacionDiaria)
            <tr>
                <td>{{ $operacionDiaria->tarjeta_fisica }}</td>
                <td>{{ $operacionDiaria->transaccion }}</td>
                <td>{{ $operacionDiaria->tipo_operacion }}</td>
                <td>{{ $operacionDiaria->monto }}</td>
                <td>{{ $operacionDiaria->estancia }}</td>
                <td>
                    {!! Form::open(['route' => ['operacionDiarias.destroy', $operacionDiaria->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('operacionDiarias.edit', [$operacionDiaria->id]) }}"
                           class='btn btn-default btn-xs'>Editar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
