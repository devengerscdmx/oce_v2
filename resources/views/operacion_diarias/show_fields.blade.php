<!-- Tarjeta Fisica Field -->
<div class="form-group">
    {!! Form::label('tarjeta_fisica', 'Tarjeta Fisica:') !!}
    <p>{{ $operacionDiaria->tarjeta_fisica }}</p>
</div>

<!-- Transaccion Field -->
<div class="form-group">
    {!! Form::label('transaccion', 'Transaccion:') !!}
    <p>{{ $operacionDiaria->transaccion }}</p>
</div>

<!-- Tipo Operacion Field -->
<div class="form-group">
    {!! Form::label('tipo_operacion', 'Tipo Operacion:') !!}
    <p>{{ $operacionDiaria->tipo_operacion }}</p>
</div>

<!-- Monto Field -->
<div class="form-group">
    {!! Form::label('monto', 'Monto:') !!}
    <p>{{ $operacionDiaria->monto }}</p>
</div>

<!-- Estancia Field -->
<div class="form-group">
    {!! Form::label('estancia', 'Estancia:') !!}
    <p>{{ $operacionDiaria->estancia }}</p>
</div>

