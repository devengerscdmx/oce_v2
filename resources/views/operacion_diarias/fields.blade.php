<!-- Tarjeta Fisica Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tarjeta_fisica', 'Tarjeta Fisica:') !!}
    {!! Form::number('tarjeta_fisica', null, ['class' => 'form-control']) !!}
</div>

<!-- Transaccion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('transaccion', 'Transaccion:') !!}
    {!! Form::number('transaccion', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Operacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_operacion', 'Tipo Operacion:') !!}
    {!! Form::text('tipo_operacion', null, ['class' => 'form-control','maxlength' => 45,'maxlength' => 45]) !!}
</div>

<!-- Monto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto', 'Monto:') !!}
    {!! Form::number('monto', null, ['class' => 'form-control']) !!}
</div>

<!-- Estancia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estancia', 'Estancia:') !!}
    {!! Form::text('estancia', null, ['class' => 'form-control','maxlength' => 12,'maxlength' => 12]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('operacionDiarias.index') }}" class="btn btn-default">Cancelar</a>
</div>
