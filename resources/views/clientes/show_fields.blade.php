<!-- Apellido Paterno Field -->
<div class="form-group">
    {!! Form::label('apellido_paterno', 'Apellido Paterno:') !!}
    <p>{{ $cliente->apellido_paterno }}</p>
</div>

<!-- Apellido Materno Field -->
<div class="form-group">
    {!! Form::label('apellido_materno', 'Apellido Materno:') !!}
    <p>{{ $cliente->apellido_materno }}</p>
</div>

<!-- Nombres Field -->
<div class="form-group">
    {!! Form::label('nombres', 'Nombres:') !!}
    <p>{{ $cliente->nombres }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $cliente->email }}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{{ $cliente->estatus }}</p>
</div>

<!-- Rfc Field -->
<div class="form-group">
    {!! Form::label('RFC', 'Rfc:') !!}
    <p>{{ $cliente->RFC }}</p>
</div>

