@extends('layouts.app')
@section('title', 'Facturas')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Clientes</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-8">
                <button type="button" class="btn btn-default btn-lg">
                    Clientes encontrados: <span class="counter badge badge-danger ml-4"></span>
                </button>
            </div>
            <div class="col-4 text-right">
                <a type="button" class="btn btn-dark btn-rounded" href="{{ route('clientes.create') }}">
                    Añadir nuevo
                </a>
            </div>
        </div>
        <br>
        @include('clientes.table')
    </section>
@endsection

