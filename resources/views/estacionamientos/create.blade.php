@extends('layouts.app')
@section('title','Estacioneamientos')
@section('content')
    <style>#map_canvas {
            display: none
        }</style>
    <style>#guardar {
            display: none
        }</style>
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Agregar Estacionamiento</h3>
    </div>
    </div>
    <hr>

    <div class="col-12">
        @include('layouts.errors')
        <div class="row justify-content-md-center">
            <div class="col-md-6">
                {!! Form::open(['route' => 'estacionamientos.store' ]) !!}
                @include('estacionamientos.fields')
                {!! Form::close() !!}
            </div>
            <div class="col-md-6">
                <!-- Card -->
                <div class="card map-card">
                    <!--Google map-->
                    <div id="map_canvas" class="z-depth-1-half map-container" style="height: 500px"></div>
                    <!-- Card content -->
                    <div class="card-body closed px-0">
                        <div class="button px-2 mt-3">
                            <a class="btn-floating btn-lg btn-default float-right"><i class="fas fa-parking"></i></a>
                        </div>
                        <div class="white px-4 pb-4 pt-3-5">
                            <!-- Title -->
                            <h5 class="card-title h5 living-coral-text" id="proyecto">Estacionamiento</h5>
                            <hr>
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th scope="row" class="px-0 pb-3 pt-2">
                                        <i class="fas fa-map-marker-alt living-coral-text"></i>
                                    </th>
                                    <td class="pb-3 pt-2"><h5>Dirección:</h5>
                                        <span id="calle">....</span>
                                        <span id="num_ext"></span>
                                        <p id="colonia_id"></p>
                                        <span id="codigop"></span>
                                        <span id="pais_id"></span>
                                        <span>,</span>
                                        <span id="estado_id"></span>
                                    </td>

                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- Card -->
            </div>
        </div>
    </div>

@endsection

@push('maps')
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('MAPS_KEY','AIzaSyBoN6cE0y9vrkzluAmHhRZVZXTACDqT2CI')}}"></script>
@endpush

<script>

    @push('scripts')
    //Variables
    var map = null;
    var infoWindow = null;
    var resultados_lat = "";
    var resultados_long = "";
    var direc = "";
    var mensajeError = "";

    $(document).ready(function () {
        //input de nombre
        $("#nombre").keyup(function () {
            var value = $(this).val();
            $("#proyecto").text(value);
        });

        //input de direccion
        $("#calles").keyup(function () {
            var value = $(this).val();
            $("#calle").text(value);
        });

        //input de numero exterior
        $("#numeroexterior").keyup(function () {
            var value = $(this).val();
            $("#num_ext").text(value);
        });

        //input de colonia
        $("#colonia").keyup(function () {
            var value = $(this).val();
            $("#colonia_id").text(value);
        });

        //input de codigo postal
        $("#codigo").keyup(function () {
            var value = $(this).val();
            $("#codigop").text(value);
        });

        //input de pais
        $("#pais").keyup(function () {
            var value = $(this).val();
            $("#pais_id").text(value);
        });

        //input de estado
        $("#estado").keyup(function () {
            var value = $(this).val();
            $("#estado_id").text(value);
        });

    });

    //Script para Validar datos y Mostrar mapa
    $('#boton01').click(function () {
        if ($('#nombre').val() === '' || $('#calles').val() === '' || $('#numeroexterior').val() === '' || $('#colonia').val() === '' || $('#codigo').val() === '' || $('#estado').val() === '' || $('#pais').val() === '') {
            alert('Hay campos sin completar');
            $('#map_canvas').hidden();
        } else {
            direc = $("#calles").val() + " " + $("#numeroexterior").val() + ", " + $("#colonia").val() + ", " + $("#codigo").val() + ", " + $("#estado").val() + " " + $("#pais").val() + " ";
            $("#direccion").val(direc);
            $('#map_canvas').show();
            initialize();
        }
    });

    //Codigo Para generar Mapa

    //Se hace peticion de mapa
    function initialize() {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': direc}, function (results, status) {
            if (status === 'OK') {
                var resultados = results[0].geometry.location,
                    resultados_lat = resultados.lat(),
                    resultados_long = resultados.lng();
                //alert("Latitud: " +resultados_lat + " longitud: " + resultados_long);
                $("#latitud").val(resultados_lat);
                $("#longitud").val(resultados_long);
                positionMap(resultados_lat, resultados_long);
                //Se muestra el boton de guardar
                $('#guardar').show();
            } else {
                if (status === "ZERO_RESULTS") {
                    mensajeError = "No hubo resultados para la dirección ingresada.";
                } else if (status === "OVER_QUERY_LIMIT" || status === "REQUEST_DENIED" || status === "UNKNOWN_ERROR") {
                    mensajeError = "Error general del mapa.";
                } else if (status === "INVALID_REQUEST") {
                    mensajeError = "Error de la web. Contacte con Name Agency.";
                }
                alert(mensajeError);
            }
        });
    }

    function positionMap(latd, long) {
        var myLatlng = {lat: latd, lng: long};

        var image = {

            url: '{{asset('logo/logo/marker-central-2.png')}}',
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(25, 25),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(14, 30)
        };


        var myOptions = {
            zoom: 13,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

        infoWindow = new google.maps.InfoWindow();

        var marker = new google.maps.Marker({
            position: myLatlng,
            draggable: true,
            icon: image,
            map: map,
            title: 'Ejemplo marcador arrastrable'
        });
        google.maps.event.addListener(marker, 'drag', function () {
            openInfoWindow2(marker);
        });
        google.maps.event.addListener(marker, 'dragend', function () {
            openInfoWindow(marker);
        });
    }

    function openInfoWindow(marker) {
        var markerLatLng = marker.getPosition();
        infoWindow.setContent([
            'La posicion del marcador es: ',
            markerLatLng.lat(),
            ', ',
            markerLatLng.lng(),
            ' Arrastrame y haz click para actualizar la posición.'
        ].join(''));
        infoWindow.open(map, marker);
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                console.log(results);
                var address = results[0]['formatted_address'];
                $("#direccion").val(address);
                var componente = results[0]['address_components'];
                console.log(componente);
                $('#colonia').val(componente[2]['long_name']);
                $('#colonia_id').text(componente[2]['long_name']);
                $('#calles').val(componente[1]['long_name']);
                $("#calle").text(componente[1]['long_name']);
                $('#numeroexterior').val(componente[0]['long_name']);
                $('#num_ext').text(componente[0]['long_name']);
                $('#codigo').val(componente[6]['long_name']);
                $('#codigop').text(componente[6]['long_name']);
            }
        });
    }

    function openInfoWindow2(marker) {
        var markerLatLng = marker.getPosition();
        $("#latitud").val(markerLatLng.lat());
        $("#longitud").val(markerLatLng.lng());
    }

    @endpush

</script>
