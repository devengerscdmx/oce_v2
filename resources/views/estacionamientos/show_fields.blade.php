<!-- No Est Field -->
<div class="form-group">
    {!! Form::label('no_est', 'No Est:') !!}
    <p>{!! $estacionamiento->no_est !!}</p>
</div>

<!-- Id Marca Field -->
<div class="form-group">
    {!! Form::label('id_marca', 'Id Marca:') !!}
    <p>{!! $estacionamiento->id_marca !!}</p>
</div>

<!-- Id Org Field -->
<div class="form-group">
    {!! Form::label('id_org', 'Id Org:') !!}
    <p>{!! $estacionamiento->id_org !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $estacionamiento->nombre !!}</p>
</div>

<!-- Dist Regio Field -->
<div class="form-group">
    {!! Form::label('dist_regio', 'Dist Regio:') !!}
    <p>{!! $estacionamiento->dist_regio !!}</p>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{!! $estacionamiento->direccion !!}</p>
</div>

<!-- Calle Field -->
<div class="form-group">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{!! $estacionamiento->calle !!}</p>
</div>

<!-- No Ext Field -->
<div class="form-group">
    {!! Form::label('no_ext', 'No Ext:') !!}
    <p>{!! $estacionamiento->no_ext !!}</p>
</div>

<!-- Colonia Field -->
<div class="form-group">
    {!! Form::label('colonia', 'Colonia:') !!}
    <p>{!! $estacionamiento->colonia !!}</p>
</div>

<!-- Municipio Field -->
<div class="form-group">
    {!! Form::label('municipio', 'Municipio:') !!}
    <p>{!! $estacionamiento->municipio !!}</p>
</div>

<!-- Estado Field -->
<div class="form-group">
    {!! Form::label('estado', 'Estado:') !!}
    <p>{!! $estacionamiento->estado !!}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{!! $estacionamiento->cp !!}</p>
</div>

<!-- Latitud Field -->
<div class="form-group">
    {!! Form::label('latitud', 'Latitud:') !!}
    <p>{!! $estacionamiento->latitud !!}</p>
</div>

<!-- Longitud Field -->
<div class="form-group">
    {!! Form::label('longitud', 'Longitud:') !!}
    <p>{!! $estacionamiento->longitud !!}</p>
</div>

<!-- Facturable Field -->
<div class="form-group">
    {!! Form::label('Facturable', 'Facturable:') !!}
    <p>{!! $estacionamiento->Facturable !!}</p>
</div>

<!-- Automatico Field -->
<div class="form-group">
    {!! Form::label('Automatico', 'Automatico:') !!}
    <p>{!! $estacionamiento->Automatico !!}</p>
</div>

<!-- Cajones Field -->
<div class="form-group">
    {!! Form::label('cajones', 'Cajones:') !!}
    <p>{!! $estacionamiento->cajones !!}</p>
</div>

<!-- Folio Field -->
<div class="form-group">
    {!! Form::label('folio', 'Folio:') !!}
    <p>{!! $estacionamiento->folio !!}</p>
</div>

<!-- Serie Field -->
<div class="form-group">
    {!! Form::label('serie', 'Serie:') !!}
    <p>{!! $estacionamiento->serie !!}</p>
</div>

<!-- Correo Field -->
<div class="form-group">
    {!! Form::label('correo', 'Correo:') !!}
    <p>{!! $estacionamiento->correo !!}</p>
</div>

<!-- Pensiones Field -->
<div class="form-group">
    {!! Form::label('pensiones', 'Pensiones:') !!}
    <p>{!! $estacionamiento->pensiones !!}</p>
</div>

<!-- Cortesias Field -->
<div class="form-group">
    {!! Form::label('cortesias', 'Cortesias:') !!}
    <p>{!! $estacionamiento->cortesias !!}</p>
</div>

<!-- Cobro Field -->
<div class="form-group">
    {!! Form::label('cobro', 'Cobro:') !!}
    <p>{!! $estacionamiento->cobro !!}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $estacionamiento->observaciones !!}</p>
</div>

<!-- Escuela Field -->
<div class="form-group">
    {!! Form::label('escuela', 'Escuela:') !!}
    <p>{!! $estacionamiento->escuela !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $estacionamiento->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $estacionamiento->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $estacionamiento->updated_at !!}</p>
</div>

