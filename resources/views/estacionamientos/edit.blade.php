@extends('layouts.app')
@section('title', 'Estacionamientos')
@section('content')

    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Estacionamiento: {!! $proyecto->nombre !!}</h3>
        </div>
    </div>
    @include('layouts.errors')
    <hr>
    <div class="row">
        <div class="col-md-2">
            <div class="md-form">
                <i class="fas fa-hashtag prefix"></i>
                {!! Form::label('no_est', 'Número:') !!}
                {!! Form::number('no_est', $proyecto->no_est, ['class' => 'form-control', 'disabled']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="md-form">
                <i class="fas fa-parking prefix"></i>
                {!! Form::label('nombre', 'Estacionamiento:') !!}
                {!! Form::text('nombre', $proyecto->nombre, ['class' => 'form-control', 'disabled']) !!}
            </div>
        </div>
        <div class="col-md-2">
            <div class="md-form">
                <i class="fas fa-book prefix"></i>
                {!! Form::label('serie', 'Serie:') !!}
                {!! Form::text('serie', $proyecto->serie, ['class' => 'form-control', 'disabled']) !!}
            </div>
        </div>
        <div class="col-md-2">
            <div class="md-form">
                <i class="fas fa-file-invoice-dollar prefix"></i>
                {!! Form::label('folio', 'Folio:') !!}
                {!! Form::number('folio', $proyecto->folio, ['class' => 'form-control', 'disabled']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="md-form">
                <i class="fab fa-black-tie prefix"></i>
                {!! Form::label('org', 'Organización:') !!}
                {!! Form::text('org', $proyecto->Razon_social, ['class' => 'form-control', 'disabled']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="md-form">
                <i class="fas fa-qrcode prefix"></i>
                {!! Form::label('RFC', 'RFC:') !!}
                {!! Form::text('RFC', $proyecto->RFC, ['class' => 'form-control', 'disabled']) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="md-form">
                <i class="fas fa-map-marked prefix"></i>
                {!! Form::label('distrito', 'Distrito:') !!}
                {!! Form::text('distrito', $proyecto->dist_regio, ['class' => 'form-control', 'disabled']) !!}
            </div>
        </div>
    </div>
    <hr>
    {!! Form::model($proyecto, ['route' => ['estacionamientos.update', $proyecto->no_est], 'method' => 'patch']) !!}
    <div class="row">
        <div class="col-md-6">
            <!-- Pais Field -->
            <div class="md-form">
                <i class="fas fa-city prefix"></i>
                {!! Form::label('pais', 'País:') !!}
                {!! Form::text('pais', $proyecto->pais, ['class' => 'form-control', 'id' => 'pais']) !!}
            </div>

            <!-- Edo Prov Field -->
            <div class="md-form">
                <i class="fas fa-map-marked-alt prefix"></i>
                {!! Form::label('edo_prov', 'Estado/Provincia:') !!}
                {!! Form::text('edo_prov', $proyecto->estado, ['class' => 'form-control', 'id' => 'estado']) !!}
            </div>

            <!-- Mun Cd Field -->
            <div class="md-form">
                <i class="fas fa-map-signs prefix"></i>
                {!! Form::label('mun_cd', 'Municipio:') !!}
                {!! Form::text('mun_cd', $proyecto->municipio, ['class' => 'form-control']) !!}
            </div>

            <!-- Colonia Field -->
            <div class="md-form">
                <i class="fas fa-map-pin prefix"></i>
                {!! Form::label('colonia', 'Colonia:') !!}
                {!! Form::text('colonia', $proyecto->colonia, ['class' => 'form-control', 'id' => 'colonia']) !!}
            </div>


            <!-- Calles Field -->
            <div class="md-form">
                <i class="fas fa-road prefix"></i>
                {!! Form::label('calles', 'Calle:') !!}
                {!! Form::text('calles', $proyecto->calle, ['class' => 'form-control', 'id' => 'calles']) !!}
            </div>

            <!-- Numeroext Field -->
            <div class="md-form">
                <i class="fas fa-map-marker prefix"></i>
                {!! Form::label('numeroExt', 'Número exterior:') !!}
                {!! Form::text('numeroExt', $proyecto->no_ext, ['class' => 'form-control', 'id' => 'numeroexterior']) !!}
            </div>

            <!-- Cp Field -->
            <div class="md-form">
                <i class="fas fa-map-marker-alt prefix"></i>
                {!! Form::label('cp', 'Código postal:') !!}
                {!! Form::text('cp', $proyecto->cp, ['class' => 'form-control', 'id' => 'codigo']) !!}
            </div>
            {!! Form::hidden('direccion', $proyecto->direccion, ['id' => 'direccion', 'name' => 'direccion']) !!}

            {{ Form::hidden('latitud', null , ['id' => 'latitud', 'name' => 'latitud']) }}

            {{ Form::hidden('longitud', null , ['id' => 'longitud', 'name' => 'longitud']) }}
        </div>
        <div class="col-md-6">
            <div class="card map-card">
                <div id="map_canvas" class="z-depth-1-half map-container" style="height: 450px"></div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div class="md-form ">
                <i class="fas fa-bookmark prefix"></i>
                {!! Form::label('marca', 'Marca:') !!}
                {!! Form::text('marca', $proyecto->marca, ['class' => 'form-control', 'disabled']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <select class="mdb-select md-form colorful-select dropdown-success" searchable="Buscar..." name="marca">
                <option value="" selected >¿Cambia la marca?</option>
                @forelse($marcas as $item)
                    <option value="{{$item->id}}">{{$item->marca}}</option>
                @empty
                    <option value="">Sin marcas</option>
                @endforelse
            </select>
        </div>
        <div class="col-md-6">
            <div class="md-form ">
                <i class="fas fa-school prefix"></i>
                {!! Form::label('escuela', '¿Es escuela?:') !!}
                @if($proyecto->escuela)
                    {!! Form::text('escuela', 'Si' , ['class' => 'form-control', 'disabled']) !!}
                @else
                    {!! Form::text('escuela', 'No' , ['class' => 'form-control', 'disabled']) !!}
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <select class="mdb-select md-form colorful-select dropdown-success" name="escuela">
                <option value="" selected >¿Es escuela?</option>
                <option value=1>Si</option>
                <option value=0>No</option>
            </select>
        </div>
        <div class="col-md-6">
            <div class="md-form ">
                <i class="fas fa-file-invoice-dollar prefix"></i>
                {!! Form::label('facturable', '¿Es facturable?:') !!}
                @if($proyecto->Facturable)
                    {!! Form::text('facturable', 'Si' , ['class' => 'form-control', 'disabled']) !!}
                @else
                    {!! Form::text('facturable', 'No' , ['class' => 'form-control', 'disabled']) !!}
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <select class="mdb-select md-form colorful-select dropdown-success" name="faturable">
                <option value="" selected >¿Es facturable?</option>
                <option value=1>Si</option>
                <option value=0>No</option>
            </select>
        </div>
        <div class="col-md-6">
            <div class="md-form ">
                <i class="fas fa-robot prefix"></i>
                {!! Form::label('auto', '¿Es automático?:') !!}
                @if($proyecto->Automatico)
                    {!! Form::text('auto', 'Si' , ['class' => 'form-control', 'disabled']) !!}
                @else
                    {!! Form::text('auto', 'No' , ['class' => 'form-control', 'disabled']) !!}
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <select class="mdb-select md-form colorful-select dropdown-success" name="automatico">
                <option value="" selected >¿Es automático?</option>
                <option value=1>Si</option>
                <option value=0>No</option>
            </select>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            {!! Form::submit('Actualiza', ['class' => 'btn btn-primary', 'id' => 'guardar', 'onclick' => "return confirm('¿Has confirmado que la dirección sea correcta?')"]) !!}
            <a href="{!! route('estacionamientos.index') !!}" class="btn btn-default">Atras</a>
            {!! Form::close() !!}
        </div>
    </div>
    <br>
    <br>
@endsection

@push('maps')
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('MAPS_KEY','AIzaSyBoN6cE0y9vrkzluAmHhRZVZXTACDqT2CI')}}"></script>
@endpush

<script>
    @push('scripts')
    //Variables
    var map = null;
    var infoWindow = null;
    var resultados_lat = "";
    var resultados_long = "";
    var direc = "";
    var mensajeError = "";

    $(document).ready(function () {
        $('.mdb-select').materialSelect();
        //input de nombre
        $("#nombre").keyup(function () {
            var value = $(this).val();
            $("#proyecto").text(value);
        });

        //input de direccion
        $("#calles").keyup(function () {
            var value = $(this).val();
            $("#calle").text(value);
        });

        //input de numero exterior
        $("#numeroexterior").keyup(function () {
            var value = $(this).val();
            $("#num_ext").text(value);
        });

        //input de colonia
        $("#colonia").keyup(function () {
            var value = $(this).val();
            $("#colonia_id").text(value);
        });

        //input de codigo postal
        $("#codigo").keyup(function () {
            var value = $(this).val();
            $("#codigop").text(value);
        });

        //input de pais
        $("#pais").keyup(function () {
            var value = $(this).val();
            $("#pais_id").text(value);
        });

        //input de estado
        $("#estado").keyup(function () {
            var value = $(this).val();
            $("#estado_id").text(value);
        });
        $('#map_canvas').show();
        direc = $("#direccion").val();
        initialize();

    });

    //Script para Validar datos y Mostrar mapa
    $('#boton01').click(function () {
        if ($('#nombre').val() === '' || $('#calles').val() === '' || $('#numeroexterior').val() === '' || $('#colonia').val() === '' || $('#codigo').val() === '' || $('#estado').val() === '' || $('#pais').val() === '') {
            alert('Hay campos sin completar');
        } else {
            direc = $("#calles").val() + " " + $("#numeroexterior").val() + ", " + $("#colonia").val() + ", " + $("#codigo").val() + ", " + $("#estado").val() + " " + $("#pais").val() + " ";
            $("#direccion").val(direc);
            initialize();
        }
    });

    //Codigo Para generar Mapa

    //Se hace peticion de mapa
    function initialize() {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': direc}, function (results, status) {
            if (status === 'OK') {
                var resultados = results[0].geometry.location,
                    resultados_lat = resultados.lat(),
                    resultados_long = resultados.lng();
                //alert("Latitud: " +resultados_lat + " longitud: " + resultados_long);
                $("#latitud").val(resultados_lat);
                $("#longitud").val(resultados_long);
                positionMap(resultados_lat, resultados_long);
                //Se muestra el boton de guardar
                //$('#guardar').show();
            } else {
                if (status === "ZERO_RESULTS") {
                    mensajeError = "No hubo resultados para la dirección ingresada.";
                } else if (status === "OVER_QUERY_LIMIT" || status === "REQUEST_DENIED" || status === "UNKNOWN_ERROR") {
                    mensajeError = "Error general del mapa.";
                } else if (status === "INVALID_REQUEST") {
                    mensajeError = "Error de la web. Contacte con Name Agency.";
                }
                alert(mensajeError);
            }
        });
    }

    function positionMap(latd, long) {
        var myLatlng = {lat: latd, lng: long};

        var image = {

            url: '{{asset('logo/logo/marker-central-2.png')}}',
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(25, 25),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(14, 30)
        };


        var myOptions = {
            zoom: 13,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

        infoWindow = new google.maps.InfoWindow();

        var marker = new google.maps.Marker({
            position: myLatlng,
            draggable: true,
            icon: image,
            map: map,
            title: 'Ejemplo marcador arrastrable'
        });
        google.maps.event.addListener(marker, 'drag', function () {
            openInfoWindow2(marker);
        });
        google.maps.event.addListener(marker, 'dragend', function () {
            openInfoWindow(marker);
        });
    }

    function openInfoWindow(marker) {
        var markerLatLng = marker.getPosition();
        infoWindow.setContent([
            'La posicion del marcador es: ',
            markerLatLng.lat(),
            ', ',
            markerLatLng.lng(),
            ' Arrastrame y haz click para actualizar la posición.'
        ].join(''));
        infoWindow.open(map, marker);
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                console.log(results);
                var address = results[0]['formatted_address'];
                $("#direccion").val(address);
                var componente = results[0]['address_components'];
                console.log(componente);
                $('#colonia').val(componente[2]['long_name']);
                $('#colonia_id').text(componente[2]['long_name']);
                $('#calles').val(componente[1]['long_name']);
                $("#calle").text(componente[1]['long_name']);
                $('#numeroexterior').val(componente[0]['long_name']);
                $('#num_ext').text(componente[0]['long_name']);
                $('#codigo').val(componente[6]['long_name']);
                $('#codigop').text(componente[6]['long_name']);
            }
        });
    }

    function openInfoWindow2(marker) {
        var markerLatLng = marker.getPosition();
        $("#latitud").val(markerLatLng.lat());
        $("#longitud").val(markerLatLng.lng());
    }

    @endpush
</script>
