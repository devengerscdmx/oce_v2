<!-- Nombre Field -->
<div class="md-form">
    <i class="far fa-address-card prefix"></i>
    {!! Form::label('nombre', 'Estacionamiento:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control', 'id' => 'nombre']) !!}
</div>

<div class="md-form">
    <i class="fas fa-list-ol prefix"></i>
    {!! Form::label('no_est', 'Número de Estacionamiento:') !!}
    {!! Form::number('no_est', null, ['class' => 'form-control']) !!}
</div>

<!-- Pais Field -->
<div class="md-form">
    <i class="fas fa-city prefix"></i>
    {!! Form::label('pais', 'País:') !!}
    {!! Form::text('pais', null, ['class' => 'form-control', 'id' => 'pais']) !!}
</div>

<!-- Edo Prov Field -->
<div class="md-form">
    <i class="fas fa-map-marked-alt prefix"></i>
    {!! Form::label('edo_prov', 'Estado/Provincia:') !!}
    {!! Form::text('edo_prov', null, ['class' => 'form-control', 'id' => 'estado']) !!}
</div>

<!-- Mun Cd Field -->
<div class="md-form">
    <i class="fas fa-map-signs prefix"></i>
    {!! Form::label('mun_cd', 'Municipio:') !!}
    {!! Form::text('mun_cd', null, ['class' => 'form-control']) !!}
</div>

<!-- Colonia Field -->
<div class="md-form">
    <i class="fas fa-map-pin prefix"></i>
    {!! Form::label('colonia', 'Colonia:') !!}
    {!! Form::text('colonia', null, ['class' => 'form-control', 'id' => 'colonia']) !!}
</div>


<!-- Calles Field -->
<div class="md-form">
    <i class="fas fa-road prefix"></i>
    {!! Form::label('calles', 'Calle:') !!}
    {!! Form::text('calles', null, ['class' => 'form-control', 'id' => 'calles']) !!}
</div>

<!-- Numeroext Field -->
<div class="md-form">
    <i class="fas fa-map-marker prefix"></i>
    {!! Form::label('numeroExt', 'Número exterior:') !!}
    {!! Form::text('numeroExt', null, ['class' => 'form-control', 'id' => 'numeroexterior']) !!}
</div>

<!-- Cp Field -->
<div class="md-form">
    <i class="fas fa-map-marker-alt prefix"></i>
    {!! Form::label('cp', 'Código postal:') !!}
    {!! Form::text('cp', null, ['class' => 'form-control', 'id' => 'codigo']) !!}
</div>

<!-- Cp Field -->
<div class="md-form">
    <i class="fas fa-map-marker-alt prefix"></i>
    {!! Form::label('distrito', 'Distrito:') !!}
    {!! Form::text('distrito', null, ['class' => 'form-control']) !!}
</div>

<!--Marcas select-->
<select class="mdb-select md-form colorful-select dropdown-success" searchable="Buscar..." name="marca">
    <option value="" selected disabled>Seleccione una Marca</option>
    @forelse($marcas as $item)
        <option value="{{$item->id}}">{{$item->marca}}</option>
    @empty
        <option value="">Sin marcas</option>
    @endforelse
</select>

<!--Blue select-->
<select class="mdb-select md-form colorful-select dropdown-success" name="escuela">
    <option value="" selected disabled>¿Es escuela?</option>
    <option value=1>Si</option>
    <option value=0>No</option>
</select>
<!--/Blue select-->

<!--Marcas select-->
<select class="mdb-select md-form colorful-select dropdown-success" searchable="Buscar..." name="organizacion">
    <option value="" selected disabled>Seleccione una organización</option>
    @forelse($org as $item)
        <option value="{{$item->id}}">{{$item->Razon_social}}</option>
    @empty
        <option value="">Sin organizaciones</option>
    @endforelse
</select>

<!--Blue select-->
<select class="mdb-select md-form colorful-select dropdown-success" name="faturable">
    <option value="" selected disabled>¿Es facturable?</option>
    <option value=1>Si</option>
    <option value=0>No</option>
</select>
<!--/Blue select-->

<!--Blue select-->
<select class="mdb-select md-form colorful-select dropdown-success" name="automatico">
    <option value="" selected disabled>¿Es automatico?</option>
    <option value=1>Si</option>
    <option value=0>No</option>
</select>
<!--/Blue select-->

{!! Form::hidden('direccion', null, ['id' => 'direccion', 'name' => 'direccion']) !!}

{{ Form::hidden('latitud', null , ['id' => 'latitud', 'name' => 'latitud']) }}

{{ Form::hidden('longitud', null , ['id' => 'longitud', 'name' => 'longitud']) }}


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary', 'id' => 'guardar', 'onclick' => "return confirm('¿Has confirmado que la dirección sea correcta?')"]) !!}
    <a href="{!! route('estacionamientos.index') !!}" class="btn btn-default">Cancelar</a>
    <input type="button" name="boton01" id="boton01" value="Previsualizar" class="btn btn-deep-purple">

</div>
<br>
<br>

@push('scripts')
    $(document).ready(function() {
    $('.mdb-select').materialSelect();
    });
@endpush

