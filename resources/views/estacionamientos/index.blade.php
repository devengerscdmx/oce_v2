@extends('layouts.app')
@section('title', 'Estacionamientos')
@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Estacionamientos</h3>
        </div>
    </div>
    <hr>
    @include('flash::message')
    <div class="row">
        <div class="col-6">
            <button type="button" class="btn btn-default btn-lg">
                Estacionamientos encontrados: <span class="counter badge badge-danger ml-4"></span>
            </button>
        </div>
        <div class="col-3 text-right">
            <a type="button" class="btn btn-primary btn-rounded" href="{!! route('marcas.index') !!}">
                Marcas
            </a>
        </div>
        <div class="col-3 text-right">
            <a type="button" class="btn btn-dark btn-rounded" href="{!! route('estacionamientos.create') !!}">
                Agregar estacionamiento
            </a>
        </div>
    </div>
    <br>
    @include('estacionamientos.table')
    <br><br>

@endsection
