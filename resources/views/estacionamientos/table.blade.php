<div class="table-responsive ">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Distrito/Región</th>
            <th>Código postal</th>
            <th>Tipo/Marca</th>
            <th>Facturable</th>
            <th>Automático</th>
            <th>Estadística</th>
            <th>Facturas</th>
            <th>Información</th>
            <th>Eliminar</th>
        </tr>
        <tr class="warning no-result">
            <td colspan="14" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>
        <tbody>
        @foreach($estacionamientos as $estacionamiento)
            <tr>
                <td>{!! $estacionamiento->no_est !!}</td>
                <td>{!! strtoupper($estacionamiento->nombre) !!}</td>
                <td>{!! $estacionamiento->dist_regio !!}</td>
                <td>{!! $estacionamiento->cp !!}</td>
                <td>
                    <a type="button" class="btn btn-outline-default  btn-rounded material-tooltip-main"
                       data-toggle="tooltip"
                       data-placement="bottom"
                       @if($estacionamiento->escuela)
                       title="Escuela"><i class="fas fa-school"></i>
                    @else
                       title="Estacionamiento"><i class="fas fa-parking "></i>
                        @endif
                    {!! $estacionamiento->marca !!}</a>
                </td>
                <td>
                    @if($estacionamiento->Facturable)
                        <a type="button" class="btn-floating btn-sm btn-success material-tooltip-main"
                           data-toggle="tooltip"
                           data-placement="bottom" title="Si"><i class="fas fa-check"></i></a>
                    @else
                        <a type="button" class="btn-floating btn-sm btn-danger material-tooltip-main"
                           data-toggle="tooltip"
                           data-placement="bottom" title="No"><i class="fas fa-times"></i></a>
                    @endif
                </td>
                <td>
                    @if($estacionamiento->Automatico)
                        <a type="button" class="btn-floating btn-sm btn-elegant material-tooltip-main"
                           data-toggle="tooltip"
                           data-placement="bottom" title="Si"><i class="fas fa-robot"></i></a>
                    @else
                        <a type="button" class="btn-floating btn-sm btn-elegant material-tooltip-main"
                           data-toggle="tooltip"
                           data-placement="bottom" title="No"><i class="fas fa-user"></i></a>
                    @endif
                </td>
                <td>
                    <a type="button" class="btn-floating btn-sm btn-primary" href="{!! route('graficas', [$estacionamiento->no_est]) !!}"><i
                                class="fas fa-chart-pie"></i></a>
                </td>
                <td>
                    <a type="button" class="btn-floating btn-sm btn-unique" href="{!! route('estacionamientos.show', [$estacionamiento->no_est]) !!}"><i
                                class="fas fa-file-invoice-dollar"></i></a>
                </td>
                <td>
                    <a type="button" class="btn-floating btn-sm btn-default"
                       href="{!! route('estacionamientos.edit',[$estacionamiento->no_est]) !!}"><i
                                class="fas fa-info"></i></a>
                </td>
                <td>
                    {!! Form::open(['route' => ['estacionamientos.destroy', $estacionamiento->no_est], 'method' => 'delete' ]) !!}
                    {!! Form::button('<i class="fas fa-ban"></i>', ['type' => 'submit', 'class' => 'btn btn-floating btn-danger', 'onclick' => "return confirm('¿Seguro de eliminarlo?')"]) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$estacionamientos->links()}}
</div>
