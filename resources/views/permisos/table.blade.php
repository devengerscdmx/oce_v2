<div class="table-responsive">
    <table class="table" id="permisos-table">
        <thead>
        <tr>
            <th>Nombre</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($permisos as $permiso)
            <tr>
                <td>{!! $permiso->nombre !!}</td>
                <td>
                    {!! Form::open(['route' => ['permisos.destroy', $permiso->id], 'method' => 'delete']) !!}

                    <a href="{!! route('permisos.edit', [$permiso->id]) !!}"
                       class='btn btn-default btn-xs white-text'>visualizar</a>

                    {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
